using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

namespace PhotoTracker
{
    public class StyleImage : CollectionBase
    {
        #region Members   
    
        int _intStyleImageID;
        string _strStyle = String.Empty;
        string _strImageURL = String.Empty;
        int _intImageType;
        bool _isPrimaryImage = false;
        int _intSortOrder;
        string _strFieldName = String.Empty;
        DateTime _dtCreatedDate;

        #endregion Members 

       public enum ImageType
        {
        Normal = 1,
        Swatch = 2,
        Flat = 3,
        Video = 4,
        Model=5,
        Square = 6
        };

 
        #region Properties
        public int IntStyleImageID
        {
            get { return _intStyleImageID; }
            
        }
        public string StrStyle
        {
            get { return _strStyle; }
            set { _strStyle = value; }
        }
        public string StrImageURL
        {
            get { return _strImageURL; }
            set { _strImageURL = value; }
        }        
        public int  IntImageType
        {
            get { return  _intImageType; }
            set { _intImageType = value; }
        }
        public bool  IsPrimaryImage
        {
            get { return  _isPrimaryImage; }
            set { _isPrimaryImage = value; }
        }
        public int  IntSortOrder
        {
            get { return  _intSortOrder; }
            set { _intSortOrder = value; }
        }
        public string StrFieldName
        {
            get { return _strFieldName; }
            set { _strFieldName = value; }
        }  
        public DateTime DtCreatedDate
        {
            get { return _dtCreatedDate; }
            set { _dtCreatedDate = value; }
        }
        
        
        #endregion Properties


        public StyleImage()
        {
        }

        public void PopulateProperties(SqlDataReader dr)
        {
            _strStyle = Convert.ToString(dr["ItemNum"]);
            _strImageURL = Convert.ToString(dr["ImageURL"]);
            _intImageType = Convert.ToInt16(dr["ImageType"]);
            _isPrimaryImage = Convert.ToBoolean(dr["PrimaryImage"]);
            _intSortOrder = Convert.ToInt16(dr["SortOrder"]);
            _strFieldName = Convert.ToString(dr["FieldName"]);
            if (dr["CreatedDate"] != System.DBNull.Value) 
            {
                _dtCreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
            }
            else
            {
                _dtCreatedDate = DateTime.Parse("1/1/1980");
            }          

        }
		
    }
}
