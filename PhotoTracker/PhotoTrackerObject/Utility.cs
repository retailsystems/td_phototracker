using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PhotoTracker
{
    public class Utility
    {
        public Utility()
        {
        }

        public static string GetPerformName(int intPerfType, int intEntryTyp, string itemNum, ItemEntryGroup itemEntryGroup)
        {
            string strEmpName = string.Empty;
            if (itemEntryGroup.Count > 0)
            {
                foreach (ItemEntry itmEntry in itemEntryGroup)
                {
                    if ((itmEntry.IntEmpTrkAccStatus == intPerfType) && (itmEntry.IntEntryType == intEntryTyp))
                    {
                        strEmpName = itmEntry.StrEmpName;
                    }
                }
            }

            return strEmpName;
        }


        public static bool GetItemRight(int intPerfType, int intEntryTyp, ItemEntryGroup itemEntryGroup)
        {
            bool bHasRight = false;

            if (itemEntryGroup.Count > 0)
            {
                foreach (ItemEntry itmEntry in itemEntryGroup)
                {
                    if ((itmEntry.IntEmpTrkAccStatus == intPerfType) && (itmEntry.IntEntryType == intEntryTyp))
                    {
                        bHasRight = true;
                    }
                }
            }
            return bHasRight;
        }

        public static bool GetUserRight(int intPerfType, int intEmpID, EmpEntryGroup empEntryGroup)
        {
            bool bHasRight = false;

            //string strDepartment = dtUser.Rows[0]["Dept"].ToString().Trim();
            if (empEntryGroup.Count > 0)
            {
                foreach (EmpEntry empEntry in empEntryGroup)
                {
                    if ((empEntry.IntEmployeeID == intEmpID) && (empEntry.IntTrackGroupNo == intPerfType))
                    {
                        bHasRight = true;
                    }
                }
            }
            else
            {
                bHasRight = false;
            }
            return bHasRight;
        }

        public static bool GetRight(int intPerfType, int intEntryTyp, int intEmpID, EmpEntryGroup empEntryGroup, ItemEntryGroup itemEntryGroup)
        {
            bool bHasItemRight = false;
            bool bHasEmpRight = false;
            bool bHasAllRight = false;

            bHasItemRight = Utility.GetItemRight(intPerfType, intEntryTyp, itemEntryGroup);
            bHasEmpRight = Utility.GetUserRight(intPerfType, intEmpID, empEntryGroup);
            if ((bHasItemRight == true) && (bHasEmpRight == true))
            {
                bHasAllRight = false;
            }
            else if ((bHasItemRight == true) && (bHasEmpRight == false))
            {
                bHasAllRight = false;
            }
            else if ((bHasItemRight == false) && (bHasEmpRight == true))
            {
                bHasAllRight = true;
            }
            else if ((bHasItemRight == false) && (bHasEmpRight == false))
            {
                bHasAllRight = false;
            }

            return bHasAllRight;
        }

        public static bool CheckSecurity(int permit, string region, string district, string store, string userCode, string userLoc, string userRegion)
        {

            bool isOK = false;
            switch (permit)
            {
                case 1:
                    isOK = true;
                    break;
                case 5:
                    isOK = true;
                    break;
                case 2:
                    //RD, see if they are inside their own region
                    if (userCode == region)
                    {
                        isOK = true;
                    }
                    break;

                case 3:
                    //DM, see if they are inside their own region and district
                    if (userCode == district && userRegion == region)
                    {
                        isOK = true;
                    }
                    break;
                case 4:
                    //Store, see if they are inside their own store
                    if (userLoc == store)
                    {
                        isOK = true;
                    }
                    break;
            }
            return isOK;

        }
    }
}
