using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Text;


namespace PhotoTracker
{
    public class EmpEntry : CollectionBase
    {
        #region Members
        int _intEmployeeID;
        int _intTrackGroupNo;
        bool _bEmpRight = false;
        #endregion Members

        #region Properties
        public int IntEmployeeID
        {
            get { return _intEmployeeID; }
            set { _intEmployeeID = value; }
        }

        public int IntTrackGroupNo
        {
            get { return _intTrackGroupNo; }
            set { _intTrackGroupNo = value; }
        }

        public bool BEmpRight
        {
            get { return _bEmpRight; }
            set { _bEmpRight = value; }
        }

        #endregion Properties

        public void PopulateEmpEntryProperties(SqlDataReader dr)
        {
            _intEmployeeID = Convert.ToInt32(dr["EmployeeID"]);
            _intTrackGroupNo = Convert.ToInt32(dr["TrackGroupNo"]);
            _bEmpRight = true;
        }

        internal static EmpEntry GetEmpEntry(SqlDataReader dr)
        {
            EmpEntry _EmpEntry = new EmpEntry();
            _EmpEntry.PopulateEmpEntryProperties(dr);
            return _EmpEntry;
        }
    }
}
