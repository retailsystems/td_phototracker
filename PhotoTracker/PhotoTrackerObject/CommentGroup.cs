using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.IO;


namespace PhotoTracker
{
    [Serializable]
    public class CommentGroup : CollectionBase
    {
        public CommentGroup()
        { 
        }

        public static CommentGroup GetCommentsDataTable(string strItemNum)
        {
            CommentGroup theCommentGroup = new CommentGroup();

            theCommentGroup.CommentGroupFetch(strItemNum);
            return theCommentGroup;
        }
        //public static OrderItemGroup GetOrderItemDataTable(string strOrderNo)
        //{
        //    OrderItemGroup theOrderItemGroup = new OrderItemGroup();
        //    theOrderItemGroup.OrderItemGroupFetch(strOrderNo);
        //    return theOrderItemGroup;
        //}

        private void CommentGroupFetch(string strItemNum)
        //private void OrderItemGroupFetch(string strOrderNo)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();

            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Track_GetCommentsByItemNum";

                SqlDataReader dr;
                cm.Parameters.Clear();

                SqlParameter mmm = new SqlParameter();
                mmm.ParameterName = "@ItemNum";
                mmm.SqlDbType = SqlDbType.VarChar;
                mmm.Value = strItemNum;
                cm.Parameters.Add(mmm);

                dr = cm.ExecuteReader();

                while (dr.Read())
                    List.Add(Comment.GetComment(dr));
            }
            //catch (Exception ex)
            //{
            //    //int s=0;

            //}
            finally
            {
                cn.Close();
            }
        }
    }
}
