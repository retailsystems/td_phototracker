using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

namespace PhotoTracker 
{
    public class EntryControlBase: CollectionBase
    {
        #region Members
           
        #endregion Members

        public void SetSender(EntryControlBase oneSender)
        {
            if (!List.Contains(oneSender))
            {
                List.Add(oneSender);

            }
            else
            {
                throw new IndexOutOfRangeException("object already exists in this list.");
            }
        }

        // Employee Track Access permission register
        public void SenderRegister(EntryControlBase myObj, int nEmpTrackAccess)
        { 
          
        }
        //Item track access status register
        public void SenderRegister(EntryControlBase myObj, int nItemTrackAccess, EventHandler e)
        { 

        }

    }
}
