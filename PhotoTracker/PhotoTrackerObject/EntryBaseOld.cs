using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace PhotoTracker.PhotoTrackerObject
{
    public abstract class EntryBaseOld
    {
        List<EntryBase> m_listRecver = new List<EntryBase>();
        List<EntryBase> m_listSender = new List<EntryBase>();
        public WebControl m_webControl = null;
        public string m_strMsgType = null;

        public EntryBaseOld(WebControl webControl)
        {
            m_webControl = webControl; 
        }

        //call from obj of StartRegister()
        protected void Register(EntryBase RecvObj)
        {
            m_listRecver.Add(RecvObj);
        }
        //call from EntryObjManager
        public void Notification(String strMsgType, int nMsgValue, int nEntryType, bool bMsgValue, int intEmpID, string strItmNum, string strDiv)
        {
            foreach (EntryBase obj in m_listRecver)
            {
                obj.DoNotification(strMsgType, nMsgValue, nEntryType, bMsgValue, intEmpID, strItmNum, strDiv);
            }
        }

        public virtual void DoNotification(String strMsgType, int nMsgValue, int nEntryType, bool bMsgValue)
         {
         }
        //call from EntryObjManager
        public void SetSender(EntryBase SenderObj, string strMsgType)
        {
            SenderObj.m_strMsgType = strMsgType;
            m_listSender.Add(SenderObj);
        }
        public void SetSender(EntryBase SenderObj)
        {
            m_listSender.Add(SenderObj);
        }

        //call from EntryObjManager
        //public void StartRegister()
        //{
        //    foreach (EntryBase obj in m_listSender)
        //    {
        //        obj.Register(this);
        //    }
        //}
    }
}
