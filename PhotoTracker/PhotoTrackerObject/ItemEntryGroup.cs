using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Text;


namespace PhotoTracker
{
    public class ItemEntryGroup : CollectionBase
    {
        public ItemEntryGroup()
        {
        }
        public void Add(ItemEntry item)
        {
            if (!List.Contains(item))
            {
                List.Add(item);

            }
            else
            {
                throw new IndexOutOfRangeException("object already exists in this list.");
            }
        }

        public static ItemEntryGroup GetItemEntryDataTable(string strItemNo)	        
		{
			ItemEntryGroup theItemEntryGroup = new ItemEntryGroup();
			theItemEntryGroup.ItemEntryGroupFetch(strItemNo);
			return theItemEntryGroup;
		}

       private void ItemEntryGroupFetch(string strItemNo)
		{
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
			SqlCommand cm = new SqlCommand();

			try
			{
				cn.Open();
				cm.Connection = cn;
				cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Track_GetItemTrackInfo";
				
				SqlDataReader dr;              
				cm.Parameters.Clear();
		
				SqlParameter mmm = new SqlParameter();
                mmm.ParameterName = "@ItemNum";
				mmm.SqlDbType = SqlDbType.VarChar;
				mmm.Value = strItemNo;
				cm.Parameters.Add(mmm);

				dr = cm.ExecuteReader();
				
				while (dr.Read())
                    List.Add(ItemEntry.GetItem(dr));
			}
            //catch(Exception ex)
            //{
            //    //int s=0;

            //}
			finally
			{
				cn.Close();
			}
		}
    }
}
