using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

namespace PhotoTracker
{
    public class Item : CollectionBase
    {
        #region Members
        string _strItemNum = String.Empty;
        string _strDesc = String.Empty;
        //int _intDivision;
        string _strDivision = String.Empty;
        string _strDept = String.Empty;
        string _strClass = String.Empty;
        string _strSubClass = String.Empty;
        //int _intClass;
        //int _intSubClass;
        double _dblCurrRetlPrice;
        string _strTheme1 = String.Empty;
        string _strTheme2 = String.Empty;
        string _strTheme3 = String.Empty;
        string _strTheme4 = String.Empty;
        string _strTheme5 = String.Empty;
        string _strPrivLabel = String.Empty;
        string _strWebTitle1 = String.Empty;
        string _strWebDesc1 = String.Empty;
        string _strWebDesc2 = String.Empty;
        string _strWebDesc3 = String.Empty;
        string _strWebDesc4 = String.Empty;
        string _strWebDesc5 = String.Empty;
        string _strWebDesc6 = String.Empty;
        string _strWebDesc7 = String.Empty;
        string _strWebDesc8 = String.Empty;
        string _strWebDesc9 = String.Empty;
        string _strPONumber = String.Empty;
        DateTime _dtCancelDate;
        string _strTopSample = String.Empty;
        int _intItemType;

        string _strLastApprDt = String.Empty;
        string _strPhCompleteInd = String.Empty;
        string _strPhEditCompleteInd = String.Empty;
        string _strVideoExistInd = String.Empty;
        string _strPrimIMGPath = String.Empty;
        string _stritemTitle = String.Empty;

              

        #endregion Members 

        #region Properties
        public string StrItemNum
        {
            get { return _strItemNum; }
            set { _strItemNum = value; }
        }
        public string StrDesc
        {
            get { return _strDesc; }
            set { _strDesc = value; }
        }
      
        public string StrClass
        {
            get { return _strClass; }
            set { _strClass = value; }
        }
        public string StrSubClass
        {
            get { return _strSubClass; }
            set { _strSubClass = value; }
        }
        public string StrDivision
        {
            get { return _strDivision; }
            set { _strDivision = value; }
        }
        public string StrDept
        {
            get { return _strDept; }
            set { _strDept = value; }
        }
        //public int IntClass
        //{
        //    get { return _intClass; }
        //    set { _intClass = value; }
        //}
        //public int IntSubClass
        //{
        //    get { return _intSubClass; }
        //    set { _intSubClass = value; }
        //}
        public double DblCurrRetlPrice
        {
            get { return _dblCurrRetlPrice; }
            set { _dblCurrRetlPrice = value; }
        }
        public string StrTheme1
        {
            get { return _strTheme1; }
            set { _strTheme1 = value; }
        }
        public string StrTheme2
        {
            get { return _strTheme2; }
            set { _strTheme2 = value; }
        }
        public string StrTheme3
        {
            get { return _strTheme3; }
            set { _strTheme3 = value; }
        }
        public string StrTheme4
        {
            get { return _strTheme4; }
            set { _strTheme4 = value; }
        }

        public string StrTheme5
        {
            get { return _strTheme5; }
            set { _strTheme5 = value; }
        }
        public string StrPrivLabel
        {
            get { return _strPrivLabel; }
            set { _strPrivLabel = value; }
        }
        public string StrWebTitle1
        {
            get { return _strWebTitle1; }
            set { _strWebTitle1 = value; }
        }
        public string StrWebDesc1
        {
            get { return _strWebDesc1; }
            set { _strWebDesc1 = value; }
        }
        public string StrWebDesc2
        {
            get { return _strWebDesc2; }
            set { _strWebDesc2 = value; }
        }
        public string StrWebDesc3
        {
            get { return _strWebDesc3; }
            set { _strWebDesc3 = value; }
        }
        public string StrWebDesc4
        {
            get { return _strWebDesc4; }
            set { _strWebDesc4 = value; }
        }
        public string StrWebDesc5
        {
            get { return _strWebDesc5; }
            set { _strWebDesc5 = value; }
        }
        public string StrWebDesc6
        {
            get { return _strWebDesc6; }
            set { _strWebDesc6 = value; }
        }
        public string StrWebDesc7
        {
            get { return _strWebDesc7; }
            set { _strWebDesc7 = value; }
        }
        public string StrWebDesc8
        {
            get { return _strWebDesc8; }
            set { _strWebDesc8 = value; }
        }
        public DateTime DtCancelDate
        {
            get { return _dtCancelDate; }
            set { _dtCancelDate = value; }
        }
        public string StrWebDesc9
        {
            get { return _strWebDesc9; }
            set { _strWebDesc9 = value; }
        }
        public string StrTopSample
        {
            get { return _strTopSample; }
            set { _strTopSample = value; }
        }
        public int IntItemType
        {
            get { return _intItemType; }
            set { _intItemType = value; }
        }
        public string StrLastApprDt
        {
            get { return _strLastApprDt; }
            set { _strLastApprDt = value; }
        }
        public string StrPhCmpltInd
        {
            get { return _strPhCompleteInd; }
            set { _strPhCompleteInd = value; }
        }
        public string StrPhEditCmpltdInd
        {
            get { return _strPhEditCompleteInd; }
            set { _strPhEditCompleteInd = value; }
        }
        public string StrVDExistInd
        {
            get { return _strVideoExistInd; }
            set { _strVideoExistInd = value; }
        }
        public string StrPrimIMGPath
        {
            get { return _strPrimIMGPath; }
            set { _strPrimIMGPath = value; }
        }
        public string StrItemTitle
        {
            get { return _stritemTitle; }
            set { _stritemTitle = value; }
        }
        
        #endregion Properties


        public Item()
        {
        }

        public void PopulateProperties(SqlDataReader dr)
        {
            _strItemNum = Convert.ToString(dr["ItemNum"]);
            _strDesc = Convert.ToString(dr["Description"]);
            //_intDivision = Convert.ToInt16(dr["Division"]);
            _strDivision = Convert.ToString(dr["Division"]);
            _strDept = Convert.ToString (dr["Department"]);
            _strClass = Convert.ToString(dr["Class"]);
            _strSubClass = Convert.ToString(dr["SubClass"]);
            //_intClass = Convert.ToUInt16(dr["Class"]);
            //_intSubClass = Convert.ToUInt16(dr["SubClass"]);
            _dblCurrRetlPrice = Convert.ToDouble(dr["CurrentRetailPrice"]);
            _strTheme1 = Convert.ToString(dr["Theme1"]);
            _strTheme2 = Convert.ToString(dr["Theme2"]);
            _strTheme3 = Convert.ToString(dr["Theme3"]);
            _strTheme4 = Convert.ToString(dr["Theme4"]);
            _strTheme5 = Convert.ToString(dr["Theme5"]);
            _strPrivLabel = Convert.ToString(dr["PrivLabel"]);
            _strTopSample = Convert.ToString(dr["TopSample"]);
            //_intItemType = Convert.ToInt16(dr["ItemType"]);
        
            if (dr["CancelDate"] != System.DBNull.Value) 
            {
                _dtCancelDate = Convert.ToDateTime(dr["CancelDate"]);
            }
            else
            {
                _dtCancelDate = DateTime.Parse("1/1/1980");
            }

            if (dr["lastphapprdate"].ToString() != null && dr["lastphapprdate"].ToString() != string.Empty)
            {
                _strLastApprDt = FilterDate(Convert.ToDateTime(dr["lastphapprdate"].ToString()));
            }

            _strPhCompleteInd = Convert.ToString(dr["phCompleteInd"]);
            _strPhEditCompleteInd = Convert.ToString(dr["phEditCompleteInd"]);
            _strVideoExistInd = Convert.ToString(dr["videoExistInd"]);
            _strPrimIMGPath = Convert.ToString(dr["primaryimgpath"]);
            _stritemTitle = Convert.ToString(dr["phtitle"]);

        }

        private string FilterDate(DateTime dt)
        {
            if (dt > DateTime.Parse("1/1/1980"))
            {
                return dt.ToShortDateString();
            }
            else
            {
                return string.Empty;
            }
        }

        public static Item GetOneItem(string strItemNo)
        {
            Item theOneItem = new Item();
            theOneItem.OneItemFetch(strItemNo);
            return theOneItem;
        }

        private void OneItemFetch(string strItemNo)
        {
 
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"]);
            SqlCommand cm = new SqlCommand();


            try
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandText = "Track_GetGersInfoByItemNo";

                SqlDataReader dr;
                cm.Parameters.Clear();

                SqlParameter nnn = new SqlParameter();
                nnn.ParameterName = "@ItemNum";
                nnn.SqlDbType = SqlDbType.VarChar;
                nnn.Value = strItemNo;
                cm.Parameters.Add(nnn);

                dr = cm.ExecuteReader();

                try
                {
                    if (dr.Read())
                    {
                        this.PopulateProperties(dr);

                    }
                }
                finally
                {
                    dr.Close();
                }

            }
            //catch (Exception ex)
            //{
            //    //int s=0;

            //}
            finally
            {
                cn.Close();
            }
        }
		
    }
}
