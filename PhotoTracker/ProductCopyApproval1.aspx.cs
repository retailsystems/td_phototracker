using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using Bytescout.BarCode;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.OracleClient;
using System.ComponentModel;
using System.Text;
using FreeTextBoxControls;
using System.Collections.Generic;


namespace PhotoTracker
{
    public partial class ProductCopyApproval1 : System.Web.UI.Page
    {
        protected Item theGersTrkItem;
        protected ItemEntry theItemEntryInfo;
        protected ItemEntryGroup theItemEntryGroup;
        protected EmpEntryGroup empEntryGroup;
        protected CommentGroup newCommentGroup;
        public DataTable theItemTopInfo;
        public DataTable dtUser;
        public DataTable dtPermit;
        string strGroupNo = String.Empty;
        public string strItemNum = string.Empty;
        public int EmployeeID = 0;
        public int colorcount = 0; //By MK
        public bool isStyleSelected = false;//By MK
        public string strDiv = string.Empty;
        public string strBarCodeDir = Data.strBarCodePath();
        public string strItmNum = string.Empty;
        public string strZoomedParams = string.Empty;
        public bool bShowImg = false;
        string strPath = ConfigurationManager.AppSettings["strImgDirPath"];
        public string strTopSamp = string.Empty;

        public string strItmPrimaryImg = string.Empty;
        string strImgHTurl = ConfigurationManager.AppSettings["strImgHTurl"];
        public string errString = string.Empty;
        public string rmsProgramNum = string.Empty;//Added by MK  
        public string rmsSelectedColor = string.Empty;//Added by MK
        public string rmsItemNum = string.Empty;
        public string rmsItemDesc = string.Empty;
        public string catalogDesc = string.Empty;
        public string dptNum = string.Empty;
        public string clsNum = string.Empty;
        public string subclsNum = string.Empty;
        public string phTitle = string.Empty;
        public string phDtlDesc = string.Empty;
        public string dtlDescTxt = string.Empty;
        public string primaryIMGPath = string.Empty;
        public string lastUpdateId = string.Empty;
        public string lastUpdateDate = string.Empty;
        public string lastUpdateDtl = string.Empty;
        public string styleOnlineFlg = string.Empty;
        public string lastApprDate;
        public string lastRcptDt;
        public string statusCode = string.Empty;
        SqlDataAdapter da1 = new SqlDataAdapter();
        DataSet ds1 = new DataSet();
        SqlCommand _cmd = new SqlCommand();
        SqlCommand _cmd1 = new SqlCommand();
        string dbconn = ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString();


        public ArrayList listImages = new ArrayList();
        //public ArrayList listImgDBSave = new ArrayList();
        //public ArrayList listAltImgDBSave = new ArrayList();
        //public ArrayList newListImages = new ArrayList();
        public ArrayList styleListImages = new ArrayList();
        public List<StyleImage> lstStyleImages = new List<StyleImage>(); //Added by MK
        public int styleItemCount = 0; //Added by MK

        //Commented by Mk as new Image Table will be used
        public string primeImgDB = string.Empty;
        //public string alt1ImgDB = string.Empty;
        //public string alt2ImgDB = string.Empty;
        //public string alt3ImgDB = string.Empty;
        //public string alt4ImgDB = string.Empty;
        //public string alt5ImgDB = string.Empty;
        //public string alt6ImgDB = string.Empty;
        public string swatchImgDB = string.Empty;
        public string flatImgDB = string.Empty;
        public string strFunctionStatus = string.Empty;

        bool bHasEditPermt = false;
        bool bHasApprovePermt = false;

        public void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////get user DataTable from session
                strItmNum = strPath + strItemNum;
                //strZoomedParams = "?$400x600$"; //"?$35x53$";   
                strZoomedParams = ConfigurationManager.AppSettings["strZoomParamStd"];

                //bHasPermt = AppUser.CheckSecurity("2", dtPermit);

                setUserRole();

                if (bHasApprovePermt == true)
                {

                    txtItemNum.Visible = true;
                    searchbBtn.Visible = true;
                    BarcodeWebImage2.Visible = false;
                    last_upd_dt_id.Enabled = false;
                    programNum.Enabled = false; //by MK
                    selColor.Enabled = false; //by MK
                    itemNum.Enabled = false;
                    catDesc.Enabled = false;
                    txtRMSDesc.Enabled = false;
                    deptNum.Enabled = false;
                    classNum.Enabled = false;
                    subClassNum.Enabled = false;
                    Image52.Visible = false;
                    itemDtlDesc.Visible = false;
                    itemDtlDesc.Enabled = false;
                    FreeTextBox1.EnableHtmlMode = false;
                    FreeTextBox1.EnableViewState = false;
                    FreeTextBox1.EnableToolbars = false;
                    FreeTextBox1.EnableSsl = false;
                    FreeTextBox1.ReadOnly = true;

                    //itemTitle.Enabled = false;
                    itemTitle.ReadOnly = true;

                    //itemTitle.Enabled = false;
                    //save.Enabled = false;
                    //approve.Enabled = false;
                    unApprove.Enabled = false;
                    //searchbBtn.Enabled = false;
                    ////imgItem.Visible = false;
                    //dtlDescEdit.Visible = false;
                    titleEdit.Visible = false;
                }
                else if (bHasEditPermt == true)
                {

                    txtItemNum.Visible = true;
                    searchbBtn.Visible = true;
                    BarcodeWebImage2.Visible = false;
                    last_upd_dt_id.Enabled = false;
                    itemNum.Enabled = false;
                    programNum.Enabled = false; //by MK
                    selColor.Enabled = false; //by MK
                    catDesc.Enabled = false;
                    txtRMSDesc.Enabled = false;
                    deptNum.Enabled = false;
                    classNum.Enabled = false;
                    subClassNum.Enabled = false;
                    Image52.Visible = false;
                    itemDtlDesc.Visible = false;
                    itemDtlDesc.Enabled = false;
                    FreeTextBox1.EnableHtmlMode = false;
                    FreeTextBox1.EnableViewState = false;
                    FreeTextBox1.EnableToolbars = false;
                    FreeTextBox1.EnableSsl = false;
                    FreeTextBox1.ReadOnly = true;
                    approve.Visible = false;
                    unApprove.Visible = false;
                    apprDate.Enabled = false;
                    apprDate.ReadOnly = true;
                    lastRcptDate.Enabled = false;
                    lastRcptDate.ReadOnly = true;

                    //itemTitle.Enabled = false;
                    itemTitle.ReadOnly = true;

                    //itemTitle.Enabled = false;
                    //save.Enabled = false;
                    //approve.Enabled = false;
                    unApprove.Enabled = false;
                    //searchbBtn.Enabled = false;
                    ////imgItem.Visible = false;
                    //dtlDescEdit.Visible = false;
                    titleEdit.Visible = false;
                }
                else
                {
                    //no proper permission go back to Login
                    Response.Redirect("./Login.aspx");

                }
            }

        }

        protected void setUserRole()
        {
            dtUser = (DataTable)Session["UserProfile"];
            dtPermit = (DataTable)Session["ViewPermission"];


            //ShowItmImage(strItemNum);
            //bool bHasPermt = false;

            if (dtPermit.Rows.Count > 0)
            {
                for (int i = 0; i < (dtPermit.Rows.Count); i++)
                {
                    strGroupNo = dtPermit.Rows[i]["GroupNo"].ToString().Trim();
                    if (strGroupNo == "4")
                    {
                        bHasApprovePermt = true;
                    }
                    else if (strGroupNo == "5")
                    {
                        bHasEditPermt = true;

                    }
                }
            }
        }


        private bool ImageExists(string imgURL)
        {
            WebRequest webRequest = WebRequest.Create(imgURL);

            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                webResponse.GetResponseStream();
                webResponse.Close();

                return true;
            }

            catch
            {
                return false;
            }
        }


        private void ClearExpItmTxt()
        {
            itemNum.Visible = false;
            catDesc.Visible = false;
            BarcodeWebImage2.Visible = false;
            txtRMSDesc.Visible = false;
            last_upd_dt_id.Visible = false;
            apprDate.Visible = false;
            lastRcptDate.Visible = false;
            //itemTitle.Visible = false;
            itemTitle.ReadOnly = true;
            itemDtlDesc.Visible = false;
            deptNum.Visible = false;
        }

        private void EmtyExpItmTxt()
        {
            programNum.Text = string.Empty;
            selColor.Text = string.Empty;
            //txtItemNum.Text = string.Empty;
            itemNum.Text = string.Empty;
            catDesc.Text = string.Empty;
            BarcodeWebImage2.Visible = false;
            txtRMSDesc.Text = string.Empty;
            last_upd_dt_id.Text = string.Empty;
            apprDate.Text = string.Empty;
            lastRcptDate.Text = string.Empty;
            itemTitle.Text = string.Empty;
            itemDtlDesc.Text = string.Empty;
            deptNum.Text = string.Empty;
            classNum.Text = string.Empty;
            imgPath.Text = string.Empty;
            subClassNum.Text = string.Empty;
            FreeTextBox1.Text = string.Empty;
            FreeTextBox1.EnableHtmlMode = false;
            FreeTextBox1.EnableViewState = false;
            FreeTextBox1.EnableToolbars = false;
            FreeTextBox1.EnableSsl = false;
            FreeTextBox1.ReadOnly = true;
            skuGrdView.Visible = false;           
        }


        private void EnableTxt(object sender, EventArgs e)
        {
            itemNum.Visible = true;
            catDesc.Visible = true;
            BarcodeWebImage2.Visible = true;
            txtRMSDesc.Visible = true;
            last_upd_dt_id.Visible = true;
            apprDate.Visible = true;
            lastRcptDate.Visible = true;
            itemTitle.Visible = true;
            //itemDtlDesc.Visible = true;
            deptNum.Visible = true;
            classNum.Visible = true;
            skuGrdView.Visible = true;
           // ddlColorShade.Visible = true; //MK
            imgPath.Visible = true;
            subClassNum.Visible = true;

        }

        private void ClearAll()
        {
            txtItemNum.Text = string.Empty;
            ClearExpItmTxt();
        }


        private void LoadEntryTime(TextBox txtTimeBox, string strTime)
        {
            txtTimeBox.Text = string.Empty;
            txtTimeBox.Text = strTime;

            if (strTime != "")
            {
                txtTimeBox.Enabled = false;

            }
        }
        private void LoadRecvName(TextBox txtRecNameBox, string strName)
        {
            txtRecNameBox.Text = string.Empty;
            txtRecNameBox.Text = strName;

            if (strName != "")
            {
                txtRecNameBox.Enabled = false;

            }
        }
        private void LoadDropdownName(DropDownList ddlList, string strName, bool IsSelected)
        {
            ddlList.Items.Clear();
            ListItem itm = new ListItem();
            itm.Text = strName;
            itm.Value = strName;
            ddlList.Items.Add(itm);
            if (IsSelected)
            {
                ddlList.Enabled = false;

            }
        }
        //Filter out invalid dates
        private string FilterDate(DateTime dt)
        {
            if (dt > DateTime.Parse("1/1/1980"))
            {
                return dt.ToShortDateString();
            }
            else
            {
                return string.Empty;
            }
        }




        protected int GetItemDtl(string strItemNum,ref string strErrorMsg)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            String strSupported_Divisions = null;

            int returnCode = 0;

            strErrorMsg = string.Empty;

            try
            {
                strSupported_Divisions = ConfigurationManager.AppSettings["Supported_Divisions"];
                conn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString());
                conn.Open();
                //Commented by MK to add Division as addition filter criteria and fetch it from appsettings
                //cmd = new SqlCommand("SELECT im.Style,im.description rmsItmDesc, ci.description catDesc,isnull(ONLINE_FLAG,'N') ONLINE_FLG , " +
                //                     "im.department,im.class,im.subclass,im.phtitle,im.primaryimgpath, im.lastphupdateid, " +
                //                     " im.lastphupdatedate,im.lastphapprdate,im.last_receipt_date,im.dtlDesc,im.status,cast(im.dtl_Desc_txt as varchar(max)) as LongDesc FROM [RMS_PH_STYLE_Master] im " +
                //                     " INNER JOIN [Catalog_Id] ci ON im.division = ci.division where " +
                //                     "  (im.Style='" + strItemNum + "' or  im.Style in (select Program from [RMS_DW_ITEM_MASTER] im1 " + //need to change the sku table name
                //                     " where im1.sku='" + strItemNum + "')) ", conn);

                cmd = new SqlCommand("GetItemDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@division", SqlDbType.VarChar, 25, "division"));
                cmd.Parameters.Add(new SqlParameter("@itmNum", SqlDbType.VarChar, 25, "itmNum"));
                cmd.Parameters.Add(new SqlParameter("@SearchByStyle", SqlDbType.Bit, 25, "isStyleSelected"));
                cmd.Parameters[0].Value = strSupported_Divisions;
                cmd.Parameters[1].Value = strItemNum;
                cmd.Parameters[2].Value = isStyleSelected;


                SqlDataReader reader = cmd.ExecuteReader();
                
                //conn = new SqlConnection(ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString());
                //conn.Open();
                /*cmd = new SqlCommand("SELECT im.itemnum,im.description rmsItmDesc, [Catalog_Id].description catDesc FROM [RMS_Item_Master] im " +
                                     " INNER JOIN [Catalog_Id] ON im.division = [Catalog_Id].division where im.Program is null " +
                                     " and (im.itemnum='" + strItemNum + "' or  im.itemnum in (select Program from [RMS_Item_Master] im1 " +
                                     " where im1.itemnum='" + strItemNum + "')) ", conn); */

                //reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    /*if (reader["Style"].ToString() != null && reader["Style"].ToString() != string.Empty)
                    {
                        rmsItemNum = reader["Style"].ToString();
                    }
                    if (reader["rmsItmDesc"].ToString() != null && reader["rmsItmDesc"].ToString() != string.Empty)
                    {
                        rmsItemDesc = reader["rmsItmDesc"].ToString();
                    }
                    if (reader["catDesc"].ToString() != null && reader["catDesc"].ToString() != string.Empty)
                    {
                        catalogDesc = reader["catDesc"].ToString();
                    }*/
                    rmsProgramNum = reader["programNum"].ToString();
                    rmsSelectedColor = reader["selColor"].ToString();

                    rmsItemNum = reader["Style"].ToString();
                    rmsItemDesc = reader["rmsItmDesc"].ToString();
                    catalogDesc = reader["catDesc"].ToString();
                    styleOnlineFlg = reader["ONLINE_FLG"].ToString();                    
                    dptNum = reader["department"].ToString();
                    clsNum = reader["class"].ToString();
                    subclsNum = reader["subclass"].ToString();
                    phTitle = reader["phtitle"].ToString();
                    primaryIMGPath = reader["primaryimgpath"].ToString();
                   // if (isStyleSelected)
                   // {
                       primaryIMGPath= primaryIMGPath.Replace(rmsProgramNum, strItemNum);                      
                        
                   // }
                    
                    phDtlDesc = reader["dtlDesc"].ToString();
                    if (reader["lastphupdatedate"].ToString() != null && reader["lastphupdatedate"].ToString() != string.Empty)
                    {
                        //lastUpdateDate = FilterDate(Convert.ToDateTime(reader["lastphupdatedate"].ToString()));
                        lastUpdateDate = reader["lastphupdatedate"].ToString();
                    }
                    if (reader["lastphupdateid"].ToString() != null && reader["lastphupdateid"].ToString() != string.Empty)
                    {
                        lastUpdateId = reader["lastphupdateid"].ToString();
                    }
                    if ((lastUpdateDate == null || lastUpdateDate == string.Empty) && (lastUpdateDtl == null || lastUpdateDtl == string.Empty))
                    {
                        lastUpdateDtl = string.Empty;
                    }
                    else
                    {
                        lastUpdateDtl = "[" + lastUpdateId + "," + lastUpdateDate + "]";
                    }

                    if (reader["lastphapprdate"].ToString() != null && reader["lastphapprdate"].ToString() != string.Empty)
                    {
                        lastApprDate = FilterDate(Convert.ToDateTime(reader["lastphapprdate"].ToString()));
                    }

                    if (reader["last_receipt_date"].ToString() != null && reader["last_receipt_date"].ToString() != string.Empty)
                    {
                        lastRcptDt = FilterDate(Convert.ToDateTime(reader["last_receipt_date"].ToString()));
                    }

                    statusCode = reader["status"].ToString();
                    dtlDescTxt = reader["LongDesc"].ToString();

                }

                cmd.Dispose();
                conn.Close();

                returnCode = 0;
            }
            catch (Exception ex)
            {
                strErrorMsg = ex.Message;
                System.Diagnostics.EventLog.WriteEntry("Photocopyapproval", ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                //LogError("An error occurred in GetItemDtl function while fetching the item details " + ex.ToString());
                returnCode = -1;
            }

            return returnCode;
        }

        //Deba
        protected void searchBtnClicked(object sender, EventArgs e)
        {
            bool bItem = false;
            //lblNoItemMsg.Text = string.Empty;
            errString = string.Empty;
            primeImgPath.Text = string.Empty;
            int intRevResult = 0;
            int intItemExixt = 0;
            int intRetCode = 0;

            primeImgChk.Checked = false;
            //ClearExpItmTxt();
            if (strFunctionStatus != "SAVEBTN" && strFunctionStatus != "APPROVEBTN" && strFunctionStatus != "UNAPPROVEBTN")
            {
                lblNoItemMsg.Text = string.Empty;
                strFunctionStatus = string.Empty;
            }
            else
            {
                strFunctionStatus = string.Empty;
            }
            EmtyExpItmTxt();
           
            strItemNum = this.txtItemNum.Text.Trim();
            setUserRole();
            if (bHasApprovePermt == true)
            {
                approve.Visible = true;
            }
            else
            {
                approve.Visible = false;
            }

            if (strItemNum != "")
            {
                ViewState["StrItemNum"] = strItemNum;
                isStyleSelected = false;  //MK
                intRetCode = GetItemDtl(strItemNum, ref errString);

                if (intRetCode == 0)
                {
                    if (intRetCode == 0 && rmsItemNum != string.Empty)
                    {
                        programNum.Text = rmsProgramNum;
                        selColor.Text = rmsSelectedColor;

                        itemNum.Text = rmsItemNum;
                        txtRMSDesc.Text = rmsItemDesc;
                        catDesc.Text = catalogDesc;
                        BarcodeWebImage2.Visible = true;
                        BarcodeWebImage2.Value = rmsItemNum;
                        //imgPath.Text    = primaryIMGPath;
                        last_upd_dt_id.Text = lastUpdateDtl;
                        apprDate.Text = lastApprDate;
                        lastRcptDate.Text = lastRcptDt;
                        itemTitle.Text = phTitle;
                        //itemDtlDesc.Text=phDtlDesc;
                        FreeTextBox1.Text = dtlDescTxt;
                        itemDtlDesc.Text = dtlDescTxt;
                        FreeTextBox1.ReadOnly = true;
                        deptNum.Text = dptNum;
                        classNum.Text = clsNum;
                        subClassNum.Text = subclsNum;
                        //dtlDescEdit.Visible = true;
                        //if (rmsProgramNum == rmsItemNum) //By MK
                        //{ titleEdit.Visible = true; }
                        //else { titleEdit.Visible = false; }
                       // skuBindData(rmsItemNum); //commented by MK
                        titleEdit.Visible = true;
                        skuBindData(strItemNum);
                        ColorShadeBindData(strItemNum);
                        skuGrdView.Visible = true;
                       // ddlColorShade.Visible = true;//MK
                        //itemTitle.Enabled = false;
                        itemTitle.ReadOnly = true;
                        FreeTextBox1.EnableHtmlMode = false;
                        FreeTextBox1.EnableViewState = false;
                        FreeTextBox1.EnableToolbars = false;
                        FreeTextBox1.EnableSsl = false;
                        FreeTextBox1.ReadOnly = true;

                        if (primaryIMGPath != null && primaryIMGPath != string.Empty)
                        {
                            strItmPrimaryImg = strImgHTurl + primaryIMGPath;
                            primeImgPath.Text = strItmPrimaryImg;
                        }

                        ShowItmImage(itemNum.Text);

                        currStatus.Text = statusCode;

                        if (statusCode == "APPROVED")
                        {
                            unApprove.Enabled = true;
                            unApprove.CssClass = "buttonRed";
                            approve.Enabled = false;
                            approve.CssClass = "buttonYellow";
                        }
                        else if ((statusCode == "SAVED" || statusCode == "UNAPPROVED") && (FreeTextBox1.Text != string.Empty && itemTitle.Text != string.Empty))
                        {
                            approve.Enabled = true;
                            approve.CssClass = "buttonRed";
                            unApprove.Enabled = false;
                            unApprove.CssClass = "buttonYellow";
                        }
                        else
                        {
                            approve.Enabled = true;
                            approve.CssClass = "buttonRed";
                            unApprove.Enabled = false;
                            unApprove.CssClass = "buttonYellow";
                        }

                    }
                    else
                    {
                        lblNoItemMsg.Text = "Item # " + strItemNum + " does not exist for Enrichment. Please re-enter the correct Item";
                        BarcodeWebImage2.Value = rmsItemNum;
                        //ClearAll();
                        ddlColorShade.Items.Clear();                        
                        EmtyExpItmTxt();
                        txtItemNum.Text = string.Empty;

                    }
                }
                else
                {
                    lblNoItemMsg.Text = errString;
                    //ClearAll();
                    EmtyExpItmTxt();
                    txtItemNum.Text = string.Empty;

                }

            }
            else
            {
                ClearAll();
                //pnlRecv.Visible = false;
                //pnlUpdate.Visible = true;
                //pnlEntry.Visible = false;
                lblNoItemMsg.Text = "You must input Item Number";
            }
        }
        //Deba


        private void ShowItmImage(string strItemNum)
        {
            string strURL = string.Empty;
            string strItemImage = string.Empty;
            string strPicValue = string.Empty;
            bool bUrlExists = false;
            bool bPrimeUrlExists = false;
            bool bSmlUrlExists = false;
            bool bColUrlExists = false;
            ArrayList listItems = new ArrayList();
            string strSmlURL = string.Empty;
            string strBHURL = string.Empty;
            string imgDB = string.Empty;
            string imgDBItem = string.Empty;

            strItmNum = strPath + strItemNum;
            //strZoomedParams = "?$400x600$";
            strZoomedParams = ConfigurationManager.AppSettings["strZoomParamStd"];
            strItemImage = ConfigurationManager.AppSettings["strImgPathSplit1"];

            strURL = strItemImage.Replace("[picvalue]", strItemNum);

            imgDB = ConfigurationManager.AppSettings["strImgPathSplit"];
            imgDBItem = imgDB.Replace("[picvalue]", strItemNum);

            if (strItmPrimaryImg != string.Empty)
            {
                strItmPrimaryImg = strItmPrimaryImg + strZoomedParams;
                bPrimeUrlExists = ImageExists(strItmPrimaryImg);
            }

            //strItmPrimaryImg = string.Empty;

            if (bPrimeUrlExists == true)
            {
                bUrlExists = true;
            }
            else
            {
                bUrlExists = ImageExists(strURL);
                if (bUrlExists == true)
                {
                    strItmPrimaryImg = strURL + strZoomedParams;
                }
            }

            if (bUrlExists == true)
            {
                string filePath = strURL;
                // int uAVUbound = int.Parse(ConfigurationManager.AppSettings["intAVNumber"]);
                int scanmoreimages = int.Parse(ConfigurationManager.AppSettings["scanmoreimages"]);
                //newListImages.Add(strURL);
                listImages.Add("_hi");
                //listImgDBSave.Add(imgDBItem + "_hi");

                //for (int u = 1; u < uAVUbound; u++)
                //{
                //    string strSml = "_av" + u.ToString();
                //    strSmlURL = filePath.Replace("_hi", strSml);

                //    bSmlUrlExists = ImageExists(strSmlURL);
                //    if (bSmlUrlExists == true)
                //    {
                //        listImages.Add(strSml);
                //        listImgDBSave.Add(imgDBItem + strSml);
                //        newListImages.Add(strSmlURL);
                //    }
                //}
                int u = 0;
                while (true)
                {
                    u = u + 1;
                    string strSml = "_av" + u.ToString();
                    strSmlURL = filePath.Replace("_hi", strSml);

                    bSmlUrlExists = ImageExists(strSmlURL);
                    if (bSmlUrlExists == true)
                    {
                        listImages.Add(strSml);
                        styleListImages.Add(strSmlURL);
                    }
                    else
                    {
                        u = u + 1;
                        while (scanmoreimages > 0)
                        {
                            strSml = "_av" + u.ToString();
                            strSmlURL = filePath.Replace("_hi", strSml);

                            bSmlUrlExists = ImageExists(strSmlURL);
                            if (bSmlUrlExists == true)
                            {
                                listImages.Add(strSml);
                                styleListImages.Add(strSmlURL);
                                scanmoreimages = int.Parse(ConfigurationManager.AppSettings["scanmoreimages"]);
                            }
                            else
                            {
                                scanmoreimages = scanmoreimages - 1;
                            }
                        }
                        break;
                    }

                }

                //Adding Swatch
                string strSml_s = "_swatch";
                strSmlURL = filePath.Replace("_hi", strSml_s);

                bSmlUrlExists = ImageExists(strSmlURL);
                if (bSmlUrlExists == true)
                {
                    listImages.Add(strSml_s);
                   // listImgDBSave.Add(imgDBItem + strSml_s);
                   // newListImages.Add(strSmlURL);
                }
                //

                //Adding Flat            
                string strSml_f = "_flat";
                strSmlURL = filePath.Replace("_hi", strSml_f);

                bSmlUrlExists = ImageExists(strSmlURL);
                if (bSmlUrlExists == true)
                {
                    listImages.Add(strSml_f);
                   // listImgDBSave.Add(imgDBItem + strSml_f);
                   // newListImages.Add(strSmlURL);
                }
                //
                bShowImg = true;
            }
            else
            {
                bShowImg = false;
                Image52.Visible = true;
            }

        }

        //private void ShowImage4DB(string strItemNum)
        //{
        //    string strURL = string.Empty;
        //    string strItemImage = string.Empty;
        //    string strPicValue = string.Empty;
        //    bool bUrlExists = false;
        //    bool bSmlUrlExists = false;
        //    ArrayList listItems = new ArrayList();
        //    string strSmlURL = string.Empty;
        //    string strBHURL = string.Empty;
        //    string imgDB = string.Empty;
        //    string imgDBItem = string.Empty;

        //    strItmNum = strPath + strItemNum;
        //    //strZoomedParams = "?$400x600$";
        //    strZoomedParams = ConfigurationManager.AppSettings["strZoomParamStd"];
        //    strItemImage = ConfigurationManager.AppSettings["strItemImage"];

        //    strURL = strItemImage.Replace("[picvalue]", strItemNum);

        //    imgDB = ConfigurationManager.AppSettings["strImgPathSplit1"];
        //    strURL = imgDB.Replace("[picvalue]", strItemNum);

        //    //if (strItmPrimaryImg != string.Empty)
        //    //{
        //    //    strItmPrimaryImg = strItmPrimaryImg + strZoomedParams;
        //    //    bPrimeUrlExists = ImageExists(strItmPrimaryImg);
        //    //}
        //    //else
        //    //{

        //    strItmPrimaryImg = string.Empty;
        //    //}

        //    bUrlExists = ImageExists(strURL);

        //    if (bUrlExists == true)
        //    {
        //        string filePath = strURL;
        //        int uAVUbound = int.Parse(ConfigurationManager.AppSettings["intAVNumber"]);
        //        newListImages.Add(strURL);
                
        //        for (int u = 1; u < uAVUbound; u++)
        //        {
        //            string strSml = "_av" + u.ToString();
        //            strSmlURL = filePath.Replace("_hi", strSml);

        //            bSmlUrlExists = ImageExists(strSmlURL);
        //            if (bSmlUrlExists == true)
        //            {
        //                //listImages.Add(strSml);
        //                //listImgDBSave.Add(imgDBItem + strSml);
        //                newListImages.Add(strSmlURL);                       
        //            }
        //        }

        //        //Adding Swatch
        //        string strSml_s = "_swatch";
        //        strSmlURL = filePath.Replace("_hi", strSml_s);

        //        bSmlUrlExists = ImageExists(strSmlURL);
        //        if (bSmlUrlExists == true)
        //        {
        //            swatchImgDB = strSmlURL.Replace("http://img.hottopic.com", "");                    
        //        }
        //        //

        //        //Adding Flat
        //        string strSml_f = "_flat";
        //        strSmlURL = filePath.Replace("_hi", strSml_f);

        //        bSmlUrlExists = ImageExists(strSmlURL);
        //        if (bSmlUrlExists == true)
        //        {
        //            flatImgDB = strSmlURL.Replace("http://img.hottopic.com", "");
        //        }
        //        //
        //        bShowImg = true;
        //    }
        //    else
        //    {
        //        bShowImg = false;
        //        Image52.Visible = true;
        //    }

        //    bShowImg = false;
        //    Image52.Visible = false;

        //}

        /// <summary>
        /// Added by MK for each color swatch entry
        /// </summary>
        /// <param name="strItemNum"></param>
        private void ShowImage4StyleDB(string strItemNum)
        {
            string strURL = string.Empty;
            string strItemImage = string.Empty;
            string strPicValue = string.Empty;
            bool bUrlExists = false;
            bool bSmlUrlExists = false;
            ArrayList listItems = new ArrayList();
            string strSmlURL = string.Empty;
            string strBHURL = string.Empty;
            string imgDB = string.Empty;
            string imgDBItem = string.Empty;

            strItmNum = strPath + strItemNum;
            strZoomedParams = ConfigurationManager.AppSettings["strZoomParamStd"];
            strItemImage = ConfigurationManager.AppSettings["strItemImage"];

            strURL = strItemImage.Replace("[picvalue]", strItemNum);

            imgDB = ConfigurationManager.AppSettings["strImgPathSplit1"];
            strURL = imgDB.Replace("[picvalue]", strItemNum);
            strItmPrimaryImg = string.Empty;
           
            bUrlExists = ImageExists(strURL);

            if (bUrlExists == true)
            {
                string filePath = strURL;
                int uAVUbound = int.Parse(ConfigurationManager.AppSettings["intAVNumber"]);
                int scanmoreimages = int.Parse(ConfigurationManager.AppSettings["scanmoreimages"]);   
                styleListImages.Add(strURL);
                //for (int u = 1; u < uAVUbound; u++)
                //{
                //    string strSml = "_av" + u.ToString();
                //    strSmlURL = filePath.Replace("_hi", strSml);

                //    bSmlUrlExists = ImageExists(strSmlURL);
                //    if (bSmlUrlExists == true)
                //    {  
                //        styleListImages.Add(strSmlURL);
                //    }
                //}
                int u = 0;
                while(true)
                {
                    u = u + 1;
                    string strSml = "_av" + u.ToString();
                    strSmlURL = filePath.Replace("_hi", strSml);

                    bSmlUrlExists = ImageExists(strSmlURL);
                    if (bSmlUrlExists == true)
                    {
                        styleListImages.Add(strSmlURL);
                    }
                    else
                    {
                        u = u + 1;
                        while (scanmoreimages > 0)
                        {
                            strSml = "_av" + u.ToString();
                            strSmlURL = filePath.Replace("_hi", strSml);

                            bSmlUrlExists = ImageExists(strSmlURL);
                            if (bSmlUrlExists == true)
                            {
                                styleListImages.Add(strSmlURL);
                                scanmoreimages = int.Parse(ConfigurationManager.AppSettings["scanmoreimages"]);
                            }
                            else
                            {
                                scanmoreimages = scanmoreimages - 1;
                            }
                        }
                        break;
                    }
                    
                }
                //Adding Swatch
                string strSml_s = "_swatch";
                strSmlURL = filePath.Replace("_hi", strSml_s);

                bSmlUrlExists = ImageExists(strSmlURL);
                if (bSmlUrlExists == true)
                {
                    swatchImgDB = strSmlURL.Replace("http://img.hottopic.com", "");
                    StyleImage si = new StyleImage(); //By MK  
                    styleItemCount = styleItemCount + 1;
                    string FieldName = "swatchIMG";
                    si.StrStyle = strItemNum; si.StrImageURL = swatchImgDB; si.IntImageType = (int)StyleImage.ImageType.Swatch; si.IsPrimaryImage = false; si.IntSortOrder = styleItemCount; si.StrFieldName = FieldName; si.DtCreatedDate = DateTime.Now;
                    lstStyleImages.Add(si);
                }

                //Adding Flat  //By MK  
                string strSml_f = "_flat";
                strSmlURL = filePath.Replace("_hi", strSml_f);

                bSmlUrlExists = ImageExists(strSmlURL);
                if (bSmlUrlExists == true)
                {
                    flatImgDB = strSmlURL.Replace("http://img.hottopic.com", "");
                    StyleImage si = new StyleImage();
                    styleItemCount = styleItemCount + 1;
                    string FieldName = "flatIMG";
                    si.StrStyle = strItemNum; si.StrImageURL = flatImgDB; si.IntImageType = (int)StyleImage.ImageType.Flat; si.IsPrimaryImage = false; si.IntSortOrder = styleItemCount; si.StrFieldName = FieldName; si.DtCreatedDate = DateTime.Now;
                    lstStyleImages.Add(si);
                }
            }           

        }

        //private void setImgForDB()
        //{

        //    primeImgDB = string.Empty;
        //    alt1ImgDB = string.Empty;
        //    alt2ImgDB = string.Empty;
        //    alt3ImgDB = string.Empty;
        //    alt4ImgDB = string.Empty;
        //    alt5ImgDB = string.Empty;
        //    alt6ImgDB = string.Empty;
        //    swatchImgDB = string.Empty;
        //    flatImgDB = string.Empty;
        //    ShowImage4DB(this.itemNum.Text); 
            
        //    if (primeImgPath.Text.ToString() != string.Empty && newListImages.Contains(primeImgPath.Text.ToString()))
        //    {
        //        primeImgDB = primeImgPath.Text.ToString().Replace("http://img.hottopic.com", "");
        //        newListImages.Remove(primeImgPath.Text.ToString());

        //        for (int i = 0; i < newListImages.Count; i++)
        //        {
        //            if (i == 0)
        //            {
        //                alt1ImgDB = newListImages[0].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 1)
        //            {
        //                alt2ImgDB = newListImages[1].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 2)
        //            {
        //                alt3ImgDB = newListImages[2].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 3)
        //            {

        //                alt4ImgDB = newListImages[3].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 4)
        //            {
        //                alt5ImgDB = newListImages[4].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 5)
        //            {
        //                alt6ImgDB = newListImages[5].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //        }

        //    }

        //    else
        //    {
        //        for (int i = 0; i < newListImages.Count; i++)
        //        {                  
        //            if (i == 0)
        //            {
        //                primeImgDB = newListImages[0].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 1)
        //            {
        //                alt1ImgDB = newListImages[1].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 2)
        //            {
        //                alt2ImgDB = newListImages[2].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 3)
        //            {

        //                alt3ImgDB = newListImages[3].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 4)
        //            {
        //                alt4ImgDB = newListImages[4].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 5)
        //            {
        //                alt5ImgDB = newListImages[5].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //            if (i == 6)
        //            {
        //                alt6ImgDB = newListImages[6].ToString().Replace("http://img.hottopic.com", "");
        //            }
        //        }
        //    }


        //}
        
        
        /// <summary>
        /// Added by MK for multipleImage support for each style
        /// </summary>
        /// <param name="strStyleNum"></param>
        ///  

        private void setImgForStyleDB(string strStyleNum)
        {

            primeImgDB = string.Empty;
            //alt1ImgDB = string.Empty;
            //alt2ImgDB = string.Empty;
            //alt3ImgDB = string.Empty;
            //alt4ImgDB = string.Empty;
            //alt5ImgDB = string.Empty;
            //alt6ImgDB = string.Empty;
            swatchImgDB = string.Empty;
            flatImgDB = string.Empty;
            string FieldName = string.Empty;
            ShowImage4StyleDB(strStyleNum);
            string stylePrimeImgPath = string.Empty;
            rmsProgramNum = programNum.Text;
            if(primeImgPath.Text.ToString()!=string.Empty)
                stylePrimeImgPath = primeImgPath.Text.ToString().Replace(rmsProgramNum, strStyleNum);
                        
            StyleImage si = null;
            if (stylePrimeImgPath != string.Empty && styleListImages.Contains(stylePrimeImgPath))
            {
                primeImgDB = stylePrimeImgPath.Replace("http://img.hottopic.com", "");
                styleListImages.Remove(stylePrimeImgPath);
              
                si = new StyleImage();
                styleItemCount = styleItemCount + 1;
                FieldName="primaryimgpath";
                si.StrStyle = strStyleNum; si.StrImageURL = primeImgDB; si.IntImageType = (int)StyleImage.ImageType.Normal; si.IsPrimaryImage = true; si.IntSortOrder = styleItemCount; si.StrFieldName = FieldName; si.DtCreatedDate = DateTime.Now;
                lstStyleImages.Add(si);

                for (int i = 0; i < styleListImages.Count; i++)
                {
                    si = new StyleImage();
                    FieldName = "altimg" + (i+1).ToString();
                    styleItemCount = styleItemCount + 1;                    
                    si.StrStyle = strStyleNum; si.StrImageURL = styleListImages[i].ToString().Replace("http://img.hottopic.com", ""); si.IntImageType = (int)StyleImage.ImageType.Normal; si.IsPrimaryImage = false; si.IntSortOrder = styleItemCount; si.StrFieldName = FieldName; si.DtCreatedDate = DateTime.Now;
                    lstStyleImages.Add(si);                   
                }

            }

            else
            {
                for (int i = 0; i < styleListImages.Count; i++)
                {
                    si = new StyleImage();
                    FieldName = (i == 0 ? "primaryimgpath" : "altimg" + (i).ToString());
                    styleItemCount = styleItemCount + 1;                  
                    si.StrStyle = strStyleNum; si.StrImageURL = styleListImages[i].ToString().Replace("http://img.hottopic.com", ""); si.IntImageType = (int)StyleImage.ImageType.Normal; si.IsPrimaryImage = (i == 0 ? true : false); si.IntSortOrder = styleItemCount; si.StrFieldName = FieldName; si.DtCreatedDate = DateTime.Now;
                    lstStyleImages.Add(si);                   
                }
            }
        }

        /// <summary>
        /// MK Method added to bind Color Shade dropdown
        /// </summary>
        /// <param name="strRMSItemNum"></param>

        private void ColorShadeBindData(string strRMSItemNum)
        {
            SqlConnection Sqlconn = new SqlConnection(dbconn);
            String ColorShadeSql = string.Empty;
            try
            {
                Sqlconn.Open();
                //ColorShadeSql= "select distinct Style,Color from  [RMS_DW_ITEM_MASTER] where program='" + strRMSItemNum + "' order by Color";
                ColorShadeSql = "select distinct Style, Color from  [RMS_DW_ITEM_MASTER] where program IN (select Distinct program from [RMS_DW_ITEM_MASTER] where " +
                             " (program ='" + strRMSItemNum + "' or  Style ='" + strRMSItemNum + "' or SKU ='" + strRMSItemNum + "' )) order by 2 ";


                da1.SelectCommand = new SqlCommand(ColorShadeSql, Sqlconn);
                da1.Fill(ds1, "tblColorShade");
                ddlColorShade.DataSource = ds1.Tables["tblColorShade"];
                ddlColorShade.DataTextField = "Color";
                ddlColorShade.DataValueField = "Style";
                
                ddlColorShade.DataBind();
                ListItem defaultItem = new ListItem("All Color", string.Empty);

                ddlColorShade.Items.Insert(0, defaultItem);
                defaultItem.Selected = true;
                //if (ds1.Tables["tblColorShade"] != null && ds1.Tables["tblColorShade"].Rows.Count > 1)
                //{
                //    ddlColorShade.Items.Insert(0, defaultItem);
                    
                //}
                if (colorcount > 1)
                    defaultItem.Selected = true;
                else
                    ddlColorShade.SelectedValue = rmsItemNum;

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

        }

        private void skuBindData(string strRMSItemNum)
        {
            SqlConnection Sqlconn = new SqlConnection(dbconn);
            String skuSql = string.Empty;
            try
            {
                Sqlconn.Open();
                //Commented by MK to get skus by program or Style
                //skuSql = "select sku skuNum,size sizeId,color colorId,ats ats,unit_retail price from [RMS_DW_ITEM_MASTER] where program='" + strRMSItemNum + "' order by sku"; 
                                
                skuSql = "select sku skuNum,size sizeId,color colorId,ats ats,unit_retail price,isnull(ONLINE_IND,'N') online ,"+
                " case when d.itemnum is null then 'N' else 'Y' end AS PhotoCompleted "+
                "from [RMS_DW_ITEM_MASTER] im left outer join (select itemnum from PhotoTrackEntry where entrytype= '2' and emptrackaccstatus = '4') d on (im.style = d.itemnum) " +
                "where program='" + strRMSItemNum + "' OR style='" + strRMSItemNum + "' OR Style in (select style from [RMS_DW_ITEM_MASTER] im1  where im1.sku='" + strRMSItemNum + "')  order by sku desc";

                da1.SelectCommand = new SqlCommand(skuSql, Sqlconn);
                da1.Fill(ds1, "tblSkuDtl");
                skuGrdView.DataSource = ds1.Tables["tblSkuDtl"];
                skuGrdView.DataBind();
                DataTable  dt = ds1.Tables["tblSkuDtl"].DefaultView.ToTable(true , "colorId");
                if(!dt.Rows[0].IsNull(0))
                colorcount = dt.Rows.Count;
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

        }

        //By MK to bind sku on color shade change
        private void skuBindDataByStyle(string strRMSItemNum,bool IsAllSelected)
        {
            SqlConnection Sqlconn = new SqlConnection(dbconn);
            String skuSql = string.Empty;
            try
            {
                Sqlconn.Open();
                if (IsAllSelected)
                {
                    skuSql = "select  sku skuNum,size sizeId,color colorId,ats ats,unit_retail price,isnull(ONLINE_IND,'N') online , " +
                    "case when d.itemnum is null then 'N' else 'Y' end AS PhotoCompleted " +
                    "from [RMS_DW_ITEM_MASTER] im " +
                    "left outer join (select * from PhotoTrackEntry where entrytype= '2' and emptrackaccstatus = '4') d on (im.style = d.itemnum) " +
                    "where im.program in (select program from [RMS_DW_ITEM_MASTER] im1  where im1.style='" + strRMSItemNum + "')  order by sku desc";
                }
                else
                {
                    skuSql = "select  sku skuNum,size sizeId,color colorId,ats ats,unit_retail price,isnull(ONLINE_IND,'N') online , " +
                   "case when d.itemnum is null then 'N' else 'Y' end AS PhotoCompleted " +
                   "from [RMS_DW_ITEM_MASTER] im " +
                   "left outer join (select * from PhotoTrackEntry where entrytype= '2' and emptrackaccstatus = '4') d on (im.style = d.itemnum) " +
                   "where style='" + strRMSItemNum + "' order by sku";
                }
                da1.SelectCommand = new SqlCommand(skuSql, Sqlconn);
                da1.Fill(ds1, "tblSkuDtl");
                skuGrdView.DataSource = ds1.Tables["tblSkuDtl"];
                skuGrdView.DataBind();

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

        }

        protected void UpdSkuOnLineFlg4Style(string styleId, string onlineFlg)
        {
            SqlConnection _Sqlconn = new SqlConnection(dbconn);

            try
            {

                //update online_ind in RMS_DW_ITEM_MASTER table
                string _updateSql = "update PhotoTracker.dbo.RMS_DW_ITEM_MASTER set ONLINE_IND=@onlineInd WHERE program = @progId ";

                _Sqlconn.Open();
                _cmd = new SqlCommand(_updateSql, _Sqlconn);
                _cmd.Parameters.AddWithValue("@progId", styleId);
                _cmd.Parameters.AddWithValue("@onlineInd", onlineFlg);
                _cmd.CommandType = CommandType.Text;
                _cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                _Sqlconn.Close();
                _Sqlconn.Dispose();
            }

        }

        protected void titleEdit_Click(object sender, EventArgs e)
        {
            /*if (!this.IsPostBack)
            {
                ////get user DataTable from session
                strItmNum = strPath + strItemNum;
                strZoomedParams = "?$215x219$"; //"?$35x53$";
                dtUser = (DataTable)Session["UserProfile"];
                dtPermit = (DataTable)Session["ViewPermission"];


                //ShowItmImage(strItemNum);
                bool bHasPermt = false;

                bHasPermt = AppUser.CheckSecurity("2", dtPermit);

                if (bHasPermt == true)
                {*/
            itemTitle.Enabled = true;
            /*}
            else
            {
                lblNoItemMsg.Text = "You need Edit permission to do the changes";
            }
        }*/

        }


        protected void titleEdit_Click1(object sender, EventArgs e)
        {
            if (itemTitle.ReadOnly != false)
            {
                itemTitle.Enabled = true;
                itemTitle.ReadOnly = false;
                itemTitle.ForeColor = System.Drawing.Color.Black;
                FreeTextBox1.EnableHtmlMode = true;
                FreeTextBox1.EnableViewState = true;
                FreeTextBox1.EnableToolbars = true;
                FreeTextBox1.EnableSsl = true;
                FreeTextBox1.ReadOnly = false;
                FreeTextBox1.Text = itemDtlDesc.Text;
                itemDtlDesc.Text = string.Empty;
                BarcodeWebImage2.Value = this.itemNum.Text;
                strItmPrimaryImg = primeImgPath.Text;
                ShowItmImage(this.itemNum.Text);
            }
            else
            {
                BarcodeWebImage2.Value = this.itemNum.Text;
                strItmPrimaryImg = primeImgPath.Text;
                ShowItmImage(this.itemNum.Text);
            }
        }

        /*protected void dtlDescEdit_Click(object sender, EventArgs e)
        {
            //itemDtlDesc.Enabled = true;
            FreeTextBox1.EnableHtmlMode = true;
            FreeTextBox1.EnableViewState = true;
            FreeTextBox1.EnableToolbars = true;
            FreeTextBox1.EnableSsl = true;
            FreeTextBox1.ReadOnly = false;
            FreeTextBox1.Text = itemDtlDesc.Text;
            itemDtlDesc.Text = string.Empty;
            
            ShowItmImage(this.itemNum.Text);

        }*/

        protected string get_UserID()
        {
            dtUser = (DataTable)Session["UserProfile"];
            string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
            string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();
            string userId = strFirstName.Substring(0, 1) + strLastName;

            return userId;
        }

        protected void save_Click(object sender, EventArgs e)
        {
            strItemNum = this.itemNum.Text;
            String newItmTitle = itemTitle.Text.Trim();
            String newItmDtlDesc = itemDtlDesc.Text.Trim();
            String lastApprDT = apprDate.Text;
            String lastRcptDT = lastRcptDate.Text;

            //String tempFreeText = this;
            //String tempFreeText = FreeTextBox1.

            String lastUpdId = string.Empty;
            String tempText1 = FreeTextBox1.Text;
            SqlConnection Sqlconn = new SqlConnection(dbconn);

            lastUpdId = get_UserID();

            if (tempText1 == string.Empty)
            {
                tempText1 = newItmDtlDesc;
            }

            if (newItmTitle != string.Empty)
            {
                newItmTitle = newItmTitle.Replace(System.Environment.NewLine, " ");
            }

            /*String primIMG = string.Empty;

            foreach (string tmp in listImgDBSave)
            {
                if (this.imgPath.Text!=string.Empty)
                {
                    primIMG=this.imgPath.Text;
                }
                else if (tmp.Contains("_hi"))
                {
                    primIMG=tmp;
                }

                if (primIMG != string.Empty)
                {
                    if (primIMG != tmp)
                    {
                        listAltImgDBSave.Add(tmp);
                    }
                }
            }*/

            try
            {
                //EnableTxt();
                if ((currStatus.Text != null && currStatus.Text != string.Empty && currStatus.Text == "APPROVED") && ((newItmTitle == null || newItmTitle == string.Empty) || (tempText1 == null || tempText1 == string.Empty)))
                {
                    lblNoItemMsg.Text = "Item Enrichment can't be saved without Title and Detail description for approved items";
                    BarcodeWebImage2.Value = strItemNum;
                    strItmPrimaryImg = primeImgPath.Text;
                    ShowItmImage(this.itemNum.Text);
                    //pnlButton.Visible = true;
                    //itemTitle.Enabled = false;
                    //itemTitle.ReadOnly = true;
                    //itemTitle.ForeColor = System.Drawing.Color.Gray;
                    //FreeTextBox1.EnableHtmlMode = false;
                    //FreeTextBox1.EnableViewState = false;
                    //FreeTextBox1.EnableToolbars = false;
                    //FreeTextBox1.EnableSsl = false;
                    //FreeTextBox1.ReadOnly = true;
                }
                else if ((newItmTitle == null || newItmTitle == string.Empty) && (tempText1 == null || tempText1 == string.Empty))
                {

                    lblNoItemMsg.Text = "Item Enrichment can't be saved without Title or Detail description ";
                    //BarcodeWebImage2.Value = strItemNum;
                    //strItmPrimaryImg = primeImgPath.Text;
                    //ShowItmImage(this.itemNum.Text);
                    titleEdit_Click1(sender, e);
                    //pnlButton.Visible = true;
                    //itemTitle.Enabled = false;
                    /*itemTitle.ReadOnly = true;
                    itemTitle.ForeColor = System.Drawing.Color.Gray;
                    FreeTextBox1.EnableHtmlMode = false;
                    FreeTextBox1.EnableViewState = false;
                    FreeTextBox1.EnableToolbars = false;
                    FreeTextBox1.EnableSsl = false;
                    FreeTextBox1.ReadOnly = true;*/

                }
                else if (lastApprDT != string.Empty && currStatus.Text != "APPROVED")
                {
                    lblNoItemMsg.Text = "Approval date can't be updated for non approved Item";
                    BarcodeWebImage2.Value = strItemNum;
                    strItmPrimaryImg = primeImgPath.Text;
                    ShowItmImage(this.itemNum.Text);
                    //titleEdit_Click1(sender, e);                    

                    if (itemTitle.ReadOnly != false)
                    {
                        //itemTitle.Enabled = false;
                        //itemTitle.ReadOnly = true;
                        //itemTitle.ForeColor = System.Drawing.Color.Black;
                        FreeTextBox1.EnableHtmlMode = false;
                        FreeTextBox1.EnableViewState = false;
                        FreeTextBox1.EnableToolbars = false;
                        FreeTextBox1.EnableSsl = false;
                        FreeTextBox1.ReadOnly = true;
                        FreeTextBox1.Text = tempText1;
                        itemDtlDesc.Text = tempText1;
                    }

                }
                else
                {
                    tempText1 = ReplaceHTMLTags(tempText1);

                    String statusSql = string.Empty;
                    String statusSql1 = string.Empty;
                    byte[] dtlDesctxt = Encoding.ASCII.GetBytes(tempText1);
                   // setImgForDB();
                    foreach (ListItem li in this.ddlColorShade.Items)
                    {
                        if (li.Value != "")
                        {
                            styleListImages = new ArrayList();
                            styleItemCount = 0;
                            setImgForStyleDB(li.Value);
                        }
                    }
                    int j = lstStyleImages.Count;
                    //update the status and Title
                    /*statusSql = "update RMS_PH_STYLE_Master set status='SAVED',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,PrimaryImgPath=@primImg,"+
                        "dtlDesc=@dtlDesctxt,temp_dtl_txt=@dtl_text" +
                        " where style='" + this.itemNum.Text + "'";*/

                    //statusSql = "update RMS_PH_STYLE_Master set status='SAVED',publishedInd='N',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,PrimaryImgPath=@primImg," +
                    //    "dtl_Desc_txt=@dtldesctext" +
                    //    " where style='" + this.itemNum.Text + "'";

                    //Commented by MK to remove Images param
                    //statusSql = "update RMS_PH_STYLE_Master set status='SAVED',Online_Flag='N',publishedInd='N',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,PrimaryImgPath=@primImg," +
                    //       "LastPHSaveId=@lSaveId,LastPHSaveDate=@lSaveDate,last_receipt_date=@lastRcptDT,dtl_Desc_txt=@dtldesctext,altIMG1=@alt1Img,altIMG2=@alt2Img,altIMG3=@alt3Img,altIMG4=@alt4Img,altIMG5=@alt5Img,altIMG6=@alt6Img,swatchIMG=@swatchImg" +
                    //       //" where (status is null or ( status is not null and status! ='APPROVED' ) ) and style='" + this.itemNum.Text + "'";
                    //       " where (status is null or ( status is not null and status! ='APPROVED' ) ) and style='" + this.programNum.Text + "'";

                    statusSql = "update RMS_PH_STYLE_Master set status='SAVED',Online_Flag='N',publishedInd='N',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate," +
                           "LastPHSaveId=@lSaveId,LastPHSaveDate=@lSaveDate,last_receipt_date=@lastRcptDT,dtl_Desc_txt=@dtldesctext" +
                        //" where (status is null or ( status is not null and status! ='APPROVED' ) ) and style='" + this.itemNum.Text + "'";
                           " where (status is null or ( status is not null and status! ='APPROVED' ) ) and style='" + this.programNum.Text + "'";

                    string sqlStyleImg = "DELETE from RMS_PH_STYLE_Master_Image where style in (SELECT style from RMS_DW_ITEM_MASTER WHERE program= '" + this.programNum.Text + "') ";

                    
                    //Create a datatable that matches the temp table exactly. (order of columns must match the order in the table)
                    DataTable table = new DataTable();
                    table.Columns.Add(new DataColumn("Style", typeof(string)));
                    table.Columns.Add(new DataColumn("ImageURL", typeof(string)));
                    table.Columns.Add(new DataColumn("ImageType", typeof(int)));
                    table.Columns.Add(new DataColumn("PrimaryImage", typeof(bool)));
                    table.Columns.Add(new DataColumn("SortOrder", typeof(int)));
                    table.Columns.Add(new DataColumn("FieldName", typeof(string)));
                    table.Columns.Add(new DataColumn("CreatedDate", typeof(DateTime)));

                    //Add styleimages in our list to our DataTable
                    foreach (StyleImage si in lstStyleImages)
                    {
                        DataRow row = table.NewRow();
                        row["Style"] = si.StrStyle;
                        row["ImageURL"] = si.StrImageURL;
                        row["ImageType"] = si.IntImageType;
                        row["PrimaryImage"] = si.IsPrimaryImage;
                        row["SortOrder"] = si.IntSortOrder;
                        row["FieldName"] = si.StrFieldName;
                        row["CreatedDate"] = si.DtCreatedDate;
                        table.Rows.Add(row);
                    }



                    Sqlconn.Open();
                    //Execute the command to make a temp table
                    SqlCommand cmd = new SqlCommand(sqlStyleImg, Sqlconn);
                    cmd.ExecuteNonQuery();

                    using (SqlBulkCopy bulk = new SqlBulkCopy(Sqlconn, 
                    SqlBulkCopyOptions.TableLock |
                    SqlBulkCopyOptions.FireTriggers |
                    SqlBulkCopyOptions.UseInternalTransaction,
                    null))
                {
                    bulk.DestinationTableName = "RMS_PH_STYLE_Master_Image";
                    bulk.WriteToServer(table);
                }

                    //Commented by MK to remove image updates
                    //statusSql1 = "update RMS_PH_STYLE_Master set publishedInd='N',Online_Flag='Y',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,PrimaryImgPath=@primImg," +
                    //       "LastPHSaveId=@lSaveId,LastPHSaveDate=@lSaveDate,last_receipt_date=@lastRcptDT,LastPHApprDate=@lastApprDT,LastPHApprID=@lastApprID,dtl_Desc_txt=@dtldesctext,altIMG1=@alt1Img,altIMG2=@alt2Img,altIMG3=@alt3Img,altIMG4=@alt4Img,altIMG5=@alt5Img,altIMG6=@alt6Img,swatchIMG=@swatchImg" +
                    //       " where status is not null and status ='APPROVED' and style='" + this.itemNum.Text + "'";

                statusSql1 = "update RMS_PH_STYLE_Master set publishedInd='N',Online_Flag='Y',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate," +
                       "LastPHSaveId=@lSaveId,LastPHSaveDate=@lSaveDate,last_receipt_date=@lastRcptDT,LastPHApprDate=@lastApprDT,LastPHApprID=@lastApprID,dtl_Desc_txt=@dtldesctext" +
                       " where status is not null and status ='APPROVED' and style='" + this.programNum.Text + "'";

                   //Sqlconn.Open();BY MK
                    _cmd = new SqlCommand(statusSql, Sqlconn);
                    _cmd.Parameters.AddWithValue("@Title", newItmTitle);
                    _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                    _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@lSaveId", lastUpdId);
                    _cmd.Parameters.AddWithValue("@lSaveDate", DateTime.Now);
                    _cmd.Parameters.AddWithValue("@lastRcptDT", lastRcptDT);
                    /*if (lastApprDT == null || lastApprDT == string.Empty)
                    {
                        _cmd.Parameters.AddWithValue("@lastApprDT", DateTime.Now );
                    }
                    else
                    {
                        _cmd.Parameters.AddWithValue("@lastApprDT", lastApprDT);
                    }*/
                    //_cmd.Parameters.AddWithValue("@primImg", this.imgPath.Text);
                    //_cmd.Parameters.AddWithValue("@dtlDesctxt", newItmDtlDesc);
                    _cmd.Parameters.AddWithValue("@dtldesctext", dtlDesctxt);
                    //_cmd.Parameters.AddWithValue("@dtlDesctxt", string.Empty);
                    //_cmd.Parameters.AddWithValue("@dtl_text", dtlDesctxt);    

                    //Commented by MK 
                    //_cmd.Parameters.AddWithValue("@primImg", primeImgDB);
                    //_cmd.Parameters.AddWithValue("@alt1Img", alt1ImgDB);
                    //_cmd.Parameters.AddWithValue("@alt2Img", alt2ImgDB);
                    //_cmd.Parameters.AddWithValue("@alt3Img", alt3ImgDB);
                    //_cmd.Parameters.AddWithValue("@alt4Img", alt4ImgDB);
                    //_cmd.Parameters.AddWithValue("@alt5Img", alt5ImgDB);
                    //_cmd.Parameters.AddWithValue("@alt6Img", alt6ImgDB);
                    //_cmd.Parameters.AddWithValue("@swatchImg", swatchImgDB);

                    _cmd.CommandType = CommandType.Text;
                    _cmd.ExecuteNonQuery();

                    //For approval
                    _cmd1 = new SqlCommand(statusSql1, Sqlconn);
                    _cmd1.Parameters.AddWithValue("@Title", newItmTitle);
                    _cmd1.Parameters.AddWithValue("@lUpdID", lastUpdId);
                    _cmd1.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                    _cmd1.Parameters.AddWithValue("@lSaveId", lastUpdId);
                    _cmd1.Parameters.AddWithValue("@lSaveDate", DateTime.Now);
                    if (lastApprDT == null || lastApprDT == string.Empty)
                    {
                        _cmd1.Parameters.AddWithValue("@lastApprDT", DateTime.Now);
                    }
                    else
                    {
                        _cmd1.Parameters.AddWithValue("@lastApprDT", lastApprDT);
                    }
                    _cmd1.Parameters.AddWithValue("@lastRcptDT", lastRcptDT);
                    _cmd1.Parameters.AddWithValue("@lastApprID", lastUpdId);
                    //_cmd.Parameters.AddWithValue("@primImg", this.imgPath.Text);
                    //_cmd.Parameters.AddWithValue("@dtlDesctxt", newItmDtlDesc);
                    _cmd1.Parameters.AddWithValue("@dtldesctext", dtlDesctxt);
                    //_cmd.Parameters.AddWithValue("@dtlDesctxt", string.Empty);
                    //_cmd.Parameters.AddWithValue("@dtl_text", dtlDesctxt);    

                    //Commented by MK to remove image param
                    //_cmd1.Parameters.AddWithValue("@primImg", primeImgDB);
                    //_cmd1.Parameters.AddWithValue("@alt1Img", alt1ImgDB);
                    //_cmd1.Parameters.AddWithValue("@alt2Img", alt2ImgDB);
                    //_cmd1.Parameters.AddWithValue("@alt3Img", alt3ImgDB);
                    //_cmd1.Parameters.AddWithValue("@alt4Img", alt4ImgDB);
                    //_cmd1.Parameters.AddWithValue("@alt5Img", alt5ImgDB);
                    //_cmd1.Parameters.AddWithValue("@alt6Img", alt6ImgDB);
                    //_cmd1.Parameters.AddWithValue("@swatchImg", swatchImgDB);

                    _cmd1.CommandType = CommandType.Text;
                    _cmd1.ExecuteNonQuery();

                    lblNoItemMsg.Text = "Item # " + this.itemNum.Text + ": " + "saved successfully";

                    //update sku level on line flag
                    //UpdateSkuOnLineFlag(sender, e);


                    EmtyExpItmTxt();
                    txtItemNum.Text = string.Empty;
                    skuGrdView.Visible = false;
                    //ddlColorShade.Visible = false; //MK
                    //itemTitle.Enabled = false;
                    itemTitle.ReadOnly = true;
                    itemTitle.ForeColor = System.Drawing.Color.Gray;
                    FreeTextBox1.EnableHtmlMode = false;
                    FreeTextBox1.EnableViewState = false;
                    FreeTextBox1.EnableToolbars = false;
                    FreeTextBox1.EnableSsl = false;
                    FreeTextBox1.ReadOnly = true;
                    //added below lines by Vijay
                    strFunctionStatus = "SAVEBTN";
                    this.txtItemNum.Text = strItemNum;

                    searchBtnClicked(sender, e);

                }

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                lblNoItemMsg.Text = "Application Error Occured, please contact system Admin , Error : " + ex.Message;
                ShowItmImage(this.itemNum.Text);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

            //searchBtnClicked(sender,e);
            //ShowItmImage(this.itemNum.Text);
            /*if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (dtlDescEdit.Text != null && dtlDescEdit.Text != string.Empty))
            {
                approve.Enabled = true;
            }*/

        }

        private string ReplaceHTMLTags(string strDetailDesc)
        {
            /*strDetailDesc = strDetailDesc.Replace("<STRONG>", "<B>");
            strDetailDesc = strDetailDesc.Replace("<strong>", "<B>");
            strDetailDesc = strDetailDesc.Replace("</STRONG>", "</B>");
            strDetailDesc = strDetailDesc.Replace("</strong>", "</B>");
            strDetailDesc = strDetailDesc.Replace("<EM>", "<I>");
            strDetailDesc = strDetailDesc.Replace("<em>", "<I>");
            strDetailDesc = strDetailDesc.Replace("</EM>", "</I>");
            strDetailDesc = strDetailDesc.Replace("</em>", "</I>");
            //strDetailDesc = strDetailDesc.Replace("<LI>\r\n<DIV align=left>", "<LI>");
            //strDetailDesc = strDetailDesc.Replace("<LI>\r\n<DIV align=right>", "<LI>");
            //strDetailDesc = strDetailDesc.Replace("<LI>\r\n<DIV align=center>", "<LI>");
            //strDetailDesc = strDetailDesc.Replace("<LI>\r\n<DIV align=justify>", "<LI>");
            //strDetailDesc = strDetailDesc.Replace("</DIV>\r\n</LI>", "</LI>");
            //strDetailDesc = strDetailDesc.Replace("&NBSP;", string.Empty);
            strDetailDesc = strDetailDesc.Replace("<DIV>\r\n</DIV>", string.Empty);
            strDetailDesc = strDetailDesc.Replace("<DIV></DIV>", string.Empty);
            strDetailDesc = strDetailDesc.Replace("<LI>", "<LI style=\"LIST-STYLE-POSITION: outside !important; LIST-STYLE-TYPE: disc !important\">");
            strDetailDesc = strDetailDesc.Replace("<li>", "<li style=\"LIST-STYLE-POSITION: outside !important; LIST-STYLE-TYPE: disc !important\">");
            strDetailDesc = strDetailDesc.Replace("</P><P", "</P><BR><P");
            strDetailDesc = strDetailDesc.Replace("</P>\r\n<P", "</P><BR><P");
            strDetailDesc = strDetailDesc.Replace("</p><p", "</p><BR><p");
            strDetailDesc = strDetailDesc.Replace("</p>\r\n<p", "</p><BR><p");*/

            strDetailDesc = strDetailDesc.Replace("<STRONG>", "<b>");
            strDetailDesc = strDetailDesc.Replace("<strong>", "<b>");
            strDetailDesc = strDetailDesc.Replace("</STRONG>", "</b>");
            strDetailDesc = strDetailDesc.Replace("</strong>", "</b>");
            strDetailDesc = strDetailDesc.Replace("<EM>", "<i>");
            strDetailDesc = strDetailDesc.Replace("<em>", "<i>");
            strDetailDesc = strDetailDesc.Replace("</EM>", "</i>");
            strDetailDesc = strDetailDesc.Replace("</em>", "</i>");
            //strDetailDesc = strDetailDesc.Replace("<LI>\r\n<DIV align=left>", "<li>");
            //strDetailDesc = strDetailDesc.Replace("<LI>\r\n<DIV align=right>", "<li>");
            //strDetailDesc = strDetailDesc.Replace("<LI>\r\n<DIV align=center>", "<li>");
            //strDetailDesc = strDetailDesc.Replace("<LI>\r\n<DIV align=justify>", "<li>");
            //strDetailDesc = strDetailDesc.Replace("</DIV>\r\n</LI>", "</li>");
            //strDetailDesc = strDetailDesc.Replace("&NBSP;", string.Empty);
            strDetailDesc = strDetailDesc.Replace("<DIV>\r\n</DIV>", string.Empty);
            strDetailDesc = strDetailDesc.Replace("<DIV></DIV>", string.Empty);
            strDetailDesc = strDetailDesc.Replace("<LI>", "<li style=\"list-style-position: inside !important; list-style-type: disc !important\">");
            strDetailDesc = strDetailDesc.Replace("<li>", "<li style=\"list-style-position: inside !important; list-style-type: disc !important\">");
            strDetailDesc = strDetailDesc.Replace("</P><P", "</p><br><p");
            strDetailDesc = strDetailDesc.Replace("</P>\r\n<P", "</p><br><p");
            strDetailDesc = strDetailDesc.Replace("</p><p", "</p><br><p");
            strDetailDesc = strDetailDesc.Replace("</p>\r\n<p", "</p><br><p");

            return strDetailDesc;
        }

        protected void approve_Click(object sender, EventArgs e)
        {
            strItemNum = this.itemNum.Text;
            String newItmTitle = itemTitle.Text;
            String newItmDtlDesc = itemDtlDesc.Text;
            String lastApprDT = apprDate.Text;
            String lastRcptDT = lastRcptDate.Text;

            String lastUpdId = apprDate.Text;
            String errMsg = string.Empty;
            SqlConnection Sqlconn = new SqlConnection(dbconn);

            lastUpdId = get_UserID();
            String tempText1 = FreeTextBox1.Text;

            if (tempText1 == string.Empty)
            {
                tempText1 = newItmDtlDesc;
            }

            if (newItmTitle != string.Empty)
            {
                newItmTitle = newItmTitle.Replace(System.Environment.NewLine, " ");
            }


            try
            {
                //EnableTxt();
                if ((newItmTitle == null || newItmTitle == string.Empty) || (tempText1 == null || tempText1 == string.Empty))
                {
                    //EnableTxt();
                    lblNoItemMsg.Text = "Item Enrichment can't be approved without Title and Detail description";
                    BarcodeWebImage2.Value = strItemNum;
                    strItmPrimaryImg = primeImgPath.Text;
                    ShowItmImage(this.itemNum.Text);
                    titleEdit_Click1(sender, e);
                    FreeTextBox1.Text = tempText1;
                    itemDtlDesc.Text = tempText1;
                    //itemTitle.Enabled = false;
                    /*itemTitle.ReadOnly = true;
                    itemTitle.ForeColor = System.Drawing.Color.Gray;
                    FreeTextBox1.EnableHtmlMode = false;
                    FreeTextBox1.EnableViewState = false;
                    FreeTextBox1.EnableToolbars = false;
                    FreeTextBox1.EnableSsl = false;
                    FreeTextBox1.ReadOnly = true;*/
                    //pnlButton.Visible = true;
                }

                else
                {
                    tempText1 = ReplaceHTMLTags(tempText1);
                    String statusSql = string.Empty;
                    byte[] dtlDesctxt = Encoding.ASCII.GetBytes(tempText1);
                  //  setImgForDB();
                    foreach (ListItem li in this.ddlColorShade.Items)
                    {
                        if (li.Value != "")
                        {
                            styleListImages = new ArrayList();
                            styleItemCount = 0;
                            setImgForStyleDB(li.Value);
                        }
                    }
                    
                    //update the status and Title
                    //statusSql = "update RMS_PH_STYLE_Master set status='APPROVED',publishedInd='N',Online_Flag='Y',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,LastPHApprDate=@lastApprDT,PrimaryImgPath=@primImg,dtl_Desc_txt=@dtldesctext,LastPHApprID=@lastApprID" +
                    //    " where style='" + this.itemNum.Text + "'";

                    //Commented by MK to remove image param
                    //statusSql = "update RMS_PH_STYLE_Master set status='APPROVED',publishedInd='N',Online_Flag='Y',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,LastPHApprDate=@lastApprDT,PrimaryImgPath=@primImg,dtl_Desc_txt=@dtldesctext,LastPHApprID=@lastApprID" +
                    //        ",LastPHApprUnApprId=@LPHApprUnApprId,LastPHApprUnApprDate=@LPHApprUnApprDate, altIMG1=@alt1Img,altIMG2=@alt2Img,altIMG3=@alt3Img,altIMG4=@alt4Img,altIMG5=@alt5Img,altIMG6=@alt6Img,swatchIMG=@swatchImg" +
                    //        " where style='" + this.itemNum.Text + "'";

                    statusSql = "update RMS_PH_STYLE_Master set status='APPROVED',publishedInd='N',Online_Flag='Y',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,LastPHApprDate=@lastApprDT,dtl_Desc_txt=@dtldesctext,LastPHApprID=@lastApprID" +
                            ",LastPHApprUnApprId=@LPHApprUnApprId,LastPHApprUnApprDate=@LPHApprUnApprDate" +
                            " where style='" + this.programNum.Text + "'";


                   // string sqlStyleImg = " IF EXISTS(SELECT TOP 1 1 from  RMS_PH_STYLE_Master_Image where style='" + this.itemNum.Text + "')" +
                   //"DELETE from RMS_PH_STYLE_Master_Image where style='" + this.itemNum.Text + "' ";

                    string sqlStyleImg = "DELETE from RMS_PH_STYLE_Master_Image where style in (SELECT style from RMS_DW_ITEM_MASTER WHERE program= '" + this.programNum.Text + "') ";
                    //Create a datatable that matches the temp table exactly. (order of columns must match the order in the table)
                    DataTable table = new DataTable();
                    table.Columns.Add(new DataColumn("Style", typeof(string)));
                    table.Columns.Add(new DataColumn("ImageURL", typeof(string)));
                    table.Columns.Add(new DataColumn("ImageType", typeof(int)));
                    table.Columns.Add(new DataColumn("PrimaryImage", typeof(bool)));
                    table.Columns.Add(new DataColumn("SortOrder", typeof(int)));
                    table.Columns.Add(new DataColumn("FieldName", typeof(string)));
                    table.Columns.Add(new DataColumn("CreatedDate", typeof(DateTime)));

                    //Add styleimages in our list to our DataTable
                    foreach (StyleImage si in lstStyleImages)
                    {
                        DataRow row = table.NewRow();
                        row["Style"] = si.StrStyle;
                        row["ImageURL"] = si.StrImageURL;
                        row["ImageType"] = si.IntImageType;
                        row["PrimaryImage"] = si.IsPrimaryImage;
                        row["SortOrder"] = si.IntSortOrder;
                        row["FieldName"] = si.StrFieldName;
                        row["CreatedDate"] = si.DtCreatedDate;
                        table.Rows.Add(row);
                    }


                    Sqlconn.Open();
                    //Execute the command to make a temp table
                    SqlCommand cmd = new SqlCommand(sqlStyleImg, Sqlconn);
                    cmd.ExecuteNonQuery();

                    using (SqlBulkCopy bulk = new SqlBulkCopy(Sqlconn,
                    SqlBulkCopyOptions.TableLock |
                    SqlBulkCopyOptions.FireTriggers |
                    SqlBulkCopyOptions.UseInternalTransaction,
                    null))
                    {
                        bulk.DestinationTableName = "RMS_PH_STYLE_Master_Image";
                        bulk.WriteToServer(table);
                    }

                    //Sqlconn.Open();
                    _cmd = new SqlCommand(statusSql, Sqlconn);
                    _cmd.Parameters.AddWithValue("@Title", newItmTitle);
                    _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                    _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                    if (lastApprDT == null || lastApprDT == string.Empty)
                    {
                        _cmd.Parameters.AddWithValue("@lastApprDT", DateTime.Now);
                    }
                    else
                    {
                        _cmd.Parameters.AddWithValue("@lastApprDT", lastApprDT);
                    }
                    //_cmd.Parameters.AddWithValue("@lastRcptDat", lastRcptDT);
                    //_cmd.Parameters.AddWithValue("@primImg", this.imgPath.Text);
                    //_cmd.Parameters.AddWithValue("@dtlDesctxt", newItmDtlDesc);

                    //Commented by MK to remove image param
                    //_cmd.Parameters.AddWithValue("@primImg", primeImgDB);
                    //_cmd.Parameters.AddWithValue("@alt1Img", alt1ImgDB);
                    //_cmd.Parameters.AddWithValue("@alt2Img", alt2ImgDB);
                    //_cmd.Parameters.AddWithValue("@alt3Img", alt3ImgDB);
                    //_cmd.Parameters.AddWithValue("@alt4Img", alt4ImgDB);
                    //_cmd.Parameters.AddWithValue("@alt5Img", alt5ImgDB);
                    //_cmd.Parameters.AddWithValue("@alt6Img", alt6ImgDB);
                    //_cmd.Parameters.AddWithValue("@swatchImg", swatchImgDB);

                    _cmd.Parameters.AddWithValue("@dtldesctext", dtlDesctxt);
                    _cmd.Parameters.AddWithValue("@lastApprID", lastUpdId);

                    _cmd.Parameters.AddWithValue("@LPHApprUnApprId", lastUpdId);
                    _cmd.Parameters.AddWithValue("@LPHApprUnApprDate", DateTime.Now);

                    _cmd.CommandType = CommandType.Text;
                    try
                    {
                        _cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                        errMsg = ex.Message;
                    }
                    finally
                    {
                        Sqlconn.Close();
                        Sqlconn.Dispose();
                    }

                    if (errMsg == string.Empty)
                    {
                        //UpdSkuOnLineFlg4Style(this.itemNum.Text,"Y");
                        lblNoItemMsg.Text = "Item # " + this.itemNum.Text + ": " + "approved successfully";
                        EmtyExpItmTxt();
                        txtItemNum.Text = string.Empty;
                        skuGrdView.Visible = false;
                       // ddlColorShade.Visible = false; //MK
                        //itemTitle.Enabled = false;
                        itemTitle.ReadOnly = true;
                        itemTitle.ForeColor = System.Drawing.Color.Gray;
                        FreeTextBox1.EnableHtmlMode = false;
                        FreeTextBox1.EnableViewState = false;
                        FreeTextBox1.EnableToolbars = false;
                        FreeTextBox1.EnableSsl = false;
                        FreeTextBox1.ReadOnly = true;
                        //added below lines by Vijay
                        strFunctionStatus = "APPROVEBTN";
                        this.txtItemNum.Text = strItemNum;
                        searchBtnClicked(sender, e);
                    }
                    else
                    {
                        lblNoItemMsg.Text = "Application Error Occured, please contact system Admin , Error : " + errMsg;
                        ShowItmImage(this.itemNum.Text);
                    }
                }

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

            //searchBtnClicked(sender, e);
            //ShowItmImage(this.itemNum.Text);
        }

        protected void unApprove_Click(object sender, EventArgs e)
        {
            strItemNum = this.itemNum.Text;
            String newItmTitle = itemTitle.Text;
            String newItmDtlDesc = itemDtlDesc.Text;
            String lastApprDT = apprDate.Text;

            if (newItmTitle != string.Empty)
            {
                newItmTitle = newItmTitle.Replace(System.Environment.NewLine, " ");
            }

            String lastUpdId = apprDate.Text;
            SqlConnection Sqlconn = new SqlConnection(dbconn);

            lastUpdId = get_UserID();
            String tempText1 = FreeTextBox1.Text;

            if (tempText1 == string.Empty)
            {
                tempText1 = newItmDtlDesc;
            }


            try
            {

                String statusSql = string.Empty;
                byte[] dtlDesctxt = Encoding.ASCII.GetBytes(tempText1);

                //update the status and Title
                //statusSql = "update RMS_PH_STYLE_Master set status='UNAPPROVED',publishedInd='N',Online_Flag='N',phtitle=@Title,LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,PrimaryImgPath=@primImg,dtl_Desc_txt=@dtldesctext,LastPHApprDate=@lastApprDT,LastPHApprID=@lastApprID" +
                //    " where style='" + this.itemNum.Text + "'";

                statusSql = "update RMS_PH_STYLE_Master set status='UNAPPROVED',publishedInd='N',Online_Flag='N',LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,LastPHApprDate=null,LastPHApprID=@lastApprID,LastPHApprUnApprId=@LPHApprUnApprId,LastPHApprUnApprDate=@LPHApprUnApprDate" +
                        " where style='" + this.programNum.Text + "'";

                Sqlconn.Open();
                _cmd = new SqlCommand(statusSql, Sqlconn);
                //_cmd.Parameters.AddWithValue("@Title", this.itemTitle.Text);
                _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                //_cmd.Parameters.AddWithValue("@primImg", this.imgPath.Text);
                //_cmd.Parameters.AddWithValue("@dtlDesctxt", this.itemDtlDesc.Text);
                //_cmd.Parameters.AddWithValue("@dtldesctext", dtlDesctxt);
                //_cmd.Parameters.AddWithValue("@lastApprDT", string.Empty);                
                _cmd.Parameters.AddWithValue("@lastApprID", string.Empty);

                _cmd.Parameters.AddWithValue("@LPHApprUnApprId", lastUpdId);
                _cmd.Parameters.AddWithValue("@LPHApprUnApprDate", DateTime.Now);

                _cmd.CommandType = CommandType.Text;
                _cmd.ExecuteNonQuery();

                //UpdSkuOnLineFlg4Style(this.itemNum.Text, "N");

                lblNoItemMsg.Text = "Item # " + this.itemNum.Text + ": " + "unapproved successfully";
                EmtyExpItmTxt();
                txtItemNum.Text = string.Empty;
                skuGrdView.Visible = false;
                //ddlColorShade.Visible = false; //MK
                //itemTitle.Enabled = false;
                itemTitle.ReadOnly = true;
                itemTitle.ForeColor = System.Drawing.Color.Gray;
                FreeTextBox1.EnableHtmlMode = false;
                FreeTextBox1.EnableViewState = false;
                FreeTextBox1.EnableToolbars = false;
                FreeTextBox1.EnableSsl = false;
                FreeTextBox1.ReadOnly = true;

                //added below lines by Vijay
                strFunctionStatus = "UNAPPROVEBTN";
                this.txtItemNum.Text = strItemNum;
                searchBtnClicked(sender, e);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

            //searchBtnClicked(sender, e);
            //ShowItmImage(this.itemNum.Text);
            /*if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (dtlDescEdit.Text != null && dtlDescEdit.Text != string.Empty))
            {
                approve.Enabled = true;
            }*/


        }

        protected void textChanged1(object sender, EventArgs e)
        {
            if (itemTitle.Text != null && itemTitle.Text != string.Empty)
            {
                save.Enabled = true;
            }
            if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (FreeTextBox1.Text != null && FreeTextBox1.Text != string.Empty))
            {
                approve.Enabled = true;
            }
            ShowItmImage(this.itemNum.Text);
        }
        protected void textChanged2(object sender, EventArgs e)
        {
            if (imgPath.Text != null && imgPath.Text != string.Empty)
            {
                save.Enabled = true;
            }
            ShowItmImage(this.itemNum.Text);
        }
        protected void textChanged3(object sender, EventArgs e)
        {
            if (FreeTextBox1.Text != null && FreeTextBox1.Text != string.Empty)
            {
                save.Enabled = true;
            }
            if ((itemTitle.Text != null && itemTitle.Text != string.Empty) && (FreeTextBox1.Text != null && FreeTextBox1.Text != string.Empty))
            {
                approve.Enabled = true;
            }
            ShowItmImage(this.itemNum.Text);
        }
        /// <summary>
        /// By MK to reload data for selected color
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlColorShade_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strSelectedColor = this.ddlColorShade.SelectedItem.Text.Trim();
            if (strSelectedColor == "All Color")
            {            
                itemNum.Text = this.txtItemNum.Text.Trim();  //rmsItemNum;
                selColor.Text = this.ddlColorShade.Items.FindByValue(itemNum.Text).Text;//rmsSelectedColor;
                skuBindDataByStyle(itemNum.Text, true);
            }
            else
            {
                itemNum.Text = this.ddlColorShade.SelectedValue.Trim(); //rmsItemNum;
                selColor.Text = this.ddlColorShade.SelectedItem.Text.Trim(); //rmsSelectedColor;  
                skuBindDataByStyle(itemNum.Text, false);
            }
            ShowItmImage(itemNum.Text);
            BarcodeWebImage2.Value = this.itemNum.Text;
                FreeTextBox1.Text = itemDtlDesc.Text;
                      
            itemTitle.ReadOnly = true;
            FreeTextBox1.EnableHtmlMode = false;
            FreeTextBox1.EnableViewState = false;
            FreeTextBox1.EnableToolbars = false;
            FreeTextBox1.EnableSsl = false;
            FreeTextBox1.ReadOnly = true;
           
            //return;
            //errString = string.Empty;
            //primeImgPath.Text = string.Empty;
            //int intRetCode = 0;
            ////string strSelectedColor = this.ddlColorShade.SelectedItem.Text.Trim();
            //primeImgChk.Checked = false;
            //if (strFunctionStatus != "SAVEBTN" && strFunctionStatus != "APPROVEBTN" && strFunctionStatus != "UNAPPROVEBTN")
            //{
            //    lblNoItemMsg.Text = string.Empty;
            //    strFunctionStatus = string.Empty;
            //}
            //else
            //{
            //    strFunctionStatus = string.Empty;
            //}

            //if (strSelectedColor == "All Color")
            //{
            //    strItemNum = this.txtItemNum.Text.Trim();
            //    isStyleSelected = false;
            //    intRetCode = GetItemDtl(strItemNum, ref errString);
            //}
            //else
            //{
            //    strItemNum = this.ddlColorShade.SelectedValue.Trim();
            //    isStyleSelected = true;
            //    intRetCode = GetItemDtl(strItemNum, ref errString);

            //}
            //EmtyExpItmTxt();
            //ViewState["StrItemNum"] = strItemNum;
            //if (intRetCode == 0)
            //{
            //    if (intRetCode == 0 && rmsItemNum != string.Empty)
            //    {
            //        programNum.Text = rmsProgramNum;
            //        selColor.Text = rmsSelectedColor;
            //        itemNum.Text = rmsItemNum;
            //        txtRMSDesc.Text = rmsItemDesc;
            //        catDesc.Text = catalogDesc;
            //        BarcodeWebImage2.Visible = true;
            //        BarcodeWebImage2.Value = rmsItemNum;
            //        last_upd_dt_id.Text = lastUpdateDtl;
            //        apprDate.Text = lastApprDate;
            //        lastRcptDate.Text = lastRcptDt;
            //        itemTitle.Text = phTitle;
            //        FreeTextBox1.Text = dtlDescTxt;
            //        itemDtlDesc.Text = dtlDescTxt;
            //        FreeTextBox1.ReadOnly = true;
            //        deptNum.Text = dptNum;
            //        classNum.Text = clsNum;
            //        subClassNum.Text = subclsNum;
            //        if (rmsProgramNum == rmsItemNum)
            //        { titleEdit.Visible = true; }
            //        else { titleEdit.Visible = false; }
            //        skuGrdView.Visible = true;
            //        itemTitle.ReadOnly = true;
            //        FreeTextBox1.EnableHtmlMode = false;
            //        FreeTextBox1.EnableViewState = false;
            //        FreeTextBox1.EnableToolbars = false;
            //        FreeTextBox1.EnableSsl = false;
            //        FreeTextBox1.ReadOnly = true;

            //        if (strSelectedColor == "All Color")
            //            skuBindDataByStyle(rmsItemNum, true);
            //        else
            //            skuBindDataByStyle(strItemNum, false);


            //        if (primaryIMGPath != null && primaryIMGPath != string.Empty)
            //        {
            //            strItmPrimaryImg = strImgHTurl + primaryIMGPath;
            //            primeImgPath.Text = strItmPrimaryImg;
            //        }
            //        ShowItmImage(itemNum.Text);
            //        currStatus.Text = statusCode;
            //        if (statusCode == "APPROVED")
            //        {
            //            unApprove.Enabled = true;
            //            unApprove.CssClass = "buttonRed";
            //            approve.Enabled = false;
            //            approve.CssClass = "buttonYellow";
            //        }
            //        else if ((statusCode == "SAVED" || statusCode == "UNAPPROVED") && (FreeTextBox1.Text != string.Empty && itemTitle.Text != string.Empty))
            //        {
            //            approve.Enabled = true;
            //            approve.CssClass = "buttonRed";
            //            unApprove.Enabled = false;
            //            unApprove.CssClass = "buttonYellow";
            //        }
            //        else
            //        {
            //            approve.Enabled = true;
            //            approve.CssClass = "buttonRed";
            //            unApprove.Enabled = false;
            //            unApprove.CssClass = "buttonYellow";
            //        }

            //    }
            //    else
            //    {
            //        bShowImg = false;
            //        Image52.Visible = true;
            //    }
            //}
            //else
            //{
            //    lblNoItemMsg.Text = errString;
            //}
        }

        /*protected void primeImgChk_CheckedChanged(object sender, EventArgs e)
        {
            if (primeImgChk.Checked == true)
            {
                if (imgPath.Text == string.Empty)
                {
                    lblPriImgerrormsg.Text = "Select the Image before checking Primary Option";
                    lblPriImgerrormsg.Visible = true;
                    primeImgChk.Checked = false;
                    searchBtnClicked(sender, e);
                }
                else
                {
                    primeImgPath.Text = imgPath.Text;
                    lblPriImgerrormsg.Text = string.Empty;
                    lblPriImgerrormsg.Visible = false;
                } 
                 
            }

            if (primeImgChk.Checked == false)
            {
                primeImgPath.Text = string.Empty;
            }

        }*/



        /*protected void txtItemNum_TextChanged(object sender, EventArgs e)
        {
            if (txtItemNum.Text != null && txtItemNum.Text != string.Empty)
            {
                searchbBtn.Enabled = true;
            }
            else
            {
                searchbBtn.Enabled = false;
            }
            ShowItmImage(this.itemNum.Text);
        }*/

    }
}
