CREATE TABLE RMS_PH_STYLE_Master_Image(	
	Style varchar(25) NOT NULL,
	ImageURL varchar(400) NULL,
	ImageType int NOT NULL,  -- 1 - Normal, 2 - Swatch, 3 - Flat, 4 - Video
	PrimaryImage bit NOT NULL,
	SortOrder int NOT NULL,
	CreatedDate datetime NOT NULL,
	StyleImageID int IDENTITY(1,1) NOT NULL,
	CONSTRAINT [XPKStyleMasterImage] PRIMARY KEY CLUSTERED 
	(
		[StyleImageID] ASC
	)	
);
CREATE INDEX IDX_RMS_PH_STYLE_Master_Image_ImageType ON RMS_PH_STYLE_Master_Image(Style, ImageType);
