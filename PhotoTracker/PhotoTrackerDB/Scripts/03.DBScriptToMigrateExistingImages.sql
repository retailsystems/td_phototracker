
BEGIN
SET NOCOUNT ON
	DECLARE @Style VARCHAR(25),
			@Division INT,
			@Status VARCHAR(15),
			@Online_Flag varchar(2),
			@PrimaryImgPath varchar(400),
			@altIMG1 varchar(400),
			@altIMG2 varchar(400),
		    @altIMG3 varchar(400),
			@altIMG4 varchar(400),
			@altIMG5 varchar(400),
			@altIMG6 varchar(400),
			@swatchIMG varchar(400),
			@CreatedDate datetime;

	PRINT 'Migration Starting...';


	DECLARE curStyleMaster CURSOR FAST_FORWARD READ_ONLY
	FOR
		SELECT [Style],[Division],[status],[Online_Flag], [PrimaryImgPath]
			  ,[altIMG1],[altIMG2],[altIMG3],[altIMG4],[altIMG5],[altIMG6],[swatchIMG]
			  ,[CreatedDate]
		FROM RMS_PH_STYLE_Master (nolock)
		WHERE [Style] NOT IN (SELECT distinct img.Style FROM RMS_PH_STYLE_Master_Image img (nolock))
		ORDER BY Style;

 	OPEN curStyleMaster;

	FETCH NEXT
	FROM curStyleMaster
	INTO
			@Style,
			@Division,
			@Status,
			@Online_Flag,
			@PrimaryImgPath,
			@altIMG1,
			@altIMG2,
		    @altIMG3,
			@altIMG4,
			@altIMG5,
			@altIMG6,
			@swatchIMG,
			@CreatedDate;

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

		SET @CreatedDate = ISNULL(@CreatedDate, GETDATE());

		--Primary Image
		IF @PrimaryImgPath IS NOT NULL AND LEN(ISNULL(@PrimaryImgPath,'')) > 0 
		BEGIN 
			INSERT INTO dbo.RMS_PH_STYLE_Master_Image
			   ([Style]
			   ,[ImageURL]
			   ,[ImageType]
			   ,[PrimaryImage]
			   ,[SortOrder]
			   ,[FieldName]
			   ,[CreatedDate])
			VALUES
			   (@Style
			   ,@PrimaryImgPath
			   ,1
			   ,CASE WHEN @PrimaryImgPath LIKE '%_hi' THEN 1 ELSE 0 END
			   ,5
			   ,CASE WHEN @PrimaryImgPath LIKE '%_hi' THEN 'primaryimgpath'
					 WHEN @PrimaryImgPath LIKE '%_av1' THEN 'altimg1'
					 WHEN @PrimaryImgPath LIKE '%_av2' THEN 'altimg2'
					 WHEN @PrimaryImgPath LIKE '%_av3' THEN 'altimg3'
					 WHEN @PrimaryImgPath LIKE '%_av4' THEN 'altimg4'
					 WHEN @PrimaryImgPath LIKE '%_av5' THEN 'altimg5'
					 WHEN @PrimaryImgPath LIKE '%_av6' THEN 'altimg6'
					 ELSE '' END
			   ,@CreatedDate);
		END;

		--Image 1
		IF @altIMG1 IS NOT NULL AND LEN(ISNULL(@altIMG1,'')) > 0 
		BEGIN 
			INSERT INTO dbo.RMS_PH_STYLE_Master_Image
			   ([Style]
			   ,[ImageURL]
			   ,[ImageType]
			   ,[PrimaryImage]
			   ,[SortOrder]
			   ,[FieldName]
			   ,[CreatedDate])
			VALUES
			   (@Style
			   ,@altIMG1
			   ,1
			   ,CASE WHEN @altIMG1 LIKE '%_hi' THEN 1 ELSE 0 END
			   ,10
			   ,CASE WHEN @altIMG1 LIKE '%_hi' THEN 'primaryimgpath'
					 WHEN @altIMG1 LIKE '%_av1' THEN 'altimg1'
					 WHEN @altIMG1 LIKE '%_av2' THEN 'altimg2'
					 WHEN @altIMG1 LIKE '%_av3' THEN 'altimg3'
					 WHEN @altIMG1 LIKE '%_av4' THEN 'altimg4'
					 WHEN @altIMG1 LIKE '%_av5' THEN 'altimg5'
					 WHEN @altIMG1 LIKE '%_av6' THEN 'altimg6'
					 ELSE '' END
			   ,@CreatedDate);
		END;

		--Image 2
		IF @altIMG2 IS NOT NULL AND LEN(ISNULL(@altIMG2,'')) > 0 
		BEGIN 
			INSERT INTO dbo.RMS_PH_STYLE_Master_Image
			   ([Style]
			   ,[ImageURL]
			   ,[ImageType]
			   ,[PrimaryImage]
			   ,[SortOrder]
			   ,[FieldName]
			   ,[CreatedDate])
			VALUES
			   (@Style
			   ,@altIMG2
			   ,1
			   ,CASE WHEN @altIMG2 LIKE '%_hi' THEN 1 ELSE 0 END
			   ,20
			   ,CASE WHEN @altIMG2 LIKE '%_hi' THEN 'primaryimgpath'
					 WHEN @altIMG2 LIKE '%_av1' THEN 'altimg1'
					 WHEN @altIMG2 LIKE '%_av2' THEN 'altimg2'
					 WHEN @altIMG2 LIKE '%_av3' THEN 'altimg3'
					 WHEN @altIMG2 LIKE '%_av4' THEN 'altimg4'
					 WHEN @altIMG2 LIKE '%_av5' THEN 'altimg5'
					 WHEN @altIMG2 LIKE '%_av6' THEN 'altimg6'
					 ELSE '' END
			   ,@CreatedDate);
		END;

		--Image 3
		IF @altIMG3 IS NOT NULL AND LEN(ISNULL(@altIMG3,'')) > 0 
		BEGIN 
			INSERT INTO dbo.RMS_PH_STYLE_Master_Image
			   ([Style]
			   ,[ImageURL]
			   ,[ImageType]
			   ,[PrimaryImage]
			   ,[SortOrder]
			   ,[FieldName]
			   ,[CreatedDate])
			VALUES
			   (@Style
			   ,@altIMG3
			   ,1
			   ,CASE WHEN @altIMG3 LIKE '%_hi' THEN 1 ELSE 0 END
			   ,30
			   ,CASE WHEN @altIMG3 LIKE '%_hi' THEN 'primaryimgpath'
					 WHEN @altIMG3 LIKE '%_av1' THEN 'altimg1'
					 WHEN @altIMG3 LIKE '%_av2' THEN 'altimg2'
					 WHEN @altIMG3 LIKE '%_av3' THEN 'altimg3'
					 WHEN @altIMG3 LIKE '%_av4' THEN 'altimg4'
					 WHEN @altIMG3 LIKE '%_av5' THEN 'altimg5'
					 WHEN @altIMG3 LIKE '%_av6' THEN 'altimg6'
					 ELSE '' END
			   ,@CreatedDate);
		END;


		--Image 4
		IF @altIMG4 IS NOT NULL AND LEN(ISNULL(@altIMG4,'')) > 0 
		BEGIN 
			INSERT INTO dbo.RMS_PH_STYLE_Master_Image
			   ([Style]
			   ,[ImageURL]
			   ,[ImageType]
			   ,[PrimaryImage]
			   ,[SortOrder]
			   ,[FieldName]
			   ,[CreatedDate])
			VALUES
			   (@Style
			   ,@altIMG4
			   ,1
			   ,CASE WHEN @altIMG4 LIKE '%_hi' THEN 1 ELSE 0 END
			   ,40
			   ,CASE WHEN @altIMG4 LIKE '%_hi' THEN 'primaryimgpath'
					 WHEN @altIMG4 LIKE '%_av1' THEN 'altimg1'
					 WHEN @altIMG4 LIKE '%_av2' THEN 'altimg2'
					 WHEN @altIMG4 LIKE '%_av3' THEN 'altimg3'
					 WHEN @altIMG4 LIKE '%_av4' THEN 'altimg4'
					 WHEN @altIMG4 LIKE '%_av5' THEN 'altimg5'
					 WHEN @altIMG4 LIKE '%_av6' THEN 'altimg6'
					 ELSE '' END
			   ,@CreatedDate);
		END;


		--Image 5
		IF @altIMG5 IS NOT NULL AND LEN(ISNULL(@altIMG5,'')) > 0 
		BEGIN 
			INSERT INTO dbo.RMS_PH_STYLE_Master_Image
			   ([Style]
			   ,[ImageURL]
			   ,[ImageType]
			   ,[PrimaryImage]
			   ,[SortOrder]
			   ,[FieldName]
			   ,[CreatedDate])
			VALUES
			   (@Style
			   ,@altIMG5
			   ,1
			   ,CASE WHEN @altIMG5 LIKE '%_hi' THEN 1 ELSE 0 END
			   ,50
			   ,CASE WHEN @altIMG5 LIKE '%_hi' THEN 'primaryimgpath'
					 WHEN @altIMG5 LIKE '%_av1' THEN 'altimg1'
					 WHEN @altIMG5 LIKE '%_av2' THEN 'altimg2'
					 WHEN @altIMG5 LIKE '%_av3' THEN 'altimg3'
					 WHEN @altIMG5 LIKE '%_av4' THEN 'altimg4'
					 WHEN @altIMG5 LIKE '%_av5' THEN 'altimg5'
					 WHEN @altIMG5 LIKE '%_av6' THEN 'altimg6'
					 ELSE '' END
			   ,@CreatedDate);
		END;


		--Image 6
		IF @altIMG6 IS NOT NULL AND LEN(ISNULL(@altIMG6,'')) > 0 
		BEGIN 
			INSERT INTO dbo.RMS_PH_STYLE_Master_Image
			   ([Style]
			   ,[ImageURL]
			   ,[ImageType]
			   ,[PrimaryImage]
			   ,[SortOrder]
			   ,[FieldName]
			   ,[CreatedDate])
			VALUES
			   (@Style
			   ,@altIMG6
			   ,1
			   ,CASE WHEN @altIMG6 LIKE '%_hi' THEN 1 ELSE 0 END
			   ,60
			   ,CASE WHEN @altIMG6 LIKE '%_hi' THEN 'primaryimgpath'
					 WHEN @altIMG6 LIKE '%_av1' THEN 'altimg1'
					 WHEN @altIMG6 LIKE '%_av2' THEN 'altimg2'
					 WHEN @altIMG6 LIKE '%_av3' THEN 'altimg3'
					 WHEN @altIMG6 LIKE '%_av4' THEN 'altimg4'
					 WHEN @altIMG6 LIKE '%_av5' THEN 'altimg5'
					 WHEN @altIMG6 LIKE '%_av6' THEN 'altimg6'
					 ELSE '' END
			   ,@CreatedDate);
		END;


		--Swatch Image
		IF @swatchIMG IS NOT NULL AND LEN(ISNULL(@swatchIMG,'')) > 0 
		BEGIN 
			INSERT INTO dbo.RMS_PH_STYLE_Master_Image
			   ([Style]
			   ,[ImageURL]
			   ,[ImageType]
			   ,[PrimaryImage]
			   ,[SortOrder]
			   ,[FieldName]
			   ,[CreatedDate])
			VALUES
			   (@Style
			   ,@swatchIMG
			   ,2
			   ,0
			   ,70
			   ,'swatchIMG'
			   ,@CreatedDate);
		END;


		FETCH NEXT
		FROM curStyleMaster
		INTO
				@Style,
				@Division,
				@Status,
				@Online_Flag,
				@PrimaryImgPath,
				@altIMG1,
				@altIMG2,
				@altIMG3,
				@altIMG4,
				@altIMG5,
				@altIMG6,
				@swatchIMG,
				@CreatedDate;

	END;

	CLOSE curStyleMaster;
	DEALLOCATE curStyleMaster;

	PRINT 'Migration Completed.';

SET NOCOUNT OFF;
END;
