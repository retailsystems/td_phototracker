USE [PhotoTracker]
GO
/****** Object:  StoredProcedure [dbo].[GetItemDetail]    Script Date: 07/23/2018 22:51:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/**********************************************************
Name: [GetItemDetail]
Author: Mukesh Kumar 7/20/2018
Last Modified by: 
Description: Get Item detail by program,style or sku num.
--Exec [GetItemDetail] @division='5',@itmNum='11291203' , @SearchByStyle=1 --style'11207139'   --sku'11207139'
**********************************************************/


CREATE procedure [dbo].[GetItemDetail]
(
 @division  VARCHAR(25),
 @itmNum  VARCHAR(25),
 @SearchByStyle bit=0
)
as
BEGIN

	DECLARE @programNum VARCHAR(25)
	DECLARE @selColor VARCHAR(25)

	IF(@SearchByStyle=1) 
	BEGIN
		SELECT top 1 @itmNum=style,@programNum=program,@selColor=color from [RMS_DW_ITEM_MASTER] where style=@itmNum

--		IF EXISTS (SELECT TOP 1 1 FROM [RMS_PH_STYLE_Master] im  INNER JOIN [Catalog_Id] ci ON im.division = ci.division 
--		where   ci.division in(Select item from split(@division, ',')) and im.Style=@itmNum)
--		BEGIN
--			SELECT @programNum as programNum,@selColor as selColor,im.Style,im.description rmsItmDesc, ci.description catDesc,isnull(ONLINE_FLAG,'N') ONLINE_FLG , im.department,im.class,im.subclass,im.phtitle,im.primaryimgpath, 
--			im.lastphupdateid,  im.lastphupdatedate,im.lastphapprdate,im.last_receipt_date,im.dtlDesc,im.status,cast(im.dtl_Desc_txt as varchar(max)) as LongDesc
--			FROM [RMS_PH_STYLE_Master] im  INNER JOIN [Catalog_Id] ci ON im.division = ci.division 
--			where   ci.division in(Select item from split(@division, ',')) and im.Style=@itmNum
--		END
--		ELSE
--			SELECT @programNum as programNum,@selColor as selColor,@itmNum as Style,'' as rmsItmDesc, '' as catDesc,
--			'' as ONLINE_FLG , '' as department,'' as class,'' as subclass,'' as phtitle,'' as primaryimgpath, 
--			'' as lastphupdateid,  '' as lastphupdatedate,'' as lastphapprdate,'' as last_receipt_date,'' as dtlDesc,'' as status,
--			'' as LongDesc
			SELECT @programNum as programNum,@selColor as selColor,@itmNum as Style,im.description rmsItmDesc, ci.description catDesc,isnull(ONLINE_FLAG,'N') ONLINE_FLG , im.department,im.class,im.subclass,im.phtitle,im.primaryimgpath, 
			im.lastphupdateid,  im.lastphupdatedate,im.lastphapprdate,im.last_receipt_date,im.dtlDesc,im.status,cast(im.dtl_Desc_txt as varchar(max)) as LongDesc
			FROM [RMS_PH_STYLE_Master] im  INNER JOIN [Catalog_Id] ci ON im.division = ci.division 
			where   ci.division in(Select item from split(@division, ',')) and im.Style=@programNum

			
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT TOP 1 1 FROM [RMS_DW_ITEM_MASTER] WHERE program=@itmNum) 
		BEGIN
		    --SELECT TOP 1 @itmNum=sm.style,@programNum=program,@selColor=color from [RMS_DW_ITEM_MASTER]im INNER JOIN [RMS_PH_STYLE_Master] sm ON im.style=sm.style where im.program=@itmNum order by sm.style
			SELECT TOP 1 @itmNum=sm.style,@programNum=program,@selColor=color from [RMS_DW_ITEM_MASTER]im INNER JOIN [RMS_PH_STYLE_Master] sm ON im.program=sm.style where im.program=@itmNum
			--SELECT TOP 1 @itmNum=style,@programNum=program,@selColor=color from [RMS_DW_ITEM_MASTER]im where im.program=@itmNum OR im.style=@itmNum order by style
		END
		ELSE IF EXISTS (SELECT TOP 1 1 FROM [RMS_DW_ITEM_MASTER] WHERE style=@itmNum)
		BEGIN
			  SELECT top 1 @itmNum=style,@programNum=program,@selColor=color from [RMS_DW_ITEM_MASTER] where style=@itmNum
		END
		ELSE IF EXISTS (SELECT TOP 1 1 FROM [RMS_DW_ITEM_MASTER] WHERE sku=@itmNum)
		BEGIN
			  SELECT top 1 @itmNum=style,@programNum=program,@selColor=color from [RMS_DW_ITEM_MASTER] where sku=@itmNum
		END
			SELECT @programNum as programNum,@selColor as selColor,@itmNum as Style,im.description rmsItmDesc, ci.description catDesc,
			isnull(ONLINE_FLAG,'N') ONLINE_FLG , im.department,im.class,im.subclass,im.phtitle,im.primaryimgpath, 
			im.lastphupdateid,  im.lastphupdatedate,im.lastphapprdate,im.last_receipt_date,im.dtlDesc,im.status,cast(im.dtl_Desc_txt as varchar(max)) as LongDesc
			FROM [RMS_PH_STYLE_Master] im  INNER JOIN [Catalog_Id] ci ON im.division = ci.division 
			where   ci.division in(Select item from split(@division, ',')) and im.Style=@programNum
--		IF EXISTS (SELECT TOP 1 1 FROM [RMS_PH_STYLE_Master] im  INNER JOIN [Catalog_Id] ci ON im.division = ci.division 
--		where   ci.division in(Select item from split(@division, ',')) and im.Style=@itmNum)
--		BEGIN
--			SELECT @programNum as programNum,@selColor as selColor,im.Style,im.description rmsItmDesc, ci.description catDesc,isnull(ONLINE_FLAG,'N') ONLINE_FLG , im.department,im.class,im.subclass,im.phtitle,im.primaryimgpath, 
--			im.lastphupdateid,  im.lastphupdatedate,im.lastphapprdate,im.last_receipt_date,im.dtlDesc,im.status,cast(im.dtl_Desc_txt as varchar(max)) as LongDesc
--			FROM [RMS_PH_STYLE_Master] im  INNER JOIN [Catalog_Id] ci ON im.division = ci.division 
--			where   ci.division in(Select item from split(@division, ',')) and im.Style=@itmNum	
--		END	
--		ELSE IF EXISTS (SELECT TOP 1 1 FROM [RMS_DW_ITEM_MASTER] WHERE program=@itmNum OR style=@itmNum OR sku=@itmNum)
--		BEGIN
--			SELECT @programNum as programNum,@selColor as selColor,@itmNum as Style,'' as rmsItmDesc, '' as catDesc,
--			'' as ONLINE_FLG , '' as department,'' as class,'' as subclass,'' as phtitle,'' as primaryimgpath, 
--			'' as lastphupdateid,  '' as lastphupdatedate,'' as lastphapprdate,'' as last_receipt_date,'' as dtlDesc,'' as status,
--			'' as LongDesc			
--		END
	END
END;