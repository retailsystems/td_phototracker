using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PhotoTracker
{
    public partial class CommentView : System.Web.UI.Page
    {
        protected CommentGroup newCommentGroup;
        protected string strItemNum = string.Empty;
        public DataTable dtUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            strItemNum = Request.QueryString["strItmNum"];
            ViewState["StrItemNum"] = strItemNum;

            LoadComments(strItemNum);

        }

        protected void dlItemView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Write("<script language='javascript'> { self.close() }</script>");
        }

        private void LoadComments(string strItemNum)
        {
            newCommentGroup = CommentGroup.GetCommentsDataTable(strItemNum);

            dlItemView.DataSource = newCommentGroup;

            dlItemView.DataBind();

        }
    }
}
