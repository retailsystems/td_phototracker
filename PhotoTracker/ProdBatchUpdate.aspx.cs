using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using Bytescout.BarCode;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.OracleClient;
using System.ComponentModel;
using System.Text;
using FreeTextBoxControls;

/* Author : Debadatta Patra
 * Change Desc : This is for new web page ProdBatchUpdate as part of Phototracker Phase 3
 * Change Date : 09/27/2016
 */

namespace PhotoTracker
{
    public partial class ProdBatchUpdate : System.Web.UI.Page
    {
        public DataTable dtUser;
        public DataTable dtPermit;
        string strGroupNo = String.Empty;
        public string strItemNum = string.Empty;
        public string strItmNum = string.Empty;


        public string errString = string.Empty;
        public string lastUpdateId = string.Empty;
        public string lastUpdateDate = string.Empty;
        public string lastUpdateDtl = string.Empty;
        public string lastApprDate;
        public string statusCode = string.Empty;
        SqlDataAdapter da1 = new SqlDataAdapter();
        DataSet ds1 = new DataSet();
        SqlCommand _cmd = new SqlCommand();
        SqlCommand _cmd1 = new SqlCommand();
        string dbconn = ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString();

        bool bHasApprovePermt = false;

        public void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////get user DataTable from session

                //bHasPermt = AppUser.CheckSecurity("2", dtPermit);

                setUserRole();

                if (bHasApprovePermt == true)
                {

                    txtItemNum.Visible = true;
                    searchbBtn.Visible = true;
                    slctAll.Checked = false;
                }
                else
                {
                    //no proper permission go back to Login
                    Response.Redirect("./Login.aspx");

                }
            }

        }

        protected void setUserRole()
        {
            dtUser = (DataTable)Session["UserProfile"];
            dtPermit = (DataTable)Session["ViewPermission"];


            if (dtPermit.Rows.Count > 0)
            {
                for (int i = 0; i < (dtPermit.Rows.Count); i++)
                {
                    strGroupNo = dtPermit.Rows[i]["GroupNo"].ToString().Trim();
                    if (strGroupNo == "4")
                    {
                        bHasApprovePermt = true;
                    }
                }
            }
        }


        private void EnableTxt(object sender, EventArgs e)
        {

            apprDate.Visible = true;
            skuGrdView.Visible = true;

        }

        private void ClearAll()
        {
            txtItemNum.Text = string.Empty;
            slctAll.Checked = false;
            apprDate.Text = string.Empty;
            rcvdDate.Text = string.Empty;
            DataTable ds = new DataTable();
            ds = null;
            skuGrdView.DataSource = ds;
            skuGrdView.DataBind();
            lblNoItemMsg.Text = string.Empty;
        }



        //Filter out invalid dates
        private string FilterDate(DateTime dt)
        {
            if (dt > DateTime.Parse("1/1/1980"))
            {
                return dt.ToShortDateString();
            }
            else
            {
                return string.Empty;
            }
        }

        protected void searchBtnClicked(object sender, EventArgs e)
        {

            lblNoItemMsg.Text = string.Empty;
            errString = string.Empty;
            int itmCnt = 0;

            strItemNum = this.txtItemNum.Text.Trim();
            strItemNum = strItemNum.Replace(System.Environment.NewLine, "");
            slctAll.Checked = false;

            setUserRole();
            if (bHasApprovePermt == true)
            {
                approve.Visible = true;
            }
            else
            {
                approve.Visible = false;
            }


            if (strItemNum != "")
            {

                itmCnt = strItemNum.Split(',').Length;
                if (itmCnt < 1000)
                {
                    ViewState["StrItemNum"] = strItemNum;

                    skuBindData(strItemNum);

                    if (errString == string.Empty)
                    {
                        skuGrdView.Visible = true;
                    }
                    else
                    {
                        lblNoItemMsg.Text = errString;
                        ClearAll();
                        txtItemNum.Text = string.Empty;
                        skuGrdView.Visible = false;
                    }

                }
                else
                {
                    ClearAll();
                    lblNoItemMsg.Text = "Batch Search is limited to 999 Items";
                    
                }


            }
            else
            {
                ClearAll();
                lblNoItemMsg.Text = "You must input Item Number";
            }
        }


        private void skuBindData(string strRMSItemNum)
        {
            SqlConnection Sqlconn = new SqlConnection(dbconn);
            String skuSql = string.Empty;
            strRMSItemNum = strRMSItemNum.Replace(System.Environment.NewLine, "");
            strRMSItemNum = strRMSItemNum.Replace(",", "','");
            try
            {
                Sqlconn.Open();
                skuSql = "select style item,description itemDesc,lastPHApprdate apprDt,last_receipt_date lastRcvdDt,status " +
                         " from [RMS_PH_STYLE_Master] im where style in ('" + strRMSItemNum + "')  or  im.Style in (select Program from [RMS_DW_ITEM_MASTER] im1 " +
                         " where im1.sku in ('" + strRMSItemNum + "')) order by style";

                da1.SelectCommand = new SqlCommand(skuSql, Sqlconn);
                da1.Fill(ds1, "tblSkuDtl");
                skuGrdView.DataSource = ds1.Tables["tblSkuDtl"];
                skuGrdView.DataBind();
                foreach (GridViewRow grdRow in skuGrdView.Rows)
                {
                    CheckBox _checkbox;
                    CheckBox _checkbox1;
                    TextBox _ApprDt_textbox;
                    TextBox _LastRcptDt_textbox;
                    foreach (System.Data.DataRow dr in ds1.Tables["tblSkuDtl"].Rows)
                    {
                        if (dr["item"].ToString() == grdRow.Cells[1].Text.ToString())
                        {
                            _checkbox = (CheckBox)grdRow.FindControl("approve");
                            _checkbox1 = (CheckBox)grdRow.FindControl("unApprove");

                            _ApprDt_textbox = (TextBox)grdRow.FindControl("apprDt");
                            _LastRcptDt_textbox = (TextBox)grdRow.FindControl("lastRcvdDt");

                            if (dr["status"].ToString() == "APPROVED")
                            {
                                _checkbox.Checked = true;
                                _checkbox1.Checked = false;
                            }
                            else if (dr["status"].ToString() == "UNAPPROVED")
                            {
                                _checkbox1.Checked = true;
                                _checkbox.Checked = false;
                            }
                            else
                            {
                                _checkbox1.Checked = false;
                                _checkbox.Checked = false;
                            }

                            if (dr["apprDt"].ToString() != null && dr["apprDt"].ToString() != string.Empty)
                            {
                                _ApprDt_textbox.Text = FilterDate(Convert.ToDateTime(dr["apprDt"].ToString()));
                            }

                            if (dr["lastRcvdDt"].ToString() != null && dr["lastRcvdDt"].ToString() != string.Empty)
                            {
                                _LastRcptDt_textbox.Text = FilterDate(Convert.ToDateTime(dr["lastRcvdDt"].ToString()));
                            }


                            //_checkbox1.Attributes.Add("onclick", "return false;");
                            //_checkbox.Attributes.Add("onclick", "return false;");
                            _checkbox1.Enabled = false;
                            _checkbox.Enabled = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                errString = ex.Message;
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

        }
        protected void UpdSkuOnLineFlg4Style(string styleId, string onlineFlg)
        {
            SqlConnection _Sqlconn = new SqlConnection(dbconn);

            try
            {

                //update online_ind in RMS_DW_ITEM_MASTER table
                string _updateSql = "update PhotoTracker.dbo.RMS_DW_ITEM_MASTER set ONLINE_IND=@onlineInd WHERE program = @progId ";

                _Sqlconn.Open();
                _cmd = new SqlCommand(_updateSql, _Sqlconn);
                _cmd.Parameters.AddWithValue("@progId", styleId);
                _cmd.Parameters.AddWithValue("@onlineInd", onlineFlg);
                _cmd.CommandType = CommandType.Text;
                _cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                _Sqlconn.Close();
                _Sqlconn.Dispose();
            }

        }
        protected string get_UserID()
        {
            dtUser = (DataTable)Session["UserProfile"];
            string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
            string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();
            string userId = strFirstName.Substring(0, 1) + strLastName;

            return userId;
        }
        protected void batchSave(object sender, EventArgs e)
        {
            String onlineChk = "N";
            String lastUpdId = string.Empty;
            String apprDat = string.Empty;
            String lastRcptDate = string.Empty;
            String status = string.Empty;
            String lastApprId = string.Empty;
            String lastUnApprId = string.Empty;
            int recSlctd = 0;

            lastUpdId = get_UserID();


            for (int i = 0; i < skuGrdView.Rows.Count; i++)
            {
                // Get the values of textboxes using findControl
                CheckBox chkSlct = (CheckBox)skuGrdView.Rows[i].Cells[0].FindControl("chkSelect");
                string itemId = skuGrdView.Rows[i].Cells[1].Text;
                CheckBox chkAppr = (CheckBox)skuGrdView.Rows[i].Cells[5].FindControl("approve");
                CheckBox chkUnAppr = (CheckBox)skuGrdView.Rows[i].Cells[6].FindControl("unApprove");
                TextBox _LastRcptDt_textbox = (TextBox)skuGrdView.Rows[i].Cells[4].FindControl("lastRcvdDt");
                TextBox _ApprDt_textbox = (TextBox)skuGrdView.Rows[i].Cells[3].FindControl("apprDt");

                apprDat = _ApprDt_textbox.Text;
                lastRcptDate = _LastRcptDt_textbox.Text;

                if (chkSlct.Checked == true)
                {
                    recSlctd = recSlctd+1;
                    SqlConnection _Sqlconn = new SqlConnection(dbconn);

                    if (chkUnAppr.Checked == true)
                    {
                        onlineChk = "N";
                        status = "UNAPPROVED";
                        lastApprId = "";
                        lastUnApprId = lastUpdId;
                        apprDat = "";
                    }

                    if (chkAppr.Checked == true)
                    {
                        onlineChk = "Y";
                        status = "APPROVED";
                        lastApprId = lastUpdId;
                        lastUnApprId = "";

                        if (apprDat == null || apprDat == string.Empty)
                        {
                            apprDat = FilterDate(DateTime.Now);
                        }

                    }

                    if (chkAppr.Checked == false && chkUnAppr.Checked == false)
                    {
                        onlineChk = "N";
                        status = "SAVED";
                        lastApprId = "";
                        lastUnApprId = "";
                        apprDat = "";
                    }

                    try
                    {

                        //update details in RMS_PH_STYLE_Master table
                        string _updateSql = "update PhotoTracker.dbo.RMS_PH_STYLE_Master set ONLINE_Flag=@onlineInd,LastPHUpdateID=@lUpdID," +
                                            " LastPHUpdateDate=@lUpdDate,last_receipt_date=@lastRcptDT,LastPHApprDate=@lastApprDT,status=@statusId, " +
                                            " lastPHApprId=@lstApprId,lastPHApprUnApprId=@lstUApprId,publishedind=@pubInd,LastBtchUpdateID=@lUpdID,LastBtchUpdateDate=@lUpdDate WHERE style = @stylId ";

                        _Sqlconn.Open();
                        _cmd = new SqlCommand(_updateSql, _Sqlconn);
                        _cmd.Parameters.AddWithValue("@stylId", itemId);
                        _cmd.Parameters.AddWithValue("@onlineInd", onlineChk);
                        _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                        _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                        _cmd.Parameters.AddWithValue("@lastRcptDT", lastRcptDate);
                        _cmd.Parameters.AddWithValue("@lastApprDT", apprDat);
                        _cmd.Parameters.AddWithValue("@statusId", status);
                        _cmd.Parameters.AddWithValue("@lstApprId", lastApprId);
                        _cmd.Parameters.AddWithValue("@lstUApprId", lastUnApprId);
                        _cmd.Parameters.AddWithValue("@pubInd", "N");
                        _cmd.CommandType = CommandType.Text;
                        _cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                        lblNoItemMsg.Text = "Application Error Occured, please contact system Admin , Error : " + ex.Message;
                    }
                    finally
                    {
                        _Sqlconn.Close();
                        _Sqlconn.Dispose();
                    }
                    /*if (lblNoItemMsg.Text != string.Empty)
                    {
                        UpdSkuOnLineFlg4Style(itemId, onlineChk);
                    }*/

                }

            }

            searchBtnClicked(sender, e);
            apprDate.Text = string.Empty;
            rcvdDate.Text = string.Empty;
            if (recSlctd > 0)
            {
                lblNoItemMsg.Text = "Changes saved succssfully for " + recSlctd + " Styles";
            }
            else
            {
                lblNoItemMsg.Text = "None of the Styles are selected to save the change";
            }
        }


        protected void slctAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow grdRow in skuGrdView.Rows)
            {
                CheckBox _checkbox;
                _checkbox = (CheckBox)grdRow.FindControl("chkSelect");
                if (slctAll.Checked)
                {
                    _checkbox.Checked = true;
                }
                else
                {
                    _checkbox.Checked = false;
                }
            }

        }

        protected void apprDate_TextChanged(object sender, EventArgs e)
        {
            lblNoItemMsg.Text = string.Empty;
            string slctChk = "N";

            foreach (GridViewRow grdRow in skuGrdView.Rows)
            {
                TextBox _ApprDt_textbox;
                CheckBox _checkbox2;

                _checkbox2 = (CheckBox)grdRow.FindControl("chkSelect");
                _ApprDt_textbox = (TextBox)grdRow.FindControl("apprDt");

                if (_checkbox2.Checked == true)
                {
                    if (apprDate.Text.ToString() != null && apprDate.Text.ToString() != string.Empty)
                    {
                        _ApprDt_textbox.Text = FilterDate(Convert.ToDateTime(apprDate.Text.ToString()));
                    }
                    else
                    {
                        _ApprDt_textbox.Text = string.Empty;
                    }
                    slctChk = "Y";
                }
            }
            if (slctChk == "N")
            {
                lblNoItemMsg.Text = "At Least One Style need to be selected for Approve date apply";
            }

        }

        protected void rcvdDate_TextChanged(object sender, EventArgs e)
        {
            lblNoItemMsg.Text = string.Empty;
            string slctChk = "N";

            foreach (GridViewRow grdRow in skuGrdView.Rows)
            {
                TextBox _LastRcptDt_textbox;
                CheckBox _checkbox2;

                _checkbox2 = (CheckBox)grdRow.FindControl("chkSelect");
                _LastRcptDt_textbox = (TextBox)grdRow.FindControl("lastRcvdDt");

                if (_checkbox2.Checked == true)
                {
                    if (rcvdDate.Text.ToString() != null && rcvdDate.Text.ToString() != string.Empty)
                    {
                        _LastRcptDt_textbox.Text = FilterDate(Convert.ToDateTime(rcvdDate.Text.ToString()));
                    }
                    else
                    {
                        _LastRcptDt_textbox.Text = string.Empty;
                    }
                    slctChk = "Y";
                }
            }
            
            if (slctChk=="N")
            {
                lblNoItemMsg.Text = "At Least One Style need to be selected for Last received date apply";
            }


        }

        protected void unApprove_Click(object sender, EventArgs e)
        {

            lblNoItemMsg.Text = string.Empty;
            string slctChk = "N";

            foreach (GridViewRow grdRow in skuGrdView.Rows)
            {
                CheckBox _checkbox;
                CheckBox _checkbox1;
                CheckBox _checkbox2;                

                _checkbox = (CheckBox)grdRow.FindControl("approve");
                _checkbox1 = (CheckBox)grdRow.FindControl("unApprove");
                _checkbox2 = (CheckBox)grdRow.FindControl("chkSelect");

                if (_checkbox2.Checked == true)
                {
                    _checkbox1.Checked = true;
                    _checkbox.Checked = false;
                    slctChk = "Y";
                }
            }
            if (slctChk=="N")
            {
                lblNoItemMsg.Text = "At Least One Style need to be selected for Unapprove";
            }

        }

        protected void approve_Click(object sender, EventArgs e)
        {
            lblNoItemMsg.Text = string.Empty;
            string slctChk = "N";

            foreach (GridViewRow grdRow in skuGrdView.Rows)
            {
                CheckBox _checkbox;
                CheckBox _checkbox1;
                CheckBox _checkbox2;
                _checkbox = (CheckBox)grdRow.FindControl("approve");
                _checkbox1 = (CheckBox)grdRow.FindControl("unApprove");
                _checkbox2 = (CheckBox)grdRow.FindControl("chkSelect");

                if (_checkbox2.Checked == true)
                {
                    _checkbox1.Checked = false;
                    _checkbox.Checked = true;
                    slctChk = "Y";
                }
            }
            if (slctChk == "N")
            {
                lblNoItemMsg.Text = "At Least One Style need to be selected for Approve";
            }

        }

        protected void clearBtn_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

    }
}
