using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PhotoTracker
{
    public partial class Header : System.Web.UI.UserControl
    {
        //protected System.Web.UI.WebControls.Image Image1;
        protected System.Web.UI.HtmlControls.HtmlInputButton btnReSearch;
        protected System.Web.UI.WebControls.Label lblPermit;
        //protected System.Web.UI.WebControls.LinkButton lnkLogout;
        //protected System.Web.UI.WebControls.Image Image2;
        DataTable dtUser;

        private void Page_Load(object sender, System.EventArgs e)
        {
             //Put user code to initialize the page here
            //if (Session["ViewAccessRoles"] != null)
            if (Session["ViewPermission"] != null)
            {
                //int intPermit = int.Parse(Session["ViewAccessRoles"].ToString());
                //DisplayLocation(intPermit);
                this.lnkLogout.Visible = true;
                this.btnReturnMain.Visible = true;
            }
            else
            {
                this.lnkLogout.Visible = false;
                this.btnReturnMain.Visible = false;
                if (GetCurrentPage() != "Login.aspx")
                {
                    Response.Redirect("./Login.aspx");
                }
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lnkLogout.Click += new System.EventHandler(this.lnkLogout_Click);
            //this.btnReSearch.ServerClick += new System.EventHandler(this.btnReSearch_ServerClick);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        protected void DisplayLocation(int intPermit)
        {
            //DataTable dtUserProfile = (DataTable)Session["UserProfile"];
            //if (dtUserProfile != null)
            //{
            //    switch (intPermit)
            //    {
            //        case 1:
            //            //executive/admin level
            //            this.lblPermit.Text = "View Mode: HQ";
            //            break;
            //        case 2:
            //            this.lblPermit.Text = "View Mode: Region " + Utility.ParseDepartment(dtUserProfile.Rows[0]["Dept"].ToString());
            //            break;
            //        case 3:
            //            this.lblPermit.Text = "View Mode: District " + Utility.ParseDepartment(dtUserProfile.Rows[0]["Dept"].ToString());
            //            break;
            //        case 4:
            //            this.lblPermit.Text = "View Mode: Store " + dtUserProfile.Rows[0]["Location"].ToString();
            //            break;
            //        default:
            //            break;



            //    }
            //}

        }

        private void lnkLogout_Click(object sender, System.EventArgs e)
        {
            dtUser = (DataTable)Session["UserProfile"];
            string strEmployeeID = dtUser.Rows[0]["EmployeeID"].ToString().Trim();
            //string strStoreNum = dtUser.Rows[0]["Location"].ToString().Trim();


            Session.Clear();
            Session.Abandon();
            Response.Redirect("./Login.aspx");

        }

        //private void btnReturnMain_ServerClick(object sender, System.EventArgs e)
        //{

        //    if (GetCurrentPage() != "./TrackMenu.aspx")
        //    {
        //        Response.Redirect("./TrackMenu.aspx");
        //    }
        //}
        private string GetCurrentPage()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;

        }

        protected void btnReturnMain_ServerClick(object sender, EventArgs e)
        {
              if (GetCurrentPage() != "./TrackMenu.aspx")
              {
                  Response.Redirect("./TrackMenu.aspx");
              }
        }

        //protected void btnLogOut_ServerClick(object sender, EventArgs e)
        //{
        //    dtUser = (DataTable)Session["UserProfile"];
        //    string strEmployeeID = dtUser.Rows[0]["EmployeeID"].ToString().Trim();
        //    //string strStoreNum = dtUser.Rows[0]["Location"].ToString().Trim();


        //    Session.Clear();
        //    Session.Abandon();
        //    Response.Redirect("./Login.aspx");
        //}
    }
}
