using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections.Generic;



namespace PhotoTracker
{
    public partial class admin : System.Web.UI.Page
    {
        int GroupId;
        int TrackGroupId;
        string Zombieconn = ConfigurationManager.AppSettings["strEmpSQLConnection"].ToString();
        string PhotoTrackconn = ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnUserAcct_Click(object sender, EventArgs e)
        {

            SqlConnection _Sqlconn = new SqlConnection();
            SqlDataAdapter SQLDataAdapter = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlCommand _cmd = new SqlCommand();
            SqlDataReader _rdr =  null;
            string _sql;
            int ID;
           
            try
            {
                //_Sqlconn = new SqlConnection("Data Source=ZOMBIE;User ID=admin; Password=admin; Initial Catalog=Hottopic2");                
                //_Sqlconn = new SqlConnection(Zombieconn);
                _Sqlconn = new SqlConnection(PhotoTrackconn);
                _Sqlconn.Open();
                    //get employee id from zombie
                    //_sql = "Select EmployeeID, Company from dbo.taUltimateEmployee where NameFirst=upper('" + Firstname.Text + "') and NameLast=upper('" + Lastname.Text + "')";
                _sql = "Select EmployeeID from dbo.Employee where EmployeeID= " + EmpID.Text.ToString();  
                    SQLDataAdapter = new SqlDataAdapter(_sql, _Sqlconn);                
                    SQLDataAdapter.Fill(ds, "tblEmployee");
                    _Sqlconn.Close();
               

                if (ds.Tables["tblEmployee"].Rows.Count > 0)
                {
                    //empoyeeid not found in the zombie /dtdomain
                    lblCreateAccountResults.Text = "EmployeeID did exist on DB, please go ahead to update user instead of creat a new one.";
                }
                else
                {
                    //employeeid found in zombie 
                    //then check if it exists in PhotoTrack system - if "no" then enter else "already exists  message"
                   
                    //_Sqlconn = new SqlConnection("Data Source=casql05dev;User ID=phototracker_user; Password=photo$456; Initial Catalog=PhotoTracker");
                    _Sqlconn = new SqlConnection(PhotoTrackconn);
                    _Sqlconn.Open();

                    //   Employee_Insert
                    //Create an Employee account
                    _cmd = new SqlCommand("Employee_Insert", _Sqlconn);
                    _cmd.CommandType = CommandType.StoredProcedure;
                    //ID = Convert.ToInt32(ds.Tables["tblEmployee"].Rows[0]["EmployeeID"]);
                   
                    _cmd.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.Int, 6, "EmpID"));
                    _cmd.Parameters.Add(new SqlParameter("@EmpFirstName", SqlDbType.VarChar, 20, "FirstName"));
                    _cmd.Parameters.Add(new SqlParameter("@EmpLastName", SqlDbType.VarChar, 20, "LastName"));
                    _cmd.Parameters.Add(new SqlParameter("@EmpPassword", SqlDbType.VarChar, 40, "Password"));
                    //_cmd.Parameters.Add(new SqlParameter("@Division", SqlDbType.Char, 1, "Division"));
                    _cmd.Parameters.Add(new SqlParameter("@Result", SqlDbType.Int, 1, "Result"));
                    _cmd.Parameters["@Result"].Direction = ParameterDirection.Output;
                    
                    _cmd.Parameters[0].Value = EmpID.Text;
                    _cmd.Parameters[1].Value = Firstname.Text;
                    _cmd.Parameters[2].Value = Lastname.Text;
                    _cmd.Parameters[3].Value = Password.Text;
                    //_cmd.Parameters[4].Value = Convert.ToInt32(ds.Tables["tblEmployee"].Rows[0]["Company"]);

                    // execute the command
                    _rdr = _cmd.ExecuteReader();

                    int result = Int32.Parse(_cmd.Parameters["@Result"].Value.ToString());

                    _rdr.Close();
                    if (result == 1)
                    {
                        List<CheckBox> _permission = new List<CheckBox>();
                        _permission.Add(AdminAccess);
                        _permission.Add(PTAccess);
                        _permission.Add(RptAccess);
                        _permission.Add(PrdCpAprAccess);
                        _permission.Add(ProdCpEditAccess);

                        foreach (CheckBox c in _permission)
                        {
                            if (c.Checked == true)
                            {

                                switch (c.ID)
                                {
                                    case "AdminAccess":
                                        GroupId = 1;
                                        break;
                                    case "PTAccess":
                                        GroupId = 2;
                                        break;
                                    case "RptAccess":
                                        GroupId = 3;
                                        break;
                                    case "PrdCpAprAccess":
                                        GroupId = 4;
                                        break;
                                    case "ProdCpEditAccess":
                                        GroupId = 5;
                                        break;
                                    default:
                                        GroupId = 1;
                                        break;
                                }
                                //Employee access insert

                                _cmd = new SqlCommand("EmpAccess_Insert", _Sqlconn);
                                _cmd.CommandType = CommandType.StoredProcedure;

                                _cmd.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.Int, 6, "EmpID"));
                                _cmd.Parameters.Add(new SqlParameter("@GroupNo", SqlDbType.Int, 1, "GroupNo"));
                                _cmd.Parameters.Add(new SqlParameter("@Result", SqlDbType.Int, 1, "Result"));
                                _cmd.Parameters["@Result"].Direction = ParameterDirection.Output;

                                _cmd.Parameters[0].Value = EmpID.Text;
                                _cmd.Parameters[1].Value = GroupId;

                                // execute the command
                                _rdr = _cmd.ExecuteReader();

                                //int result = Int32.Parse(_cmd.Parameters["@Result"].Value.ToString());

                                _rdr.Close();

                                if (result == 1)
                                {
                                    lblCreateAccountResults.Text = "Employee access granted." + result;
                                }
                                else
                                {
                                    lblCreateAccountResults.Text = "Unable to grant Employee access." + result;
                                }
                            }
                        }

                        List<CheckBox> _workgroup = new List<CheckBox>();
                        _workgroup.Add(NewWrkGrp);
                        _workgroup.Add(CopyWrkGrp);
                        _workgroup.Add(PhotoWrkGrp);
                        _workgroup.Add(PHEDWrkGrp);
                        _workgroup.Add(APPRWrkGrp);
                        foreach (CheckBox c in _workgroup)
                        {
                            if (c.Checked == true)
                            {
                                switch (c.ID)
                                {
                                    case "NewWrkGrp":
                                        TrackGroupId = 1;
                                        break;
                                    case "CopyWrkGrp":
                                        TrackGroupId = 2;
                                        break;
                                    case "PhotoWrkGrp":
                                        TrackGroupId = 3;
                                        break;
                                    case "PHEDWrkGrp":
                                        TrackGroupId = 4;
                                        break;
                                    case "APPRWrkGrp":
                                        TrackGroupId = 5;
                                        break;
                                    default:
                                        TrackGroupId = 1;
                                        break;
                                }
                                //Employee Track access insert

                                _cmd = new SqlCommand("EmpTrackAccess_Insert", _Sqlconn);
                                _cmd.CommandType = CommandType.StoredProcedure;

                                _cmd.Parameters.Add(new SqlParameter("@EmployeeID", SqlDbType.Int, 6, "EmpID"));
                                _cmd.Parameters.Add(new SqlParameter("@TrackGroupNo", SqlDbType.Int, 1, "TrackGroupNo"));
                                _cmd.Parameters.Add(new SqlParameter("@Result", SqlDbType.Int, 1, "Result"));
                                _cmd.Parameters["@Result"].Direction = ParameterDirection.Output;

                                _cmd.Parameters[0].Value = EmpID.Text;
                                _cmd.Parameters[1].Value = TrackGroupId;

                                // execute the command
                                _rdr = _cmd.ExecuteReader();

                                //int result = Int32.Parse(_cmd.Parameters["@Result"].Value.ToString());

                                _rdr.Close();

                                if (result == 1)
                                {
                                    lblCreateAccountResults.Text = "Employee Track access granted successfully." + result;
                                }
                                else
                                {
                                    lblCreateAccountResults.Text = "Unable to grant Employee Track access." + result;
                                }
                              

                            }
                        }

                        lblCreateAccountResults.Text = "User account created successfully." + result;

                    }
                    else
                    {


                        lblCreateAccountResults.Text = "Unable to create User account.  Please contact IT." + result;
                    }

                        
                }
                //_rdr.Close();
                _Sqlconn.Close();

               

            }
            catch (SqlException ex)
            {
                lblCreateAccountResults.Text = "Error creating user account."+ex.Message;
                //Console.WriteLine("Problem connecting sql database. " + ex.ToString());
                Response.Write(ex.Message);
            }
        }

        protected void AdminAccess_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        protected void PTAccess_CheckedChanged(object sender, EventArgs e)
        {

           

        }

             
    }
}
