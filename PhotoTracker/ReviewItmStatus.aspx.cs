using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using Bytescout.BarCode;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.OracleClient;
using System.ComponentModel;


namespace PhotoTracker
{
    public partial class ReviewItmStatus: System.Web.UI.Page
    {
        protected Item theGersTrkItem;
        protected ItemEntry theItemEntryInfo;
        protected ItemEntryGroup theItemEntryGroup;
        protected EmpEntryGroup empEntryGroup;
        protected CommentGroup newCommentGroup;
        public DataTable theItemTopInfo;
        public DataTable dtUser;
        public DataTable dtPermit;
        public string strItemNum = string.Empty;
        public int EmployeeID = 0;
        public string strDiv = string.Empty;
        public string strBarCodeDir = Data.strBarCodePath();
        public string strItmNum = string.Empty;
        public string strZoomedParams = string.Empty;
        public bool bShowImg = false;
        string strPath = ConfigurationManager.AppSettings["strImgDirPath"];

        public ArrayList listImages = new ArrayList();
        public OleDbDataReader reader;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////get user DataTable from session
                dtUser = (DataTable)Session["UserProfile"];
                dtPermit = (DataTable)Session["ViewPermission"];

                bool bHasPermt = false;

                bHasPermt = AppUser.CheckSecurity("3", dtPermit);

                if (bHasPermt == true)
                {
                    //ClearAll();

                    pnlUpdate.Visible = true;
                    //BarcodeWebImage2.Visible = false;
                }
                else
                {
                    //no proper permission go back to Login
                    Response.Redirect("./Login.aspx");

                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            bool bItem = false;
            bool bRevEntry = false;
            lblNoItemMsg.Text = string.Empty;
            int intItemExixt = 0;
            //ViewState["StrItemNum"] = strItemNum;
            strItemNum = this.txtItemNum.Text.Trim();

            if (strItemNum != "")
            {
                ClearExpItmTxt();
                //ViewState["StrItemNum"] = strItemNum;

                dtUser = (DataTable)Session["UserProfile"];
                if (dtUser != null)
                {
                    int EmployeeID = int.Parse(dtUser.Rows[0]["EmployeeID"].ToString().Trim());
                    string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
                    string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();
                    string strDiv = dtUser.Rows[0]["Division"].ToString().Trim();

                    //see if item is on PhotoTracking table 
                    intItemExixt = Data.CheckGersItem(strItemNum, EmployeeID);
                    if (intItemExixt == 1)
                    {
                        bItem = LoadGersTrkInfo(strItemNum);
                        bRevEntry = bGetRevEntry(strItemNum);
                        //BarcodeWebImage2.Visible = true;
                        //BarcodeWebImage2.Value = strItemNum;
                        //DataTable empRev = Data.GetRevUsers(strItemNum);
                        if (bRevEntry == true)
                        {
                            //if (bRevEntry == true)
                            //{
                            GetItmTopInfo(strItemNum);
                            LoadItemEntryInfo(strItemNum);
                            //ChangCtrl(EmployeeID, txtItemNum, "SelectShow", strDiv);

                            ShowItmImage(strItemNum);
                            bool bComExist = this.IsComExist(strItemNum);
                            if (bComExist == true)
                            {
                                lblComExist.Text = "There are comments for this item.";
                            }
                            else { lblComExist.Text = string.Empty; }
                            //}
                            //else
                            //{
                            //    lblNoItemMsg.Text = "Item # " + strItemNum + " has not been gone through Receive Process yet. Please wait until it has been entried.";
                            //    ClearAll();
                            //}
                        }
                        else
                        {
                            lblNoItemMsg.Text = "Item # " + strItemNum + " has not been gone through Receive Process yet. Please wait until it has been entried.";
                            ClearAll();
                        }
                    } //end of intItemExixt
                    else if (intItemExixt == 2)
                    {
                        lblNoItemMsg.Text = "Item # " + strItemNum + " does not exist. Please re-enter.";
                        ClearAll();
                    }
                    else
                    {
                        lblNoItemMsg.Text = "There is an error once generating the Item # " + strItemNum + ". Please re-enter.";
                        ClearAll();
                    }
                }
                else
                {
                    lblNoItemMsg.Text = "Could not find employee information. Please re-logon";

                    Response.Redirect("./Login.aspx");
                }
            }
            else
            {
                ClearAll();

                lblNoItemMsg.Text = "You must input Item Number.";
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearAll();
            lblNoItemMsg.Text = string.Empty;
        }
        private void ShowItmImage(string strItemNum)
        {
            string strURL = string.Empty;
            string strItemImage = string.Empty;
            string strPicValue = string.Empty;
            bool bUrlExists = false;
            bool bSmlUrlExists = false;
            bool bColUrlExists = false;
            ArrayList listItems = new ArrayList();
            string strSmlURL = string.Empty;
            string strBHURL = string.Empty;

            strItmNum = strPath + strItemNum;
            strZoomedParams = "?$215x219$";
            strItemImage = ConfigurationManager.AppSettings["strItemImage"];
            strURL = strItemImage.Replace("[picvalue]", strItemNum);
            bUrlExists = ImageExists(strURL);

            if (bUrlExists == true)
            {
                string filePath = strURL;
                int uAVUbound = int.Parse(ConfigurationManager.AppSettings["intAVNumber"]);
                listImages.Add("_hi");

                for (int u = 1; u < uAVUbound; u++)
                {
                    string strSml = "_av" + u.ToString();
                    strSmlURL = filePath.Replace("_hi", strSml);

                    bSmlUrlExists = ImageExists(strSmlURL);
                    if (bSmlUrlExists == true)
                    {
                        listImages.Add(strSml);
                    }
                }
                // added color getting from GERS in
				//if (strItemNum != "")
				//{
				//    reader = GetGERSItemColor(strItemNum);
				//    if (reader.HasRows)
				//    {
				//        reader.Read();

				//        if (reader["COLOR"].ToString() != "")
				//        {
				//            string strColor = "-" + reader["COLOR"].ToString();
				//            //string strColor = "-" + "blue";

				//            strBHURL = filePath.Replace("_hi?$215x219$", strColor);

				//            bColUrlExists = ImageExists(strBHURL);
				//            if (bColUrlExists == true)
				//            {
				//                listImages.Add(strColor);
				//            }
				//        }
				//    }
				//    reader.Close();
				//}
                bShowImg = true;
            }
            else
            {
                bShowImg = false;
            }

        }

        static public OleDbDataReader GetGERSItemColor(string strItemNum)
        {

            OleDbCommand objORACommand = new OleDbCommand();
            OleDbConnection objORAConnection;
            OleDbDataReader objORADataReader;
            try
            {
                objORAConnection = new OleDbConnection(ConfigurationManager.AppSettings["strGERSConn"]);
                string strSQL = "select UDF081 as COLOR "
                        + " from GM_INV.GM_ITM_UDF "
                        + " where ITM_CD = " + strItemNum.ToString();

                objORAConnection.Open();
                objORACommand.Connection = objORAConnection;
                objORACommand.CommandText = strSQL;
                objORADataReader = objORACommand.ExecuteReader();
            }
            catch
            {
                throw;
            }
            finally
            {
            }
            return objORADataReader;
        }
        private bool ImageExists(string imgURL)
        {
            WebRequest webRequest = WebRequest.Create(imgURL);

            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                webResponse.GetResponseStream();
                webResponse.Close();

                return true;
            }

            catch
            {
                return false;
            }
        }


        private DataTable GetEmpTrackAccessRoles(object strEmpID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private DataTable GetEmpTrackAccessRoles(object strEmpID, object strEmpPWD)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        private bool bGetRevEntry(string strItemNum)
        {
            bool bHasRevEntry = false;
            DataTable dtGetRevEntry = new DataTable();
            dtGetRevEntry = Data.GetRevEntry(strItemNum);
            if (dtGetRevEntry.Rows.Count > 0)
            {
                bHasRevEntry = true;
            }
            return bHasRevEntry;

        }
        private bool LoadGersTrkInfo(string strItemNum)
        {
            bool bHasItem = false;
            theGersTrkItem = Item.GetOneItem(strItemNum);
            if ((theGersTrkItem.StrItemNum.ToString() != string.Empty) || (theGersTrkItem.Count > 0))
            {
                txtDescription.Text = theGersTrkItem.StrDesc.ToString();
                txtDivision.Text = theGersTrkItem.StrDivision.ToString();
                txtDept.Text = theGersTrkItem.StrDept.ToString();
                txtClass.Text = theGersTrkItem.StrClass.ToString();
                txtSubclass.Text = theGersTrkItem.StrSubClass.ToString();
                txtPrice.Text = "$" + theGersTrkItem.DblCurrRetlPrice.ToString();
                txtCancelDate.Text = FilterDate(theGersTrkItem.DtCancelDate);
                txtTheme1.Text = theGersTrkItem.StrTheme1.ToString();
                txtTheme2.Text = theGersTrkItem.StrTheme2.ToString();
                txtTheme3.Text = theGersTrkItem.StrTheme3.ToString();
                txtTheme4.Text = theGersTrkItem.StrTheme4.ToString();
                txtTheme5.Text = theGersTrkItem.StrTheme5.ToString();
                txtPriLabel.Text = theGersTrkItem.StrPrivLabel.ToString();
                //rdTopSam.SelectedItem.Text = theGersTrkItem.StrTopSample.
                if (theGersTrkItem.StrTopSample.ToString() == "TOPS")
                {
                    rdTopSam.SelectedValue = "TOPS";
                }
                else if (theGersTrkItem.StrTopSample.ToString() == "SAMPLE")
                {
                    rdTopSam.SelectedValue = "SAMPLE";
                }


                bHasItem = true;
            }

            return bHasItem;
        }

        //private void GetItmTopInfo(string strItemNum)
        //{
        //    string strItmTop = Data.GetItmTopInfo(strItemNum);
        //    if (strItmTop != null)
        //    {
        //        rdTopSam.SelectedValue = strItmTop.ToString().Trim();
        //    }
        //    else
        //    { rdTopSam.SelectedValue = "TOP"; }
        //}
        private void GetItmTopInfo(string strItemNum)
        {
            theItemTopInfo = Data.GetItmTopInfo(strItemNum);
            if (theItemTopInfo.Rows.Count > 0)
            {
                rdTopSam.SelectedValue = theItemTopInfo.Rows[0]["TopSample"].ToString().Trim();
            }
            else
            {
                rdTopSam.SelectedValue = "TOPS";
            }
        }
        private void LoadItemEntryInfo(string strItemNum)
        {
            theItemEntryGroup = ItemEntryGroup.GetItemEntryDataTable(strItemNum);
            if (theItemEntryGroup != null)
            {

                foreach (ItemEntry oneItemEntry in theItemEntryGroup)
                {
                    string empTrkAccStatus = oneItemEntry.IntEmpTrkAccStatus.ToString();

                    if (oneItemEntry.IntEmpTrkAccStatus == 1)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            LoadRecvName(txtReceivIn, oneItemEntry.StrEmpName.ToString());
                            LoadEntryTime(txtSampleDateIn, oneItemEntry.DtTrackDate.ToString());
                            LoadTopSamInfo(rdTopSam, strItemNum);
                        }

                    }
                    else if (oneItemEntry.IntEmpTrkAccStatus == 2)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            LoadDropdownName(ddlCopyIn, oneItemEntry.StrEmpName.ToString(), false);
                            LoadEntryTime(txtCopyDateIn, oneItemEntry.DtTrackDate.ToString());
                        }
                        else
                        {
                            LoadDropdownName(ddlCopyOut, oneItemEntry.StrEmpName.ToString(), false);
                            LoadEntryTime(txtCopyDateOut, oneItemEntry.DtTrackDate.ToString());
                        }
                    }
                    else if (oneItemEntry.IntEmpTrkAccStatus == 3)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            LoadDropdownName(ddlPhotoIn, oneItemEntry.StrEmpName.ToString(), false);
                            LoadEntryTime(txtPhotoDateIn, oneItemEntry.DtTrackDate.ToString());
                        }
                        else
                        {
                            LoadDropdownName(ddlPhotoOut, oneItemEntry.StrEmpName.ToString(), false);
                            LoadEntryTime(txtPhotoDateOut, oneItemEntry.DtTrackDate.ToString());
                        }
                    }
                    else if (oneItemEntry.IntEmpTrkAccStatus == 4)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            LoadDropdownName(ddlPhEditIn, oneItemEntry.StrEmpName.ToString(), false);
                            LoadEntryTime(txtPhEditIn, oneItemEntry.DtTrackDate.ToString());
                        }
                        else
                        {
                            LoadDropdownName(ddlPhEditOut, oneItemEntry.StrEmpName.ToString(), false);
                            LoadEntryTime(txtPhEditOut, oneItemEntry.DtTrackDate.ToString());
                        }
                    }
                    else if (oneItemEntry.IntEmpTrkAccStatus == 5)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            LoadDropdownName(ddlFinalIn, oneItemEntry.StrEmpName.ToString(), false);
                            LoadEntryTime(txtFinalDateIn, oneItemEntry.DtTrackDate.ToString());
                        }
                        else
                        {
                            LoadDropdownName(ddlFinalOut, oneItemEntry.StrEmpName.ToString(), false);
                            LoadEntryTime(txtFinalDateOut, oneItemEntry.DtTrackDate.ToString());
                        }
                    }

                }
            }

        }
        private void ClearExpItmTxt()
        {
            ddlCopyIn.Items.Clear();
            //ddlCopyIn.Enabled = false;
            ddlCopyOut.Items.Clear();
            //ddlCopyOut.Enabled = false;
            ddlPhotoIn.Items.Clear();
            //ddlPhotoIn.Enabled = false;
            ddlPhotoOut.Items.Clear();
            //ddlPhotoOut.Enabled = false;
            ddlPhEditIn.Items.Clear();
            //ddlPhEditIn.Enabled = false;
            ddlPhEditOut.Items.Clear();
            //ddlPhEditOut.Enabled = false;
            ddlFinalIn.Items.Clear();
            //ddlFinalIn.Enabled = false;
            ddlFinalOut.Items.Clear();
            //ddlFinalOut.Enabled = false;

            txtReceivIn.Text = string.Empty;
            //txtReceivIn.Enabled = false;
            txtSampleDateIn.Text = string.Empty;
            //txtSampleDateIn.Enabled = false;
            txtCopyDateIn.Text = string.Empty;
            //txtCopyDateIn.Enabled = false;
            txtCopyDateOut.Text = string.Empty;
            //txtCopyDateOut.Enabled = false;
            txtPhotoDateIn.Text = string.Empty;
            //txtPhotoDateIn.Enabled = false;
            txtPhotoDateOut.Text = string.Empty;
            //txtPhotoDateOut.Enabled = false;
            txtPhEditIn.Text = string.Empty;
            //txtPhEditIn.Enabled = false;
            txtPhEditOut.Text = string.Empty;
            //txtPhEditOut.Enabled = false;
            txtFinalDateIn.Text = string.Empty;
            //txtFinalDateIn.Enabled = false;
            txtFinalDateOut.Text = string.Empty;
            //txtFinalDateOut.Enabled = false;

            txtDivision.Text = string.Empty;
            //txtDivision.Enabled = false;
            txtDescription.Text = string.Empty;
            txtDept.Text = string.Empty;
            //txtDept.Enabled = false;
            txtClass.Text = string.Empty;
            //txtClass.Enabled = false;
            txtSubclass.Text = string.Empty;
            //txtSubclass.Enabled = false;
            txtPrice.Text = string.Empty;
            //txtPrice.Enabled = false;
            txtCancelDate.Text = string.Empty;
            //txtCancelDate.Enabled = false;
            txtTheme1.Text = string.Empty;
            //txtTheme1.Enabled = false;
            txtTheme2.Text = string.Empty;
            //txtTheme2.Enabled = false;
            txtTheme3.Text = string.Empty;
            //txtTheme3.Enabled = false;
            txtTheme4.Text = string.Empty;
            //txtTheme4.Enabled = false;
            txtTheme5.Text = string.Empty;
            //txtTheme5.Enabled = false;
            txtPriLabel.Text = string.Empty;
            //txtPriLabel.Enabled = false;
            lblComExist.Text = string.Empty;
            lblComExist.Enabled = false;

            //imgItem.Visible = false;
            //rdTopSam.SelectedIndex = 0;

        }
        private void ClearAll()
        {
            txtItemNum.Text = string.Empty;
            ClearExpItmTxt();

        }
        private void LoadEntryTime(TextBox txtTimeBox, string strTime)
        {
            txtTimeBox.Text = string.Empty;
            txtTimeBox.Text = strTime;

            //if (strTime != "")
            //{
            //    txtTimeBox.Enabled = false;

            //}
        }
        private void LoadRecvName(TextBox txtRecNameBox, string strName)
        {
            txtRecNameBox.Text = string.Empty;
            txtRecNameBox.Text = strName;

            //if (strName != "")
            //{
            //    txtRecNameBox.Enabled = false;
            //}
        }
        private void LoadTopSamInfo(RadioButtonList rdTopSamChk, string strItemNum)
        {
            DataTable theItmTopInfo = Data.GetItmTopInfo(strItemNum);
            if (theItmTopInfo.Rows.Count > 0)
            {
                rdTopSam.SelectedValue = theItmTopInfo.Rows[0]["TopSample"].ToString().Trim();
                rdTopSam.Enabled = false;
            }
        }
        private void LoadDropdownName(DropDownList ddlList, string strName, bool IsSelected)
        {
            ddlList.Items.Clear();
            ListItem itm = new ListItem();
            itm.Text = strName;
            itm.Value = strName;
            ddlList.Items.Add(itm);
            //if (IsSelected)
            //{
            //    ddlList.Enabled = false;

            //}
        }
        //Filter out invalid dates
        private string FilterDate(DateTime dt)
        {
            if (dt > DateTime.Parse("1/1/1980"))
            {
                return dt.ToShortDateString();
            }
            else
            {
                return "N/A";
            }
        }

        private bool IsComExist(string strItemNum)
        {
            bool bHasComment = false;
            newCommentGroup = CommentGroup.GetCommentsDataTable(strItemNum);
            if (newCommentGroup.Count > 0)
            {
                lblComExist.Enabled = true;

                bHasComment = true;
            }

            return bHasComment;
        }

        protected void txtItemNum_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
