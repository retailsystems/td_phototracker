USE [PhotoTracker]
GO
/****** Object:  StoredProcedure [dbo].[Create_PhotoTrackerIndivProd_Report]    Script Date: 10/11/2010 16:37:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Regina Taylor>
-- Create date: <September 21, 2010>
-- Description:	<Create Photo Tracker Individual Productivity Report>
-- =============================================
ALTER PROCEDURE [dbo].[Create_PhotoTrackerIndivProd_Report]
	-- Add the parameters for the stored procedure here

	@BRAND_TYPE	NVARCHAR(1),
 	@EMPLOYEE_NAME	NVARCHAR(100), 
	@START_DATE	DATETIME,  
	@END_DATE DATETIME, 
	@PROCESS_SELECTION	NVARCHAR(1) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @EMPLOYEE_FULLNAME	NVARCHAR(100) 
DECLARE @EMPLOYEE_FIRSTNAME	NVARCHAR(50) 
DECLARE @EMPLOYEE_LASTNAME	NVARCHAR(50) 
DECLARE @EMPLOYEE_ID		INT 

DECLARE @NextString NVARCHAR(40)
DECLARE @Pos INT
DECLARE @NextPos INT
DECLARE @String NVARCHAR(40)
DECLARE @Delimiter NVARCHAR(40)

DECLARE @AreaReceivedValue	NVARCHAR(1) 
DECLARE @AreaCopyValue		NVARCHAR(1) 
DECLARE @AreaPhotoValue		NVARCHAR(1) 
DECLARE @AreaPhotoEditValue	NVARCHAR(1) 
DECLARE @AreaApproverValue	NVARCHAR(1) 

DECLARE @CheckedBox		NVARCHAR (6)
DECLARE @UnCheckedBox	NVARCHAR (6)
 
DECLARE @AreaReceived	NVARCHAR(6) 
DECLARE @AreaCopy		NVARCHAR(6) 
DECLARE @AreaPhoto		NVARCHAR(6) 
DECLARE @AreaPhotoEdit	NVARCHAR(6) 
DECLARE @AreaApprover	NVARCHAR(6) 
DECLARE @AreaAll		NVARCHAR(6) 


SET @AreaReceivedValue	= '1'
SET @AreaCopyValue		= '2'  
SET @AreaPhotoValue		= '3'
SET @AreaPhotoEditValue	= '4'
SET @AreaApproverValue	= '5'
SET @AreaAll			= '6'


SET @CheckedBox		= '[X]'
SET @UnCheckedBox	= '[ ]'

SET @AreaReceived	= @UnCheckedBox
SET @AreaCopy		= @UnCheckedBox
SET @AreaPhoto		= @UnCheckedBox
SET @AreaPhotoEdit	= @UnCheckedBox
SET @AreaApprover	= @UnCheckedBox


CREATE  TABLE #MyTempTable (
ItemNum			NVARCHAR(12)
,ItemDesc		NVARCHAR(50)
,ItemDivision	TINYINT
,AreaReceived	NVARCHAR(6)
,AreaCopy		NVARCHAR(6)
,AreaPhoto		NVARCHAR(6)
,AreaPhotoEdit	NVARCHAR(6)
,AreaApprover	NVARCHAR(6)
--,EmployeeID		INT
--,EmpFirstName	NVARCHAR(20)
--,EmpLastName	NVARCHAR(20)
 )

CREATE  TABLE #MyTempTable2 (
ItemNum			NVARCHAR(12)
,ItemDesc		NVARCHAR(50)
,ItemDivision	TINYINT
 )

CREATE  TABLE #MyTempTable3  (
EmpID INT 
,EmpFirstname NVARCHAR(50)
,EmpLastname NVARCHAR(50)
,EmpDiv NVARCHAR(1)
 )

INSERT INTO #MyTempTable3 (
EmpID  
,EmpFirstname  
,EmpLastname 
,EmpDiv  
)

SELECT DISTINCT Emp.EmployeeID
,UPPER(RTRIM(Emp.EmpFirstName)) 
,UPPER(RTRIM(Emp.EmpLastName)) 
,Emp.Division
FROM Employee  AS emp

SET @EMPLOYEE_FULLNAME = @EMPLOYEE_NAME

SET @Delimiter = ' '
SET @Pos = CHARINDEX (@Delimiter,@EMPLOYEE_FULLNAME)
SET @EMPLOYEE_FIRSTNAME = SUBSTRING(@EMPLOYEE_FULLNAME,1,@Pos - 1)
SET @EMPLOYEE_LASTNAME = SUBSTRING(@EMPLOYEE_FULLNAME,@Pos + 1,LEN(@EMPLOYEE_FULLNAME) - @Pos)

SET @EMPLOYEE_ID = (SELECT EmpID 
FROM #MyTempTable3 
WHERE UPPER(EmpFirstName) = UPPER(@EMPLOYEE_FIRSTNAME)
AND UPPER(EmpLastName) = UPPER(@EMPLOYEE_LASTNAME))

--select @EMPLOYEE_ID

INSERT INTO #MyTempTable (
ItemNum 
--EmployeeID 
--,ItemDesc  
----,ItemDivision  
--,AreaReceived
--,AreaCopy
--,AreaPhoto
--,AreaPhotoEdit
--,AreaApprover  
)

  
SELECT	 dbo.PhotoTrackEntry.ItemNum
FROM dbo.PhotoTrackEntry 
WHERE TrackDate BETWEEN @START_DATE AND @END_DATE
AND (dbo.PhotoTrackEntry.EmployeeId =  @EMPLOYEE_ID)  

 
UPDATE  #MyTempTable  
SET AreaReceived	= @UnCheckedBox, 
	AreaCopy		= @UnCheckedBox, 
	AreaPhoto		= @UnCheckedBox,
	AreaPhotoEdit	= @UnCheckedBox,
	AreaApprover	= @UnCheckedBox 

--SELECT * FROM #MyTempTable 
--ORDER BY ItemNum ASC

INSERT INTO #MyTempTable2 (
ItemNum  
,ItemDesc  
,ItemDivision  
)

SELECT distinct dbo.PhotoTracking.itemnum,dbo.PhotoTracking.Description, dbo.PhotoTracking.division
FROM dbo.PhotoTracking 
RIGHT JOIN dbo.PhotoTrackEntry 
ON (dbo.PhotoTracking.ItemNum  = dbo.PhotoTrackEntry.ItemNum)


UPDATE #MyTempTable
    SET #MyTempTable.ItemDesc = #MyTempTable2.ItemDesc 
FROM #MyTempTable, #MyTempTable2
    WHERE #MyTempTable.ItemNum =#MyTempTable2.ItemNum

UPDATE #MyTempTable
    SET #MyTempTable.ItemDivision = #MyTempTable2.ItemDivision
FROM #MyTempTable, #MyTempTable2
    WHERE #MyTempTable.ItemNum =#MyTempTable2.ItemNum


IF @PROCESS_SELECTION= @AreaAll --'6'
BEGIN
UPDATE  #MyTempTable  
SET AreaReceived	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    WHERE #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	AND PhotoTrackEntry.EmpTrackAccStatus = @AreaReceivedValue -- '1'

UPDATE  #MyTempTable  
SET AreaCopy	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    WHERE #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	AND PhotoTrackEntry.EmpTrackAccStatus = @AreaCopyValue -- '2'

UPDATE  #MyTempTable  
SET @AreaPhoto 	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    WHERE #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	AND PhotoTrackEntry.EmpTrackAccStatus = @AreaPhotoValue -- '3'

UPDATE  #MyTempTable  
SET @AreaPhotoEdit	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    WHERE #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum  
	AND PhotoTrackEntry.EmpTrackAccStatus = @AreaPhotoEditValue --'4'

UPDATE  #MyTempTable  
SET @AreaApprover	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    WHERE #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	AND PhotoTrackEntry.EmpTrackAccStatus = @AreaApproverValue --'5' 
END

ELSE IF @PROCESS_SELECTION = @AreaReceivedValue --'1'
BEGIN
UPDATE  #MyTempTable  
SET AreaReceived	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    WHERE #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	AND PhotoTrackEntry.EmpTrackAccStatus = @AreaReceivedValue -- '1'
END
ELSE IF @PROCESS_SELECTION = @AreaCopyValue --'2'
BEGIN

UPDATE  #MyTempTable  
set AreaCopy	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    WHERE #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	AND PhotoTrackEntry.EmpTrackAccStatus = @AreaCopyValue -- '2'
END
ELSE IF @PROCESS_SELECTION= @AreaPhotoValue --'3'
BEGIN

UPDATE  #MyTempTable  
SET @AreaPhoto 	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    WHERE #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	AND PhotoTrackEntry.EmpTrackAccStatus = @AreaPhotoValue -- '3'
END
ELSE IF @PROCESS_SELECTION = @AreaPhotoEditValue --'4'
BEGIN

UPDATE  #MyTempTable  
SET @AreaPhotoEdit	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    WHERE #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum  
	AND PhotoTrackEntry.EmpTrackAccStatus = @AreaPhotoEditValue --'4'
END
ELSE IF @PROCESS_SELECTION = @AreaApproverValue --'5'
BEGIN

UPDATE  #MyTempTable  
SET @AreaApprover	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    WHERE #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	AND PhotoTrackEntry.EmpTrackAccStatus = @AreaApproverValue --'5' 
END


SELECT DISTINCT ItemNum,ItemDesc,ItemDivision 
				,AreaReceived,AreaCopy,AreaPhoto,AreaPhotoEdit,AreaApprover
FROM #MyTempTable
WHERE ItemDivision = @BRAND_TYPE
ORDER BY ItemNum ASC

--SELECT * FROM #MyTempTable2
--WHERE dbo.PhotoTrackEntry.EmployeeId = '11999'
--ORDER BY ItemNum ASC

--SELECT * FROM #MyTempTable3

END
