USE [PhotoTracker]
GO
/****** Object:  StoredProcedure [dbo].[Create_PhotoTrackerItemStatus_Report]    Script Date: 10/12/2010 10:51:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Regina Taylor>
-- Create date: <September 15, 2010>
-- Description:	<Creates PhotoTracker Item Status Report>
-- =============================================
ALTER PROCEDURE [dbo].[Create_PhotoTrackerItemStatus_Report]  
	-- Add the parameters for the stored procedure here

	@BRAND_TYPE	NVARCHAR(1), 
	@PROCESS_SELECTION	NVARCHAR(1), --INT, 
	@START_DATE	DATETIME, --NVARCHAR(50), 
	@END_DATE	DATETIME --NVARCHAR(50) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

DECLARE @AreaReceivedValue	NVARCHAR(1) 
DECLARE @AreaCopyValue		NVARCHAR(1) 
DECLARE @AreaPhotoValue		NVARCHAR(1) 
DECLARE @AreaPhotoEditValue	NVARCHAR(1) 
DECLARE @AreaApproverValue	NVARCHAR(1) 

DECLARE @CheckedBox		NVARCHAR (6)
DECLARE @UnCheckedBox	NVARCHAR (6)
 
DECLARE @AreaReceived	NVARCHAR(6) 
DECLARE @AreaCopy		NVARCHAR(6) 
DECLARE @AreaPhoto		NVARCHAR(6) 
DECLARE @AreaPhotoEdit	NVARCHAR(6) 
DECLARE @AreaApprover	NVARCHAR(6) 
DECLARE @AreaAll		NVARCHAR(6) 


SET @AreaReceivedValue	= '1'
SET @AreaCopyValue		= '2'  
SET @AreaPhotoValue		= '3'
SET @AreaPhotoEditValue	= '4'
SET @AreaApproverValue	= '5'
SET @AreaAll			= '6'


SET @CheckedBox		= '[X]'
SET @UnCheckedBox	= '[ ]'

SET @AreaReceived	= @UnCheckedBox
SET @AreaCopy		= @UnCheckedBox
SET @AreaPhoto		= @UnCheckedBox
SET @AreaPhotoEdit	= @UnCheckedBox
SET @AreaApprover	= @UnCheckedBox


CREATE  TABLE #MyTempTable (
ItemNum			NVARCHAR(12)
,ItemDesc		NVARCHAR(50)
,ItemDivision	TINYINT
,AreaReceived	NVARCHAR(6)
,AreaCopy		NVARCHAR(6)
,AreaPhoto		NVARCHAR(6)
,AreaPhotoEdit	NVARCHAR(6)
,AreaApprover	NVARCHAR(6)
 )

CREATE  TABLE #MyTempTable2 (
ItemNum			NVARCHAR(12)
,ItemDesc		NVARCHAR(50)
,ItemDivision	TINYINT
 )


INSERT INTO #MyTempTable (
ItemNum  
--,ItemDesc  
----,ItemDivision  
--,AreaReceived
--,AreaCopy
--,AreaPhoto
--,AreaPhotoEdit
--,AreaApprover  
)


SELECT	 dbo.PhotoTrackEntry.ItemNum 
FROM dbo.PhotoTrackEntry 
where TrackDate between @START_DATE and @END_DATE

 
UPDATE  #MyTempTable  
set AreaReceived	= @UnCheckedBox, 
	AreaCopy		= @UnCheckedBox, 
	AreaPhoto		= @UnCheckedBox,
	AreaPhotoEdit	= @UnCheckedBox,
	AreaApprover	= @UnCheckedBox 


INSERT INTO #MyTempTable2 (
ItemNum  
,ItemDesc  
,ItemDivision  
)

select distinct dbo.PhotoTracking.itemnum,dbo.PhotoTracking.Description, dbo.PhotoTracking.division
from dbo.PhotoTracking 
right join dbo.PhotoTrackEntry 
on (dbo.PhotoTracking.ItemNum  = dbo.PhotoTrackEntry.ItemNum)


update #MyTempTable
    set #MyTempTable.ItemDesc = #MyTempTable2.ItemDesc 
FROM #MyTempTable, #MyTempTable2
    where #MyTempTable.ItemNum =#MyTempTable2.ItemNum

update #MyTempTable
    set #MyTempTable.ItemDivision = #MyTempTable2.ItemDivision
FROM #MyTempTable, #MyTempTable2
    where #MyTempTable.ItemNum =#MyTempTable2.ItemNum

IF @PROCESS_SELECTION= @AreaAll --'6'
BEGIN
UPDATE  #MyTempTable  
set AreaReceived	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    where #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	and PhotoTrackEntry.EmpTrackAccStatus = @AreaReceivedValue -- '1'

UPDATE  #MyTempTable  
set AreaCopy	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    where #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	and PhotoTrackEntry.EmpTrackAccStatus = @AreaCopyValue -- '2'

UPDATE  #MyTempTable  
set @AreaPhoto 	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    where #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	and PhotoTrackEntry.EmpTrackAccStatus = @AreaPhotoValue -- '3'

UPDATE  #MyTempTable  
set @AreaPhotoEdit	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    where #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum  
	and PhotoTrackEntry.EmpTrackAccStatus = @AreaPhotoEditValue --'4'

UPDATE  #MyTempTable  
set @AreaApprover	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    where #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	and PhotoTrackEntry.EmpTrackAccStatus = @AreaApproverValue --'5' 
END

ELSE IF @PROCESS_SELECTION = @AreaReceivedValue --'1'
BEGIN
UPDATE  #MyTempTable  
set AreaReceived	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    where #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	and PhotoTrackEntry.EmpTrackAccStatus = @AreaReceivedValue -- '1'
END
ELSE IF @PROCESS_SELECTION = @AreaCopyValue --'2'
BEGIN

UPDATE  #MyTempTable  
set AreaCopy	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    where #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	and PhotoTrackEntry.EmpTrackAccStatus = @AreaCopyValue -- '2'
END
ELSE IF @PROCESS_SELECTION= @AreaPhotoValue --'3'
BEGIN

UPDATE  #MyTempTable  
set @AreaPhoto 	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    where #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	and PhotoTrackEntry.EmpTrackAccStatus = @AreaPhotoValue -- '3'
END
ELSE IF @PROCESS_SELECTION = @AreaPhotoEditValue --'4'
BEGIN

UPDATE  #MyTempTable  
set @AreaPhotoEdit	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    where #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum  
	and PhotoTrackEntry.EmpTrackAccStatus = @AreaPhotoEditValue --'4'
END
ELSE IF @PROCESS_SELECTION = @AreaApproverValue --'5'
BEGIN

UPDATE  #MyTempTable  
set @AreaApprover	= @CheckedBox
FROM PhotoTrackEntry,#MyTempTable
    where #MyTempTable.ItemNum =PhotoTrackEntry.ItemNum
	and PhotoTrackEntry.EmpTrackAccStatus = @AreaApproverValue --'5' 
END


SELECT DISTINCT ItemNum,ItemDesc,ItemDivision,
				AreaReceived,AreaCopy,AreaPhoto,AreaPhotoEdit,AreaApprover	 
FROM #MyTempTable
WHERE ItemDivision = @BRAND_TYPE
ORDER BY ItemNum ASC

--SELECT * FROM #MyTempTable2
--ORDER BY ItemNum ASC



END
