using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using Bytescout.BarCode;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.OracleClient;
using System.ComponentModel;



namespace PhotoTracker
{
    public partial class PhotoTrackingNew : System.Web.UI.Page
    {
        protected Item theGersTrkItem;
        protected ItemEntry theItemEntryInfo;
        protected ItemEntryGroup theItemEntryGroup;
        protected EmpEntryGroup empEntryGroup;
        protected CommentGroup newCommentGroup;
        public DataTable theItemTopInfo;
        public DataTable dtUser;
        public DataTable dtPermit;
        public string strItemNum = string.Empty;
        public int EmployeeID = 0;
        public string strDiv = string.Empty;
        public string strBarCodeDir = Data.strBarCodePath();
        public string strItmNum = string.Empty;
        public string strZoomedParams = string.Empty;
        public bool bShowImg = false;
        string strPath = ConfigurationManager.AppSettings["strImgDirPath"];
        string strVideoPath = ConfigurationManager.AppSettings["strVideoDirPath"];
        public string strTopSamp = string.Empty;
        string dbconn = ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString();
        SqlCommand _cmd = new SqlCommand();
        SqlCommand _cmdrdr = new SqlCommand();
        public string strItmPrimaryImg = string.Empty;
        string strImgHTurl = ConfigurationManager.AppSettings["strImgHTurl"];
        public string scrNeedClear = string.Empty;


        public ArrayList listImages = new ArrayList();
        public ArrayList listImgDBSave = new ArrayList();
        public ArrayList listAltImgDBSave = new ArrayList();
        public ArrayList newListImages = new ArrayList();

        public string primeImgDB = string.Empty;
        public string alt1ImgDB = string.Empty;
        public string alt2ImgDB = string.Empty;
        public string alt3ImgDB = string.Empty;
        public string alt4ImgDB = string.Empty;
        public string alt5ImgDB = string.Empty;
        public string alt6ImgDB = string.Empty;
        public string swatchImgDB = string.Empty;
        public string primaryIMGPath = string.Empty;


        public OleDbDataReader reader;

        public void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////get user DataTable from session
                strItmNum = strPath + strItemNum;
                //strZoomedParams = "?$215x219$"; //"?$35x53$";
                strZoomedParams = ConfigurationManager.AppSettings["strZoomParamMed"];
                dtUser = (DataTable)Session["UserProfile"];
                dtPermit = (DataTable)Session["ViewPermission"];


                // ShowItmImage(strItemNum);
                bool bHasPermt = false;

                bHasPermt = AppUser.CheckSecurity("2", dtPermit);

                if (bHasPermt == true)
                {

                    pnlUpdate.Visible = true;
                    //itemTitle.Enabled = false;
                    Image52.Visible = false;


                    ////imgItem.Visible = false;
                }
                else
                {
                    //no proper permission go back to Login
                    Response.Redirect("./Login.aspx");

                }
            }

        }


        private bool ImageExists(string imgURL)
        {
            WebRequest webRequest = WebRequest.Create(imgURL);

            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                webResponse.GetResponseStream();
                webResponse.Close();

                return true;
            }

            catch
            {
                return false;
            }
        }
        private bool VideoExists(string videoURL)
        {
            WebRequest webRequest = WebRequest.Create(videoURL);

            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                webResponse.GetResponseStream();
                webResponse.Close();

                return true;
            }

            catch
            {
                return false;
            }
        }

        //private bool CheckItemOnDB(string strItemNum)
        //{
        //    bool bHasItem = false;

        //    return true;
        //}

        private bool LoadGersTrkInfo(string strItemNum)
        {
            bool bHasItem = false;
            theGersTrkItem = Item.GetOneItem(strItemNum);
            if (theGersTrkItem.StrItemNum.ToString() != string.Empty)
            {
                txtDescription.Text = theGersTrkItem.StrDesc.ToString();
                txtDivision.Text = theGersTrkItem.StrDivision.ToString();
                txtDept.Text = theGersTrkItem.StrDept.ToString();
                txtClass.Text = theGersTrkItem.StrClass.ToString();
                txtSubclass.Text = theGersTrkItem.StrSubClass.ToString();
                txtPrice.Text = "$" + theGersTrkItem.DblCurrRetlPrice.ToString();
                txtCancelDate.Text = FilterDate(theGersTrkItem.DtCancelDate);
                txtTheme1.Text = theGersTrkItem.StrTheme1.ToString();
                txtTheme2.Text = theGersTrkItem.StrTheme2.ToString();
                txtTheme3.Text = theGersTrkItem.StrTheme3.ToString();
                txtTheme4.Text = theGersTrkItem.StrTheme4.ToString();
                txtTheme5.Text = theGersTrkItem.StrTheme5.ToString();
                txtPriLabel.Text = theGersTrkItem.StrPrivLabel.ToString();
                //rdTopSam.SelectedItem.Text = theGersTrkItem.StrTopSample.
                apprDt.Text = theGersTrkItem.StrLastApprDt.ToString();
                //imgPath.Text = theGersTrkItem.StrPrimIMGPath.ToString();
                itemTitle.Text = theGersTrkItem.StrItemTitle.ToString();

                if (theGersTrkItem.StrPrimIMGPath.ToString() != null && theGersTrkItem.StrPrimIMGPath.ToString() != string.Empty)
                {
                    strItmPrimaryImg = strImgHTurl + theGersTrkItem.StrPrimIMGPath.ToString();
                    primeImgPath.Text = strItmPrimaryImg;
                }

                if (theGersTrkItem.StrPhCmpltInd.ToString() == "N")
                {
                    phComplete.Checked = false;
                }
                else
                {
                    phComplete.Checked = true;
                }
                if (theGersTrkItem.StrPhEditCmpltdInd.ToString() == "N")
                {
                    phEditComplete.Checked = false;
                }
                else
                {
                    phEditComplete.Checked = true;
                }
                if (theGersTrkItem.StrVDExistInd.ToString() == "N")
                {
                    videoChkd.Checked = false;
                }
                else
                {
                    videoChkd.Checked = true;
                }

                if (theGersTrkItem.StrTopSample.ToString() == "TOPS")
                {
                    rdTopSam.SelectedValue = "TOPS";
                }
                else if (theGersTrkItem.StrTopSample.ToString() == "SAMPLE")
                {
                    rdTopSam.SelectedValue = "SAMPLE";
                }
                else
                {
                    //rdTopSam.SelectedValue = "TOPS"; 
                    if (Session["ViewTopSamp"] != null)
                    //|| (Session["ViewTopSamp"] != string.Empty)) 
                    {

                        rdTopSam.SelectedValue = (string)Session["ViewTopSamp"];
                    }
                    else
                    {
                        rdTopSam.SelectedValue = "TOPS";
                    }
                }
                bHasItem = true;
            }

            return bHasItem;
        }
        //private void GetItmTopInfo(string strItemNum)
        //{
        //    string strItmTop = Data.GetItmTopInfo(strItemNum);
        //    if (strItmTop != null)
        //    {
        //        rdTopSam.SelectedValue = strItmTop.ToString().Trim();

        //    }
        //}
        private void GetItmTopInfo(string strItemNum)
        {
            DataTable theItmTopInfo = Data.GetItmTopInfo(strItemNum);
            if (theItmTopInfo.Rows.Count > 0)
            {
                if (theItmTopInfo.Rows[0]["TopSample"] != null && (theItmTopInfo.Rows[0]["TopSample"].ToString() != null && theItmTopInfo.Rows[0]["TopSample"].ToString() != string.Empty))
                {
                    rdTopSam.SelectedValue = theItmTopInfo.Rows[0]["TopSample"].ToString().Trim();
                }
            }
            else
            {
                rdTopSam.SelectedValue = "TOPS";
            }
        }

        private void LoadItemEntryInfo(string strItemNum)
        {
            theItemEntryGroup = ItemEntryGroup.GetItemEntryDataTable(strItemNum);
            if (theItemEntryGroup != null)
            {
                foreach (ItemEntry oneItemEntry in theItemEntryGroup)
                {
                    string empTrkAccStatus = oneItemEntry.IntEmpTrkAccStatus.ToString();

                    if (oneItemEntry.IntEmpTrkAccStatus == 1)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            LoadRecvName(txtReceivIn, oneItemEntry.StrEmpName.ToString());
                            LoadEntryTime(txtSampleDateIn, oneItemEntry.DtTrackDate.ToString());
                            LoadTopSamInfo(rdTopSam, strItemNum);
                        }

                    }
                    else if (oneItemEntry.IntEmpTrkAccStatus == 6)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            LoadRecvName(txtStylistChkIn, oneItemEntry.StrEmpName.ToString());
                            LoadEntryTime(txtStylistChkInDt, oneItemEntry.DtTrackDate.ToString());
                            stlstChk.Checked = true;
                        }
                        else
                        {
                            stlstChk.Checked = false;
                        }

                    }
                    else if (oneItemEntry.IntEmpTrkAccStatus == 2)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            //LoadDropdownName(ddlCopyIn, oneItemEntry.StrEmpName.ToString(), false);
                            //LoadEntryTime(txtCopyDateIn, oneItemEntry.DtTrackDate.ToString());
                        }
                        else
                        {
                            //LoadDropdownName(ddlCopyOut, oneItemEntry.StrEmpName.ToString(), false);
                            //LoadEntryTime(txtCopyDateOut, oneItemEntry.DtTrackDate.ToString());
                        }
                    }
                    else if (oneItemEntry.IntEmpTrkAccStatus == 3)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            //LoadDropdownName(ddlPhotoIn, oneItemEntry.StrEmpName.ToString(), false);
                            //LoadEntryTime(txtPhotoDateIn, oneItemEntry.DtTrackDate.ToString());
                        }
                        else
                        {
                            LoadDropdownName(ddlPhotoOut, oneItemEntry.StrEmpName.ToString(), false);
                            LoadEntryTime(txtPhotoDateOut, oneItemEntry.DtTrackDate.ToString());
                        }
                    }
                    else if (oneItemEntry.IntEmpTrkAccStatus == 4)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            //LoadDropdownName(ddlPhEditIn, oneItemEntry.StrEmpName.ToString(), false);
                            //LoadEntryTime(txtPhEditIn, oneItemEntry.DtTrackDate.ToString());
                        }
                        else
                        {
                            LoadDropdownName(ddlPhEditOut, oneItemEntry.StrEmpName.ToString(), false);
                            LoadEntryTime(txtPhEditOut, oneItemEntry.DtTrackDate.ToString());
                        }
                    }
                    else if (oneItemEntry.IntEmpTrkAccStatus == 5)
                    {
                        if (oneItemEntry.IntEntryType == 1)
                        {
                            //LoadDropdownName(ddlFinalIn, oneItemEntry.StrEmpName.ToString(), false);
                            //LoadEntryTime(txtFinalDateIn, oneItemEntry.DtTrackDate.ToString());
                        }
                        else
                        {
                            //LoadDropdownName(ddlFinalOut, oneItemEntry.StrEmpName.ToString(), false);
                            //LoadEntryTime(txtFinalDateOut, oneItemEntry.DtTrackDate.ToString());
                        }
                    }

                }
            }

        }

        private string CheckTrackStatus(string strItemNum,int trkId)
        {
            theItemEntryGroup = ItemEntryGroup.GetItemEntryDataTable(strItemNum);
            //string trkGrpExists = "N";
            if (theItemEntryGroup != null)
            {
                foreach (ItemEntry oneItemEntry in theItemEntryGroup)
                {
                    string empTrkAccStatus = oneItemEntry.IntEmpTrkAccStatus.ToString();

                    if (oneItemEntry.IntEmpTrkAccStatus == trkId)
                    {
                        //trkGrpExists = "Y";
                        return "Y";
                    }                    

                }
            }
            return "N";// trkGrpExists;

        }

        private void ClearExpItmTxt()
        {
            /*ddlCopyIn.Items.Clear();
            ddlCopyOut.Items.Clear();
            ddlPhotoIn.Items.Clear();            
            ddlPhEditIn.Items.Clear();
            ddlFinalIn.Items.Clear();
            ddlFinalOut.Items.Clear();*/
            ddlPhotoOut.Items.Clear();
            ddlPhEditOut.Items.Clear();

            txtStylistChkIn.Text = string.Empty;
            txtStylistChkInDt.Text = string.Empty;
            txtStylistChkInDt.Enabled = false;
            txtReceivIn.Text = string.Empty;
            txtSampleDateIn.Text = string.Empty;
            txtStylistChkIn.Text = string.Empty;
            txtStylistChkInDt.Text = string.Empty;
            txtPhotoDateOut.Text = string.Empty;
            txtPhotoDateOut.Enabled = false;
            txtPhEditOut.Text = string.Empty;
            txtPhEditOut.Enabled = false;
            /*txtCopyDateIn.Text = string.Empty;
            txtCopyDateIn.Enabled = false;
            txtCopyDateOut.Text = string.Empty;
            txtCopyDateOut.Enabled = false;
            txtPhotoDateIn.Text = string.Empty;
            txtPhotoDateIn.Enabled = false;
            txtPhEditIn.Text = string.Empty;
            txtPhEditIn.Enabled = false;
            txtFinalDateIn.Text = string.Empty;
            txtFinalDateIn.Enabled = false;
            txtFinalDateOut.Text = string.Empty;
            txtFinalDateOut.Enabled = false;*/


            txtDivision.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtDept.Text = string.Empty;
            txtClass.Text = string.Empty;
            txtSubclass.Text = string.Empty;
            txtPrice.Text = string.Empty;
            txtCancelDate.Text = string.Empty;
            txtTheme1.Text = string.Empty;
            txtTheme2.Text = string.Empty;
            txtTheme3.Text = string.Empty;
            txtTheme4.Text = string.Empty;
            txtTheme5.Text = string.Empty;
            txtPriLabel.Text = string.Empty;
            lblComExist.Text = string.Empty;
            lblComExist.Enabled = false;

            rdTopSam.Enabled = false;

            BarcodeWebImage2.Visible = false;
            strItmNum = string.Empty;
        }

        private void ClearAll()
        {
            txtItemNum.Text = string.Empty;
            phComplete.Checked = false;
            stlstChk.Checked = false;
            imgPath.Text = string.Empty;
            phEditComplete.Checked = false;
            videoChkd.Checked = false;
            lblvdoerrormsg.Text = string.Empty;
            lblvdoerrormsg.Visible = false;
            itemTitle.Text = string.Empty;
            ClearExpItmTxt();
        }

        private void ChangCtrl(int EmployeeID, WebControl objWebControl, string strEventName, string strDiv)
        {
            bool bRight = false;

            empEntryGroup = EmpEntryGroup.GetEmpEntryDataTable(EmployeeID);
            EntryManager objMgr = new EntryManager(this);

            objMgr.InitialBinding();

            bRight = Utility.GetRight(1, 1, EmployeeID, empEntryGroup, theItemEntryGroup);  //PerfType = 1; Entrytype = 1(in); 
            objMgr.CatchEvent(objWebControl, strEventName, 1, 1, bRight, EmployeeID, strItemNum, strDiv);

            bRight = Utility.GetRight(2, 1, EmployeeID, empEntryGroup, theItemEntryGroup);

            objMgr.CatchEvent(objWebControl, strEventName, 2, 1, bRight, EmployeeID, strItemNum, strDiv);

            bRight = Utility.GetRight(2, 2, EmployeeID, empEntryGroup, theItemEntryGroup);
            objMgr.CatchEvent(objWebControl, strEventName, 2, 2, bRight, EmployeeID, strItemNum, strDiv);

            bRight = Utility.GetRight(3, 1, EmployeeID, empEntryGroup, theItemEntryGroup);
            objMgr.CatchEvent(objWebControl, strEventName, 3, 1, bRight, EmployeeID, strItemNum, strDiv);

            bRight = Utility.GetRight(3, 2, EmployeeID, empEntryGroup, theItemEntryGroup);
            objMgr.CatchEvent(objWebControl, strEventName, 3, 2, bRight, EmployeeID, strItemNum, strDiv);

            bRight = Utility.GetRight(4, 1, EmployeeID, empEntryGroup, theItemEntryGroup);
            objMgr.CatchEvent(objWebControl, strEventName, 4, 1, bRight, EmployeeID, strItemNum, strDiv);

            bRight = Utility.GetRight(4, 2, EmployeeID, empEntryGroup, theItemEntryGroup);
            objMgr.CatchEvent(objWebControl, strEventName, 4, 2, bRight, EmployeeID, strItemNum, strDiv);

            bRight = Utility.GetRight(5, 1, EmployeeID, empEntryGroup, theItemEntryGroup);
            objMgr.CatchEvent(objWebControl, strEventName, 5, 1, bRight, EmployeeID, strItemNum, strDiv);

            bRight = Utility.GetRight(5, 2, EmployeeID, empEntryGroup, theItemEntryGroup);
            objMgr.CatchEvent(objWebControl, strEventName, 5, 2, bRight, EmployeeID, strItemNum, strDiv);
        }


        private void LoadEntryTime(TextBox txtTimeBox, string strTime)
        {
            txtTimeBox.Text = string.Empty;
            txtTimeBox.Text = strTime;

            if (strTime != "")
            {
                txtTimeBox.Enabled = false;

            }
        }
        private void LoadRecvName(TextBox txtRecNameBox, string strName)
        {
            txtRecNameBox.Text = string.Empty;
            txtRecNameBox.Text = strName;

            if (strName != "")
            {
                txtRecNameBox.Enabled = false;

            }
        }
        private void LoadTopSamInfo(RadioButtonList rdTopSamChk, string strItemNum)
        {
            DataTable theItmTopInfo = Data.GetItmTopInfo(strItemNum);
            if (theItmTopInfo.Rows.Count > 0)
            {
                if (theItmTopInfo.Rows[0]["TopSample"] != null && (theItmTopInfo.Rows[0]["TopSample"].ToString() != null && theItmTopInfo.Rows[0]["TopSample"].ToString() != string.Empty))
                {
                    rdTopSam.SelectedValue = theItmTopInfo.Rows[0]["TopSample"].ToString().Trim();
                    rdTopSam.Enabled = false;
                }
                else
                {
                    rdTopSam.SelectedValue = "TOPS";
                    rdTopSam.Enabled = false;
                }
            }
        }
        private void LoadDropdownName(DropDownList ddlList, string strName, bool IsSelected)
        {
            ddlList.Items.Clear();
            ListItem itm = new ListItem();
            itm.Text = strName;
            itm.Value = strName;
            ddlList.Items.Add(itm);
            if (IsSelected)
            {
                ddlList.Enabled = false;

            }
        }
        //Filter out invalid dates
        private string FilterDate(DateTime dt)
        {
            if (dt > DateTime.Parse("1/1/1980"))
            {
                return dt.ToShortDateString();
            }
            else
            {
                return "N/A";
            }
        }

        protected void rdTopSam_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (this.rdTopSam.SelectedValue.ToString() == "TOP")
            //{}
            //else if (this.rdTopSam.SelectedValue.ToString() == "SAMPLE")
            //{}
            Session.Add("ViewTopSamp", this.rdTopSam.SelectedValue.ToString());
        }
        public WebControl getWebControl(int type)
        {
            WebControl _webControl = null;
            switch (type)
            {
                case 0:
                    _webControl = this.txtItemNum;
                    break;
                /*case 1:
                    _webControl = this.ddlCopyIn;
                    break;
                case 2:
                    _webControl = this.ddlCopyOut;
                    break;
                case 3:
                    _webControl = this.ddlPhotoIn;
                    break;*/
                case 4:
                    _webControl = this.ddlPhotoOut;
                    break;
                /*case 5:
                    _webControl = this.ddlPhEditIn;
                    break;*/
                case 6:
                    _webControl = this.ddlPhEditOut;
                    break;
                /*case 7:
                    _webControl = this.ddlFinalIn;
                    break;
                case 8:
                    _webControl = this.ddlFinalOut;
                    break;
                case 9:
                    _webControl = this.txtCopyDateIn;
                    break;
                case 10:
                    _webControl = this.txtCopyDateOut;
                    break;
                case 11:
                    _webControl = this.txtPhotoDateIn;
                    break;*/
                case 12:
                    _webControl = this.txtPhotoDateOut;
                    break;
                /*case 13:
                    _webControl = this.txtPhEditIn;
                    break;*/
                case 14:
                    _webControl = this.txtPhEditOut;
                    break;
                /*case 15:
                    _webControl = this.txtFinalDateIn;
                    break;
                case 16:
                    _webControl = this.txtFinalDateOut;
                    break;*/
                case 17:
                    _webControl = this.txtReceivIn;
                    break;
                case 18:
                    _webControl = this.txtSampleDateIn;
                    break;
                case 19:
                    _webControl = this.rdTopSam;
                    break;
                case 20:
                    _webControl = this.btnEntry;
                    break;
                //case 20:
                //    _webControl = this.txtZcav;
                //    break;

            }
            return _webControl;
        }

        protected void btnRecv_Click(object sender, EventArgs e)
        {

            int intEntryInsert = 0;
            int intPrntInsert = 0;
            int intTopImageInsert = 0;
            int intBarCodeInsert = 0;
            string strItemNum = string.Empty;
            lblNoItemMsg.Text = string.Empty;


            strItemNum = ViewState["StrItemNum"].ToString();
            dtUser = (DataTable)Session["UserProfile"];

            if (dtUser != null)
            {
                rdTopSam.Enabled = true;
                int EmployeeID = int.Parse(dtUser.Rows[0]["EmployeeID"].ToString().Trim());
                string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
                string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();

                txtReceivIn.Text = strFirstName.ToString().Trim() + " " + strLastName.ToString().Trim();
                txtSampleDateIn.Text = DateTime.Now.ToString();
                ShowItmImage(strItemNum);
                //insert to item Entry table
                intEntryInsert = Data.EntryInsert(strItemNum, EmployeeID, 1, 1);                
                Update_CustomFields(sender, e);
                if (intEntryInsert == 1)
                {
                    //insert to bar code image to DB
                    BarcodeWebImage2.Visible = true;
                    BarcodeWebImage2.Value = strItemNum;

                    //byte[] barcodeBytes = BarcodeWebImage2.GetImageBytesJPG();

                    string strBarCodePath = strBarCodeDir + BarcodeWebImage2.Value + ".gif";

                    BarcodeWebImage2.SaveImage(@strBarCodePath);

                    intBarCodeInsert = Data.BarCodeInsert(strItemNum, strBarCodePath);

                    BarcodeWebImage2.Value = strItemNum;
                    if (intBarCodeInsert == 1)
                    {
                        //insert the Top/Sample info to DB. 
                        //check user input first.

                        //check session value of TopsSample if not  if (Session["ViewPermission"] != null) dtUser = (DataTable)Session["UserProfile"];
                        if (Session["ViewTopSamp"] != null)
                        {
                            //get TopSample from session
                            strTopSamp = (string)Session["ViewTopSamp"];
                            intTopImageInsert = Data.TrkTopUpdate(strItemNum, strTopSamp, 1);
                        }
                        else
                        {
                            //insert topsample from user input to Session variable and insert to DB 
                            Session.Add("ViewTopSamp", this.rdTopSam.SelectedValue.ToString());
                            intTopImageInsert = Data.TrkTopUpdate(strItemNum, this.rdTopSam.SelectedValue.ToString(), 1);
                        }
                        //insert this.rdTopSam.SelectedValue otherwise
                        //insert session value of TopSample Session.Add("ViewPermission", dtPermit);
                        //intTopImageInsert = Data.TrkTopUpdate(strItemNum, this.rdTopSam.SelectedValue.ToString(), 1);
                        if (intTopImageInsert == 1) //updated into Top and Sample good
                        {
                            //insert to print table
                            intPrntInsert = Data.RptPrintInsert(strItemNum, EmployeeID, 0);

                            if (intPrntInsert == 1)
                            {
                                lblNoItemMsg.Text = "Item # " + strItemNum + " has been saved successfully.";
                            }
                            else if (intPrntInsert == 2)
                            {
                                lblNoItemMsg.Text = "Item # " + strItemNum + "has already been received. Update entry info for next step.";
                            }
                            else
                            {
                                lblNoItemMsg.Text = " There is an error occurred once save PrintInsert info to DB for Item # " + strItemNum;
                            }
                        }
                        else if (intTopImageInsert == 2)
                        { lblNoItemMsg.Text = "Top/Sample Item has already existed on DB for Item # " + strItemNum; }
                        else
                        { lblNoItemMsg.Text = " There is an error occurred once save Top/Sample info to DB for Item # " + strItemNum; }


                    }
                    else if (intBarCodeInsert == 2)
                    {
                        //lblNoItemMsg.Text = "There is the same BarCode Item exists on DB for Item # " + strItemNum;
                    }
                    else
                    { lblNoItemMsg.Text = "There is an error occurred once save Barcode to DB for Item # " + strItemNum; }

                    ClearAll();
                    pnlEntry.Visible = false;
                    pnlRecv.Visible = false;
                    pnlUpdate.Visible = true;
                }
            }
        }

        protected void btnEntry_Click(object sender, EventArgs e)
        {

            int intEntryInsert = 0;
            string stlChkInStatus = "N";

            if (Page.IsValid )
            {
                if (scrNeedClear != "Y")
                {
                    ViewState["StrItemNum"] = strItemNum;
                    strItemNum = this.txtItemNum.Text.Trim();
                    dtUser = (DataTable)Session["UserProfile"];
                    if (dtUser != null)
                    {
                        EmployeeID = int.Parse(dtUser.Rows[0]["EmployeeID"].ToString().Trim());
                        strDiv = dtUser.Rows[0]["Division"].ToString().Trim();

                    }
                    theItemEntryGroup = ItemEntryGroup.GetItemEntryDataTable(strItemNum);
                    if (theItemEntryGroup != null)
                    {
                        //ValidateEntryDates();
                        ChangCtrl(EmployeeID, btnEntry, "SaveEntry", strDiv);

                        //BarcodeWebImage2.Visible = false;
                        BarcodeWebImage2.Visible = true;
                        BarcodeWebImage2.Value = strItemNum;

                        //byte[] barcodeBytes = BarcodeWebImage2.GetImageBytesJPG();
                        
                        Update_CustomFields(sender, e);
                        lblNoItemMsg.Text = "Your Entry for Item # " + strItemNum + " has been saved successfully.";
                    }
                    else
                    {
                        lblNoItemMsg.Text = "Could not find the Entry information for Item # " + strItemNum + ". Please re-enter. ";
                    }
                }

            }
            else { lblNoItemMsg.Text = "Invalid Entry Time. Please click Clear button then re-enter Item # and Time. "; }
            ClearAll();
            pnlEntry.Visible = false;
            pnlRecv.Visible = false;
            pnlUpdate.Visible = true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //bool bItemTypeRight = false;
            //bool bRight = false;
            bool bItem = false;
            lblNoItemMsg.Text = string.Empty;
            int intRevResult = 0;
            int intStlystChkInResult = 0;
            int intItemExixt = 0;
            string stlChkInStatus = "N";

            strItemNum = this.txtItemNum.Text.Trim();

            if (strItemNum != "")
            {
                ViewState["StrItemNum"] = strItemNum;

                dtUser = (DataTable)Session["UserProfile"];
                if (dtUser != null)
                {
                    EmployeeID = int.Parse(dtUser.Rows[0]["EmployeeID"].ToString().Trim());
                    string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
                    string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();
                    string strDiv = dtUser.Rows[0]["Division"].ToString().Trim();

                    //see if item is on PhotoTracking table 
                    intItemExixt = Data.CheckGersItem(strItemNum, EmployeeID);


                    //if (bItem == true)  //check if item is on Gers
                    if (intItemExixt == 1)
                    {
                        bItem = LoadGersTrkInfo(strItemNum);
                        //phComplete.Checked = true; commented by vijay
                        //rdTopSam.SelectedIndex = 0; 
                        //if (Session["ViewTopSamp"] != null)
                        //{
                        //    rdTopSam.SelectedValue = (string)Session["ViewTopSamp"];
                        //}
                        //else
                        //{
                        //    rdTopSam.SelectedValue = "TOPS";
                        //}
                        BarcodeWebImage2.Visible = true;
                        BarcodeWebImage2.Value = strItemNum;
                        //byte[] barcodeBytes = BarcodeWebImage2.GetImageBytesJPG();

                        DataTable empRev = Data.GetRevUsers(strItemNum);
                        if (empRev.Rows.Count > 0)  //check if received
                        {
                            //ValidateEntryDates();                            

                            GetItmTopInfo(strItemNum);
                            LoadItemEntryInfo(strItemNum);

                            ChangCtrl(EmployeeID, txtItemNum, "SelectShow", strDiv);

                            ShowItmImage(strItemNum);

                            bool bComExist = this.IsComExist(strItemNum);
                            if (bComExist == true)
                            {
                                lblComExist.Text = "There are comments for this item.";
                            }
                            else { lblComExist.Text = string.Empty; }

                            pnlUpdate.Visible = false;
                            pnlRecv.Visible = false;
                            pnlEntry.Visible = true;
                        }
                        else
                        {
                            intRevResult = Data.CheckEmpRevRight(EmployeeID);
                            if (intRevResult == 1)  //check the emp who has item receive right or not
                            {
                                pnlRecv.Visible = true;
                                rdTopSam.Enabled = true;
                                pnlUpdate.Visible = false;
                                pnlEntry.Visible = false;

                                txtReceivIn.Text = strFirstName.ToString().Trim() + " " + strLastName.ToString().Trim();
                                txtSampleDateIn.Text = DateTime.Now.ToString();
                                ShowItmImage(strItemNum);

                            }
                            else
                            {
                                lblNoItemMsg.Text = "Item # " + strItemNum + " has not been through Receive process yet. Please ask the person who has the receive accss right to enter it first. ";
                                ClearAll();
                            }
                        }
                        //Added below code to take care of displaying the current user details by Vijay
                        //if (ddlPhotoOut.Items.Count == 0 || ddlPhotoOut.SelectedValue == "0")
                        //{
                        //    ddlPhotoOut.Items.Clear();
                        //    ListItem itm = new ListItem();
                        //    itm.Text = strFirstName + " " + strLastName;
                        //    itm.Value = EmployeeID.ToString();
                        //    ddlPhotoOut.Items.Add(itm);

                        //    LoadEntryTime(txtPhotoDateOut, DateTime.Now.ToString());
                        //}
                    }
                    else if (intItemExixt == 2)
                    {
                        lblNoItemMsg.Text = "Item # " + strItemNum + " does not exist. Please re-enter.";
                        ClearAll();
                    }
                    else
                    {
                        lblNoItemMsg.Text = "There is an error once generating the Item # " + strItemNum + ". Please re-enter.";
                        ClearAll();
                    }
                }
                else
                {
                    lblNoItemMsg.Text = "Could not find employee information. Please re-logon";

                    Response.Redirect("./Login.aspx");
                }
            }
            else
            {
                ClearAll();
                pnlRecv.Visible = false;
                pnlUpdate.Visible = true;
                pnlEntry.Visible = false;
                lblNoItemMsg.Text = "You must input Item Number.";
            }
        }

        protected void txtItemNum_TextChanged(object sender, EventArgs e)
        {
            ClearExpItmTxt();

            if (pnlRecv.Visible == true)
            {
                lblNoItemMsg.Text = "You have to click Clear button before searching other item.";

                pnlRecv.Visible = false;
                scrNeedClear = "Y";
            }
            else if (pnlEntry.Visible == true)
            {
                lblNoItemMsg.Text = "You have to click Clear button before searching other item.";

                pnlEntry.Visible = false;
                scrNeedClear = "Y";
            }
            pnlUpdate.Visible = true;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearAll();
            lblNoItemMsg.Text = string.Empty;
            pnlUpdate.Visible = true;
            pnlRecv.Visible = false;
            pnlEntry.Visible = false;
        }
        protected void ddlPhEditOut_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlPhEditOut.SelectedValue != "0")
            {
                this.txtPhEditOut.Text = DateTime.Now.ToString();
            }
        }
        protected void ddlPhotoOut_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlPhotoOut.SelectedValue != "0")
            {
                this.txtPhotoDateOut.Text = DateTime.Now.ToString();
            }
        }
        /*
        protected void ddlCopyIn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlCopyIn.SelectedValue != "0")
            {
                this.txtCopyDateIn.Text = DateTime.Now.ToString();
            }
        }

        protected void ddlCopyOut_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlCopyOut.SelectedValue != "0")
            {
                this.txtCopyDateOut.Text = DateTime.Now.ToString();
            }
        }

        protected void ddlPhotoIn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlPhotoIn.SelectedValue != "0")
            {
                this.txtPhotoDateIn.Text = DateTime.Now.ToString();
            }
        }


        protected void ddlPhEditIn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlPhEditIn.SelectedValue != "0")
            {
                this.txtPhEditIn.Text = DateTime.Now.ToString();
            }
        }

        
        protected void ddlFinalIn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlFinalIn.SelectedValue != "0")
            {
                this.txtFinalDateIn.Text = DateTime.Now.ToString();
            }
        }

        protected void ddlFinalOut_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlFinalOut.SelectedValue != "0")
            {
                this.txtFinalDateOut.Text = DateTime.Now.ToString();
            }
        }*/

        private bool IsComExist(string strItemNum)
        {
            bool bHasComment = false;
            newCommentGroup = CommentGroup.GetCommentsDataTable(strItemNum);
            if (newCommentGroup.Count > 0)
            {
                lblComExist.Enabled = true;

                bHasComment = true;
            }

            return bHasComment;
        }

        protected void ValidateCompToDates(TextBox txtDateIn, TextBox txtDateOut, CustomValidator cvalDateRange)
        {
            if (txtDateIn.Text != "" || txtDateOut.Text != "")
            {
                DateTime dtIn = DateTime.Parse("1/1/0001");
                DateTime dtOut = DateTime.Parse("1/1/0001");
                bool IsFail = false;

                if (!IsFail)
                {  //to be later than in
                    if ((txtDateIn.Text != "") && (txtDateOut.Text != ""))
                    {
                        dtIn = DateTime.Parse(txtDateIn.Text.Trim());
                        dtOut = DateTime.Parse(txtDateOut.Text.Trim());

                        if (dtIn.CompareTo(dtOut) > 0)
                        {
                            cvalDateRange.IsValid = false;
                        }
                    }
                }
            }
        }
        protected void Textbox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void rdTopSam_SelectedIndexChanged1(object sender, EventArgs e)
        {
            Session.Add("ViewTopSamp", this.rdTopSam.SelectedValue.ToString());
        }

        /*private void ShowItmImage(string strItemNum)
        {
            string strURL = string.Empty;
            string strItemImage = string.Empty;
            string strPicValue = string.Empty;
            bool bUrlExists = false;
            bool bPrimeUrlExists = false;
            bool bSmlUrlExists = false;
            bool bColUrlExists = false;
            ArrayList listItems = new ArrayList();
            string strSmlURL = string.Empty;
            string strBHURL = string.Empty;

            strItmNum = strPath + strItemNum;
            //strZoomedParams = "?$350x500$";
            strZoomedParams = ConfigurationManager.AppSettings["strZoomParamMed"];
            strItemImage = ConfigurationManager.AppSettings["strItemImageNew"];
            strURL = strItemImage.Replace("[picvalue]", strItemNum);
            bUrlExists = ImageExists(strURL);

            if (strItmPrimaryImg != string.Empty)
            {
                strItmPrimaryImg = strItmPrimaryImg + strZoomedParams;
                bPrimeUrlExists = ImageExists(strItmPrimaryImg);
            }

            if (bPrimeUrlExists == true)
            {
                bUrlExists = true;
            }
            else
            {
                bUrlExists = ImageExists(strURL);
                if (bUrlExists == true)
                {
                    strItmPrimaryImg = strURL;
                }
            }


            if (bUrlExists == true)
            {
                string filePath = strURL;
                int uAVUbound = int.Parse(ConfigurationManager.AppSettings["intAVNumber"]);
                listImages.Add("_hi");

                for (int u = 1; u < uAVUbound; u++)
                {
                    string strSml = "_av" + u.ToString();
                    strSmlURL = filePath.Replace("_hi", strSml);

                    bSmlUrlExists = ImageExists(strSmlURL);
                    if (bSmlUrlExists == true)
                    {
                        listImages.Add(strSml);
                    }
                }
                // added color getting from GERS in
                //if (strItemNum !="")
                //{
                //    reader = GetGERSItemColor(strItemNum);
                //    if (reader.HasRows)
                //    {
                //        reader.Read();

                //        if (reader["COLOR"].ToString() != "")
                //        {
                //           string strColor = "-" + reader["COLOR"].ToString(); 
                //            //string strColor = "-" + "blue";

                //           strBHURL = filePath.Replace("_hi?$215x219$", strColor);

                //           bColUrlExists = ImageExists(strBHURL);
                //            if (bColUrlExists == true)
                //            {
                //                listImages.Add(strColor);
                //            }
                //        }
                //    }
                //    reader.Close();
                //}
                bShowImg = true;
            }
            else
            {
                bShowImg = false;
                Image52.Visible = true;
            }

        }*/
        private void ShowItmImage(string strItemNum)
        {
            string strURL = string.Empty;
            string strItemImage = string.Empty;
            string strPicValue = string.Empty;
            bool bUrlExists = false;
            bool bPrimeUrlExists = false;
            bool bSmlUrlExists = false;
            bool bColUrlExists = false;
            ArrayList listItems = new ArrayList();
            string strSmlURL = string.Empty;
            string strBHURL = string.Empty;
            string imgDB = string.Empty;
            string imgDBItem = string.Empty;
            primeImgPath.Text = string.Empty;

            strItmNum = strPath + strItemNum;
            //strZoomedParams = "?$400x600$";
            strZoomedParams = ConfigurationManager.AppSettings["strZoomParamMed"];
            strItemImage = ConfigurationManager.AppSettings["strImgPathSplit1"];

            strURL = strItemImage.Replace("[picvalue]", strItemNum);

            imgDB = ConfigurationManager.AppSettings["strImgPathSplit"];
            imgDBItem = imgDB.Replace("[picvalue]", strItemNum);

            if (strItmPrimaryImg != string.Empty)
            {
                strItmPrimaryImg = strItmPrimaryImg + strZoomedParams;
                bPrimeUrlExists = ImageExists(strItmPrimaryImg);
            }

            //strItmPrimaryImg = string.Empty;

            if (bPrimeUrlExists == true)
            {
                bUrlExists = true;
            }
            else
            {
                bUrlExists = ImageExists(strURL);
                if (bUrlExists == true)
                {
                    strItmPrimaryImg = strURL+ strZoomedParams;
                }
            }

            if (bUrlExists == true)
            {
                string filePath = strURL;
                int uAVUbound = int.Parse(ConfigurationManager.AppSettings["intAVNumber"]);
                newListImages.Add(strURL);
                listImages.Add("_hi");
                listImgDBSave.Add(imgDBItem + "_hi");

                for (int u = 1; u < uAVUbound; u++)
                {
                    string strSml = "_av" + u.ToString();
                    strSmlURL = filePath.Replace("_hi", strSml);

                    bSmlUrlExists = ImageExists(strSmlURL);
                    if (bSmlUrlExists == true)
                    {
                        listImages.Add(strSml);
                        listImgDBSave.Add(imgDBItem + strSml);
                        newListImages.Add(strSmlURL);
                    }
                }

                //Adding Swatch

                string strSml_s = "_swatch";
                strSmlURL = filePath.Replace("_hi", strSml_s);

                bSmlUrlExists = ImageExists(strSmlURL);
                if (bSmlUrlExists == true)
                {
                    listImages.Add(strSml_s);
                    listImgDBSave.Add(imgDBItem + strSml_s);
                    newListImages.Add(strSmlURL);
                }
                //

                bShowImg = true;
            }
            else
            {
                bShowImg = false;
                Image52.Visible = true;
            }

        }
        // Added for PT PH3 start
        private void ShowImage4DB(string strItemNum)
        {
            string strURL = string.Empty;
            string strItemImage = string.Empty;
            string strPicValue = string.Empty;
            bool bUrlExists = false;
            bool bSmlUrlExists = false;
            ArrayList listItems = new ArrayList();
            string strSmlURL = string.Empty;
            string strBHURL = string.Empty;
            string imgDB = string.Empty;
            string imgDBItem = string.Empty;

            strItmNum = strPath + strItemNum;
            //strZoomedParams = "?$400x600$";
            strZoomedParams = ConfigurationManager.AppSettings["strZoomParamStd"];
            strItemImage = ConfigurationManager.AppSettings["strItemImage"];

            strURL = strItemImage.Replace("[picvalue]", strItemNum);

            imgDB = ConfigurationManager.AppSettings["strImgPathSplit1"];
            strURL = imgDB.Replace("[picvalue]", strItemNum);

            //if (strItmPrimaryImg != string.Empty)
            //{
            //    strItmPrimaryImg = strItmPrimaryImg + strZoomedParams;
            //    bPrimeUrlExists = ImageExists(strItmPrimaryImg);
            //}
            //else
            //{

            strItmPrimaryImg = string.Empty;
            //}

            bUrlExists = ImageExists(strURL);

            if (bUrlExists == true)
            {
                string filePath = strURL;
                int uAVUbound = int.Parse(ConfigurationManager.AppSettings["intAVNumber"]);
                newListImages.Add(strURL);

                for (int u = 1; u < uAVUbound; u++)
                {
                    string strSml = "_av" + u.ToString();
                    strSmlURL = filePath.Replace("_hi", strSml);

                    bSmlUrlExists = ImageExists(strSmlURL);
                    if (bSmlUrlExists == true)
                    {
                        //listImages.Add(strSml);
                        //listImgDBSave.Add(imgDBItem + strSml);
                        newListImages.Add(strSmlURL);
                    }
                }

                //Adding Swatch
                string strSml_s = "_swatch";
                strSmlURL = filePath.Replace("_hi", strSml_s);

                bSmlUrlExists = ImageExists(strSmlURL);
                if (bSmlUrlExists == true)
                {
                    swatchImgDB = strSmlURL.Replace("http://hottopic.scene7.com", "");
                }
                //

                bShowImg = true;
            }
            else
            {
                bShowImg = false;
                Image52.Visible = true;
            }

            bShowImg = false;
            Image52.Visible = false;

        }

        private void setImgForDB()
        {

            primeImgDB = string.Empty;
            alt1ImgDB = string.Empty;
            alt2ImgDB = string.Empty;
            alt3ImgDB = string.Empty;
            alt4ImgDB = string.Empty;
            alt5ImgDB = string.Empty;
            alt6ImgDB = string.Empty;
            swatchImgDB = string.Empty;

            ShowImage4DB(this.txtItemNum.Text);

            if (primeImgPath.Text.ToString() != string.Empty && newListImages.Contains(primeImgPath.Text.ToString()))
            {
                primeImgDB = primeImgPath.Text.ToString().Replace("http://hottopic.scene7.com", "");
                newListImages.Remove(primeImgPath.Text.ToString());

                for (int i = 0; i < newListImages.Count; i++)
                {
                    if (i == 0)
                    {
                        alt1ImgDB = newListImages[0].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 1)
                    {
                        alt2ImgDB = newListImages[1].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 2)
                    {
                        alt3ImgDB = newListImages[2].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 3)
                    {

                        alt4ImgDB = newListImages[3].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 4)
                    {
                        alt5ImgDB = newListImages[4].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 5)
                    {
                        alt6ImgDB = newListImages[5].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                }

            }

            else
            {
                for (int i = 0; i < newListImages.Count; i++)
                {

                    if (i == 0)
                    {
                        primeImgDB = newListImages[0].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 1)
                    {
                        alt1ImgDB = newListImages[1].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 2)
                    {
                        alt2ImgDB = newListImages[2].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 3)
                    {

                        alt3ImgDB = newListImages[3].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 4)
                    {
                        alt4ImgDB = newListImages[4].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 5)
                    {
                        alt5ImgDB = newListImages[5].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                    if (i == 6)
                    {
                        alt6ImgDB = newListImages[6].ToString().Replace("http://hottopic.scene7.com", "");
                    }
                }
            }


        }
        // Added for PT PH3 end
        static public OleDbDataReader GetGERSItemColor(string strItemNum)
        {

            OleDbCommand objORACommand = new OleDbCommand();
            OleDbConnection objORAConnection;
            OleDbDataReader objORADataReader;
            try
            {
                objORAConnection = new OleDbConnection(ConfigurationManager.AppSettings["strGERSConn"]);
                string strSQL = "select upper(UDF081) as COLOR "
                        + " from GM_INV.GM_ITM_UDF "
                        + " where ITM_CD = " + strItemNum.ToString();

                objORAConnection.Open();
                objORACommand.Connection = objORAConnection;
                objORACommand.CommandText = strSQL;
                objORADataReader = objORACommand.ExecuteReader();
            }
            catch
            {
                throw;
            }
            finally
            {
            }
            return objORADataReader;
        }

        protected string get_UserID()
        {
            dtUser = (DataTable)Session["UserProfile"];
            string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
            string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();
            string userId = strFirstName.Substring(0, 1) + strLastName;

            return userId;
        }

        protected void Update_CustomFields(object sender, EventArgs e)
        {


            String lastUpdId = string.Empty;
            SqlConnection Sqlconn = new SqlConnection(dbconn);

            lastUpdId = get_UserID();
            setImgForDB();
            try
            {

                String statusSql = string.Empty;
                String phCmpltd = string.Empty;
                String phEdCmpltd = string.Empty;
                String vdChkd = string.Empty;
                String stlstChkdIn = string.Empty;

                if (this.stlstChk.Checked == true)
                {
                    stlstChkdIn = "Y";
                }
                else
                {
                    stlstChkdIn = "N";
                }
                
                if (this.phComplete.Checked == true)
                {
                    phCmpltd = "Y";
                }
                else
                {
                    phCmpltd = "N";
                }
                if (this.phEditComplete.Checked == true)
                {
                    phEdCmpltd = "Y";
                }
                else
                {
                    phEdCmpltd = "N";
                }
                if (this.videoChkd.Checked == true)
                {
                    vdChkd = "Y";
                    if (lblvdolink.Text == string.Empty)
                    {
                        videoChkd_CheckedChanged(sender,e);
                    }
                }
                else
                {
                    vdChkd = "N";
                    this.lblvdolink.Text = string.Empty;
                }

                //update the status and Title
                statusSql = "update RMS_PH_STYLE_Master set LastPHUpdateID=@lUpdID,LastPHUpdateDate=@lUpdDate,phCompleteInd=@phCmpltdInd, " +
                    "phEditCompleteInd=@phEdtCmpltdInd,videoExistInd=@vidInd,videoLink=@videoLink,publishedInd=@pubInd,PrimaryImgPath=@primImg, altIMG1=@alt1Img,altIMG2=@alt2Img,altIMG3=@alt3Img,altIMG4=@alt4Img,altIMG5=@alt5Img,altIMG6=@alt6Img,swatchIMG=@swatchImg where style='" + this.txtItemNum.Text.Trim() + "'";

                Sqlconn.Open();
                _cmd = new SqlCommand(statusSql, Sqlconn);
                _cmd.Parameters.AddWithValue("@lUpdID", lastUpdId);
                _cmd.Parameters.AddWithValue("@lUpdDate", DateTime.Now);
                //_cmd.Parameters.AddWithValue("@primImg", this.imgPath.Text);
                _cmd.Parameters.AddWithValue("@phCmpltdInd", phCmpltd);
                _cmd.Parameters.AddWithValue("@phEdtCmpltdInd", phEdCmpltd);
                _cmd.Parameters.AddWithValue("@vidInd", vdChkd);
                _cmd.Parameters.AddWithValue("@videoLink", this.lblvdolink.Text.Trim().ToString());
                _cmd.Parameters.AddWithValue("@pubInd", 'N');

                //Added for PT PH3
                _cmd.Parameters.AddWithValue("@primImg", primeImgDB);
                _cmd.Parameters.AddWithValue("@alt1Img", alt1ImgDB);
                _cmd.Parameters.AddWithValue("@alt2Img", alt2ImgDB);
                _cmd.Parameters.AddWithValue("@alt3Img", alt3ImgDB);
                _cmd.Parameters.AddWithValue("@alt4Img", alt4ImgDB);
                _cmd.Parameters.AddWithValue("@alt5Img", alt5ImgDB);
                _cmd.Parameters.AddWithValue("@alt6Img", alt6ImgDB);
                _cmd.Parameters.AddWithValue("@swatchImg", swatchImgDB);
                /*if (this.lblvdolink.Text.Trim().ToString() == string.Empty)
                {
                    _cmd.Parameters.AddWithValue("@pubInd", 'Y');
                }
                else
                {
                    _cmd.Parameters.AddWithValue("@pubInd", 'N');
                }*/
                _cmd.CommandType = CommandType.Text;
                _cmd.ExecuteNonQuery();

                dtUser = (DataTable)Session["UserProfile"];
                string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
                string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();
                int empId = Convert.ToInt32(dtUser.Rows[0]["EmployeeID"].ToString().Trim());

                //
                if (stlstChkdIn == "N")
                {
                    statusSql = "delete from PhotoTrackEntry where emptrackaccstatus = '6' and itemnum='" + this.txtItemNum.Text.Trim() + "'";
                    _cmd = new SqlCommand(statusSql, Sqlconn);
                    _cmd.CommandType = CommandType.Text;
                    _cmd.ExecuteNonQuery();
                }
                else if (stlstChkdIn == "Y" && txtStylistChkIn.Text.ToString().Trim() != strFirstName + ' ' + strLastName)
                {
                }
                else
                {
                    statusSql = "select count(*) as CNT from PhotoTrackEntry where emptrackaccstatus = '6' and itemnum='" + this.txtItemNum.Text.Trim() + "'";
                    _cmdrdr = new SqlCommand(statusSql, Sqlconn);
                    SqlDataReader reader = _cmdrdr.ExecuteReader();
                    int intFoundInd = 0;
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            if (Convert.ToInt32(reader["CNT"].ToString()) > 0)
                            {
                                intFoundInd = 1;
                            }
                            else
                            {
                                intFoundInd = 0;
                            }
                        }
                    }

                    reader.Close();
                    _cmdrdr.Dispose();

                    if (intFoundInd == 0)
                    {
                        int intStatusResult = Data.EntryInsert(this.txtItemNum.Text, empId, 1, 6);
                                              
                    }
                    else
                    {
                        statusSql = "Update PhotoTrackEntry set employeeid = '" + empId + "' , trackDate='" + txtStylistChkInDt.Text.ToString().Trim() + "' where entrytype  ='1' and emptrackaccstatus = '6' and itemnum='" + this.txtItemNum.Text.Trim() + "'"; ;
                        SqlCommand _cmd1 = new SqlCommand(statusSql, Sqlconn);
                        _cmd1.CommandType = CommandType.Text;
                        _cmd1.ExecuteNonQuery();
                        _cmd1.Dispose();
                    }                
 
                }
                //
                
                
                
                if (ddlPhotoOut.SelectedItem == null) //Clear the text
                {
                    statusSql = "delete from PhotoTrackEntry where entrytype  ='2' and emptrackaccstatus = '3' and itemnum='" + this.txtItemNum.Text.Trim() + "'";
                    _cmd = new SqlCommand(statusSql, Sqlconn);
                    _cmd.CommandType = CommandType.Text;
                    _cmd.ExecuteNonQuery();
                }
                else if (ddlPhotoOut.SelectedItem.Text.Trim() != strFirstName + ' ' + strLastName) //Do nothing
                {
                    //Do nothing  
                }
                else //matching
                {
                    statusSql = "select count(*) as CNT from PhotoTrackEntry where entrytype  ='2' and emptrackaccstatus = '3' and itemnum='" + this.txtItemNum.Text.Trim() + "'";
                    _cmdrdr = new SqlCommand(statusSql, Sqlconn);
                    SqlDataReader reader = _cmdrdr.ExecuteReader();
                    int intFoundInd = 0;
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            if (Convert.ToInt32(reader["CNT"].ToString()) > 0)
                            {
                                intFoundInd = 1;
                            }
                            else
                            {
                                intFoundInd = 0;
                            }
                        }
                    }

                    reader.Close();
                    _cmdrdr.Dispose();

                    if (intFoundInd == 0)
                    {
                        int intStatusResult = Data.EntryInsert(this.txtItemNum.Text, Convert.ToInt32(ddlPhotoOut.SelectedValue.ToString()), 2, 3);
                        //1 - successful insert, 2 - if already exist (no action is needed)                       
                    }
                    else
                    {
                        statusSql = "Update PhotoTrackEntry set employeeid = '" + ddlPhotoOut.SelectedValue.ToString() + "' where entrytype  ='2' and emptrackaccstatus = '3' and itemnum='" + this.txtItemNum.Text.Trim() + "'"; ;
                        SqlCommand _cmd1 = new SqlCommand(statusSql, Sqlconn);
                        _cmd1.CommandType = CommandType.Text;
                        _cmd1.ExecuteNonQuery();
                        _cmd1.Dispose();
                    }
                }

                if (ddlPhEditOut.SelectedItem == null)//clear the text
                {
                    statusSql = "delete from PhotoTrackEntry where entrytype  ='2' and emptrackaccstatus = '4' and itemnum='" + this.txtItemNum.Text.Trim() + "'";
                    _cmd = new SqlCommand(statusSql, Sqlconn);
                    _cmd.CommandType = CommandType.Text;
                    _cmd.ExecuteNonQuery();
                }
                else if (ddlPhEditOut.SelectedItem.Text.Trim() != strFirstName + ' ' + strLastName) //Do nothing
                {
                    //Do nothing  
                }
                else //matching
                {
                    statusSql = "select count(*) as CNT from PhotoTrackEntry where entrytype  ='2' and emptrackaccstatus = '4' and itemnum='" + this.txtItemNum.Text.Trim() + "'";
                    _cmdrdr = new SqlCommand(statusSql, Sqlconn);
                    SqlDataReader reader = _cmdrdr.ExecuteReader();
                    int intFoundInd = 0;
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            if (Convert.ToInt32(reader["CNT"].ToString()) > 0)
                            {
                                intFoundInd = 1;
                            }
                            else
                            {
                                intFoundInd = 0;
                            }
                        }
                    }

                    reader.Close();
                    _cmdrdr.Dispose();

                    if (intFoundInd == 0)
                    {
                        int intStatusResult = Data.EntryInsert(this.txtItemNum.Text, Convert.ToInt32(ddlPhEditOut.SelectedValue.ToString()), 2, 4);
                        //1 - successful insert, 2 - if already exist (no action is needed)                       
                    }
                    else
                    {
                        statusSql = "Update PhotoTrackEntry set employeeid = '" + ddlPhEditOut.SelectedValue.ToString() + "' where entrytype  ='2' and emptrackaccstatus = '4' and itemnum='" + this.txtItemNum.Text.Trim() + "'"; ;
                        SqlCommand _cmd1 = new SqlCommand(statusSql, Sqlconn);
                        _cmd1.CommandType = CommandType.Text;
                        _cmd1.ExecuteNonQuery();
                        _cmd1.Dispose();
                    }

                }

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                Sqlconn.Close();
                Sqlconn.Dispose();
            }

            //searchBtnClicked(sender, e);
            //ShowItmImage(this.txtItemNum.Text);

        }

        protected void phEditComplete_CheckedChanged(object sender, EventArgs e)
        {
            if (phEditComplete.Checked == true)
            {


                dtUser = (DataTable)Session["UserProfile"];
                string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
                string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();

                ddlPhEditOut.Items.Clear();
                ListItem itm = new ListItem();
                itm.Text = strFirstName + " " + strLastName;
                itm.Value = dtUser.Rows[0]["EmployeeID"].ToString().Trim();
                ddlPhEditOut.Items.Add(itm);
                ddlPhEditOut.Enabled = false;

                //LoadDropdownName(ddlPhEditOut, strFirstName + " " + strLastName, false);
                LoadEntryTime(txtPhEditOut, DateTime.Now.ToString());
                //txtPhEditOut
            }

            if (phEditComplete.Checked == false)
            {
                ddlPhEditOut.Items.Clear();
                txtPhEditOut.Text = string.Empty;
            }

            BarcodeWebImage2.Value = this.txtItemNum.Text;

            ShowItmImage(txtItemNum.Text);

        }
        

        protected void videoChkd_CheckedChanged(object sender, EventArgs e)
        {
            lblvdoerrormsg.Text = string.Empty;
            int intFound = 0;
            string strFinalURL = string.Empty;
            if (videoChkd.Checked == true)
            {
                strItemNum = this.txtItemNum.Text.Trim();
                string[] strURLList = strVideoPath.Split(',');
                //check the video exist on site or not

                Console.WriteLine(txtDivision.Text);

                if (VideoExists(strURLList[0] + strItemNum + "-td.mp4") == true)
                {
                    strFinalURL = strURLList[0] + strItemNum + "-td.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[0] + strItemNum + "-bl.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[0] + strItemNum + "-bl.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[0] + strItemNum + "-ls.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[0] + strItemNum + "-ls.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[0] + strItemNum + "-ht.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[0] + strItemNum + "-ht.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[0] + strItemNum + "-hu.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[0] + strItemNum + "-hu.mp4";
                    intFound++;
                }

                if (VideoExists(strURLList[1] + strItemNum + "-ht.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[1] + strItemNum + "-ht.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[1] + strItemNum + "-bl.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[1] + strItemNum + "-bl.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[1] + strItemNum + "-ls.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[1] + strItemNum + "-ls.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[1] + strItemNum + "-td.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[1] + strItemNum + "-td.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[1] + strItemNum + "-hu.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[1] + strItemNum + "-hu.mp4";
                    intFound++;
                }

                if (VideoExists(strURLList[2] + strItemNum + "-td.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[2] + strItemNum + "-td.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[2] + strItemNum + "-ls.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[2] + strItemNum + "-ls.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[2] + strItemNum + "-ht.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[2] + strItemNum + "-ht.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[2] + strItemNum + "-bl.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[2] + strItemNum + "-bl.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[2] + strItemNum + "-hu.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[2] + strItemNum + "-hu.mp4";
                    intFound++;
                }

                if (VideoExists(strURLList[3] + strItemNum + "-ht.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[3] + strItemNum + "-ht.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[3] + strItemNum + "-td.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[3] + strItemNum + "-td.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[3] + strItemNum + "-bl.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[3] + strItemNum + "-bl.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[3] + strItemNum + "-ls.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[3] + strItemNum + "-ls.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[3] + strItemNum + "-hu.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[3] + strItemNum + "-hu.mp4";
                    intFound++;
                }

                if (VideoExists(strURLList[4] + strItemNum + "-ht.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[4] + strItemNum + "-ht.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[4] + strItemNum + "-td.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[4] + strItemNum + "-td.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[4] + strItemNum + "-bl.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[4] + strItemNum + "-bl.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[4] + strItemNum + "-ls.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[4] + strItemNum + "-ls.mp4";
                    intFound++;
                }
                if (VideoExists(strURLList[4] + strItemNum + "-hu.mp4") == true && intFound == 0)
                {
                    strFinalURL = strURLList[4] + strItemNum + "-hu.mp4";
                    intFound++;
                }

                if (intFound == 0)
                {
                    lblvdoerrormsg.Text = "Video for Item # " + strItemNum + " does not exist. Video option will not be selected";
                    lblvdoerrormsg.Visible = true;
                    videoChkd.Checked = false;
                    //ClearAll();
                }
                else
                {
                    lblvdolink.Text = strFinalURL.Replace("http://hottopic.scene7.com", string.Empty);
                    lblvdoerrormsg.Text = string.Empty;
                    lblvdoerrormsg.Visible = false;
                }
            }
            BarcodeWebImage2.Value = this.txtItemNum.Text;

            ShowItmImage(txtItemNum.Text);
        }

        protected void phCompleteChkd_CheckedChanged(object sender, EventArgs e)
        {
            if (phComplete.Checked == true)
            {
                dtUser = (DataTable)Session["UserProfile"];
                string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
                string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();

                ddlPhotoOut.Items.Clear();
                ListItem itm = new ListItem();
                itm.Text = strFirstName + " " + strLastName;
                itm.Value = dtUser.Rows[0]["EmployeeID"].ToString().Trim();
                ddlPhotoOut.Items.Add(itm);
                ddlPhotoOut.Enabled = false;

                //LoadDropdownName(ddlPhEditOut, strFirstName + " " + strLastName, false);
                LoadEntryTime(txtPhotoDateOut, DateTime.Now.ToString());
                //txtPhEditOut
            }

            if (phComplete.Checked == false)
            {
                ddlPhotoOut.Items.Clear();
                ddlPhotoOut.Text = string.Empty;
                txtPhotoDateOut.Text = string.Empty;
            }

            BarcodeWebImage2.Value = this.txtItemNum.Text;

            ShowItmImage(txtItemNum.Text);

        }

        protected void stlstChk_CheckedChanged(object sender, EventArgs e)
        {
            if (stlstChk.Checked == true)
            {


                dtUser = (DataTable)Session["UserProfile"];
                string strFirstName = dtUser.Rows[0]["EmpFirstName"].ToString().Trim();
                string strLastName = dtUser.Rows[0]["EmpLastName"].ToString().Trim();

                txtStylistChkIn.Text = strFirstName + " " + strLastName;
                txtStylistChkIn.Enabled = false;
                LoadEntryTime(txtStylistChkInDt, DateTime.Now.ToString());
            }

            if (stlstChk.Checked == false)
            {
                txtStylistChkIn.Text = string.Empty;
                txtStylistChkInDt.Text = string.Empty;
            }

            BarcodeWebImage2.Value = this.txtItemNum.Text;

            ShowItmImage(txtItemNum.Text);

        }

    }
}
