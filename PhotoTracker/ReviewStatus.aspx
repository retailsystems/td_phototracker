<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReviewStatus.aspx.cs" Inherits="PhotoTracker.ReviewStatus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
	<HEAD>
		<title>PhotoTrack</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
        <script language="javascript" type="text/javascript">

            function Comments()
            {
              var itemnum = "<%=strItemNum%>";
              window.open('CommentView.aspx?strItmNum=' + itemnum, 'Comments', 'width=600,height=600,resizable=yes');
            }

		</script>    
	</HEAD>
		<body onload="javascript:document.Form1.txtItemNum.focus();" bottomMargin="0" bgColor="#000000" topMargin="0" >
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header>
			<div id="main" style="width: 988px">
				<TABLE class="innerTableDetail" id="tblOption" cellSpacing="0" cellPadding="0" width="96%"
					align="center" border="0" runat="server">
					<TBODY>
						<tr>
							<td class="innerTableDetail" colSpan="2">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0" style="height: 20px" id="TABLE1">
									<TBODY>
										<tr>
											<td colSpan="2">
												<table width="100%" border="0">
													<TR>
														<TD class="tableTitle" colSpan="2" valign="middle">Items Details - Review Status</TD>
													</TR>
													<TR height="30">
														<td>
															<table width="600" border="0">
																<TR>
																	<TD width="60">Item Input: </td>
                                                                    <td width="70"><asp:textbox id="txtItemNum" runat="server" CssClass="tableInput" columns="10" OnTextChanged="txtItemNum_TextChanged"></asp:textbox></td>
                                                                    <TD width="62">&nbsp;Description:</TD>
                                                                     <TD><asp:textbox ReadOnly ="True" id="txtDescription" runat="server" CssClass="tableInput" columns="48"></asp:textbox></TD>
																</TR>
																
																<TR><td colSpan="4" width="600"><font color="#cc0000"><asp:Label ID ="lblNoItemMsg" runat="server"></asp:Label></font></td>
																</TR>
																
															</table>
                                                            </td>
													</TR>
													<tr height="20"><td><table border="0">
													            <tr height="4"><td width="38">IN BY:</td>
													            <td width="160"><asp:textbox id="txtReceivIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="33"></asp:textbox></td><td width="10"> </td>
													            <td>SAMPLE IN:</td><td><asp:textbox id="txtSampleDateIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="30"></asp:textbox></td></tr>
													</table>
                                                        </td></tr>
												     
												</table>
                                               
											</td>
											<td valign ="bottom" style="width:300px"> 
												<table cellSpacing="0" cellPadding="0" width="50%" border="0">
													<TR>
																	
																	<TD style="HEIGHT: 21px" align="left">
                                                                        <strong>&nbsp; </strong>
																		<asp:radiobuttonlist id="rdTopSam" runat="server" AutoPostBack="True" ToolTip="Please select TOP or SAMPLE item type.">
																		<asp:listitem value="TOP">TOP</asp:listitem>
																	    <asp:listitem value="SAMPLE">SAMPLE</asp:listitem></asp:radiobuttonlist>
																	</TD>
																	<td></td>
																	</TR>
												</table>
											</td>
										</tr>
									</TBODY>
								</TABLE>
							</td>
						</tr>
						<tr>
							<td class="innerTableDetail" vAlign="top">
								<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
									<TR height="20"><td><TABLE>
									     <TR valign="bottom"><TD class="tableTitle" style="HEIGHT: 20px" width="300" colSpan="2">Entry &nbsp;Information</TD>
									     <td align="right" valign="bottom"><font class="fontSmallBold">( Format: MM/DD/YYYY )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>
                                                &nbsp; &nbsp; &nbsp;</strong></font></td>
									</TR></TABLE>
                                    </td></TR>

						            <tr><td class="innerTableDetailThin"><table border="0" cellSpacing="0" cellPadding="0">
									<TR height="50">
										<TD width="12%"><b>&nbsp; Copy: </b></TD>
										<td class="tableCell">
										  <table border="0" cellSpacing="0" cellPadding="0">
										      <tr height="20"><td valign="bottom">IN</td>
     						                   <TD valign="bottom">
     						                         <asp:dropdownlist id="ddlCopyIn" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="48%">
						                          <asp:textbox id="txtCopyDateIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtCopyDateIn');"
								href="javascript:;"><IMG height="15" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0"></A></TD>
									         </TR>
									         
									         <tr height="20"><td valign="bottom">OUT</td>
     						                   <TD valign="bottom" width="30%">
     						                         <asp:dropdownlist id="ddlCopyOut" ReadOnly="True" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom">
						                          <asp:textbox id="txtCopyDateOut" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtCopyDateOut');"
								href="javascript:;"><IMG height="15" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0"></A>
									          </TD>
									         </TR>
									         <tr height="2"><td></td></tr>
									         </table>
									      </td>
									 </TR>
									</table></td></tr>  
									
									<tr><td style="width: 516px" class="innerTableDetailThin"><table border="0" cellSpacing="0" cellPadding="0">
									<TR height="50">
										<TD width="12%"><b>&nbsp; Photo: </b></TD>
										<td class="tableCell">
										  <table border="0" cellSpacing="0" cellPadding="0">
										      <tr height="20"><td valign="bottom">IN</td>
     						                   <TD valign="bottom">
     						                         <asp:dropdownlist id="ddlPhotoIn" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="48%">
						                          <asp:textbox id="txtPhotoDateIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhotoDateIn');"
								href="javascript:;"><IMG height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0"></A></TD>
									         </TR>
									         
									         <tr height="20"><td valign="bottom">OUT</td>
     						                   <TD valign="bottom">
     						                         <asp:dropdownlist id="ddlPhotoOut" ReadOnly ="True" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="30%">
						                          <asp:textbox id="txtPhotoDateOut" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhotoDateOut');"
								href="javascript:;"><IMG height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0"></A>
									          </TD>
									         </TR>
									         <tr height="2"><td></td></tr>
									         </table>
									      </td>
									 </TR>
									</table></td></tr>  
									<tr><td style="width: 516px;" class="innerTableDetailThin"><table border="0" cellSpacing="0" cellPadding="0">
									<TR height="50">
										<TD width="12%"><b>
										 &nbsp; Photo&nbsp;<br /> &nbsp; Edit:</b></TD>
										<td class="tableCell">
										  <table border="0" cellSpacing="0" cellPadding="0">
										      <tr height="20"><td valign="bottom">IN</td>
     						                   <TD valign="bottom" width="30%">
     						                         <asp:dropdownlist id="ddlPhEditIn" ReadOnly ="True" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="48%">
						                          <asp:textbox id="txtPhEditIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhEditIn');"
								href="javascript:;"><IMG height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0"></A></TD>
									         </TR>
									         
									         <tr height="20"><td valign="bottom">OUT</td>
     						                   <TD valign="bottom" width="48%">
     						                         <asp:dropdownlist id="ddlPhEditOut" ReadOnly ="True" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="30%">
						                          <asp:textbox id="txtPhEditOut" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhEditOut');"
								href="javascript:;"><IMG height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0"></A>
									          </TD>
									         </TR>
									         <tr height="2"><td></td></tr>
									         </table>
									      </td>
									 </TR>
									</table></td></tr>
									<tr><td style="width: 516px" class="innerTableDetailThin"><table border="0" cellSpacing="0" cellPadding="0">
									<TR height="50">
										<TD width="12%"><b>&nbsp; Final <br /> &nbsp;&nbsp;App: </b></TD>
										<td class="tableCell">
										  <table border="0" cellSpacing="0" cellPadding="0">
										      <tr height="20"><td valign="bottom">IN</td>
     						                   <TD valign="bottom" width="30%">
     						                         <asp:dropdownlist id="ddlFinalIn" ReadOnly ="True" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="48%">
						                          <asp:textbox id="txtFinalDateIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtFinalDateIn');"
								href="javascript:;"><IMG height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0"></A></TD>
									         </TR>
									         
									         <tr height="20"><td valign="bottom">OUT</td>
     						                   <TD valign="bottom" width="48%">
     						                         <asp:dropdownlist id="ddlFinalOut" ReadOnly ="True" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput"></asp:dropdownlist>
     						                  </TD>
						                      <TD vAlign="bottom" width="30%">
						                          <asp:textbox id="txtFinalDateOut" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><A title="Pick Date from Calendar" onclick="calendarPicker('Form1.ddlFinalOut');"
								href="javascript:;"><IMG height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0"></A>
									          </TD>
									         </TR>
									         </table>
									        
									      </td>
									 </TR>
									
									</table></td></tr>
									
									  <tr valign="bottom"><td>
									  <table><tr><td width = "200" valign="bottom">
									         <table><tr><td valign="bottom">&nbsp<a href="javascript:Comments()"><IMG src="./Image/Comments.gif" border="0" id="IMG1" language="javascript"></a></td></tr>
									                <tr><td width = "200" valign="bottom"><font color="#cc0000"><asp:Label ID ="lblComExist" runat="server"></asp:Label></font></td></tr>
									         </table></td>
									             <td></td>
									  </tr></table>
                                      </td></tr>      
								</TABLE>
                               </td>
								<TD class="innerTableDetail" vAlign="top" width="30%" align="center">
								<TABLE cellSpacing="0" cellPadding="1" width="100%" align="center" border="0">
									<TBODY>
										<TR>
											<TD class="tableTitle" style="HEIGHT: 23px" colSpan="2">Item&nbsp;Information</TD>
										</TR>
										<TR valign="top" height="100">
											<td class="innerTableDetailThin" align="center">
												<table border="0" align="center">
													<TR width="67">
														<TD>&nbsp;DIVISION:</TD><TD>&nbsp;<asp:textbox ReadOnly ="True" id="txtDivision" runat="server" CssClass="tableInput" columns="13"></asp:textbox></TD>
													</TR>
													
													<TR>
														<TD>&nbsp;DEPARTMENT:</TD><TD>&nbsp;<asp:textbox ReadOnly ="True" id="txtDept" runat="server" CssClass="tableInput" columns="13"></asp:textbox></TD>
													</TR>
													
													<TR>
														<TD>&nbsp;CLASS:</TD><TD>&nbsp;<asp:textbox ReadOnly ="True" id="txtClass" runat="server" CssClass="tableInput" columns="13"></asp:textbox></TD>
													</TR>
													
													<TR>
														<TD>&nbsp;SUBCLASS:</TD><TD>&nbsp;<asp:textbox ReadOnly ="True" id="txtSubclass" runat="server" CssClass="tableInput" columns="13"></asp:textbox></TD>
													</TR>
													
													<TR>
														<TD>&nbsp;PRICE:</TD><TD>&nbsp;<asp:textbox ReadOnly ="True" id="txtPrice" runat="server" CssClass="tableInput" columns="13"></asp:textbox></TD>
													</TR>

													<TR><td colSpan="2" style="height: 210px" class="innerTableDetailThin">
													   <table style="width: 210px; height: 190px"><tr>
												         <TD style="height: 190px" width="120" ><asp:Image id="imgItem" runat="server"></asp:Image></TD>
													   </tr></table>
													</td></TR>
												</table>
                                                </td><td width="130" style="height: 238px"><table border="0" class="innerTableDetailThin">
                                                       <TR>
												         <TD HEIGHT="1" style="width: 156px">&nbsp;&nbsp;&nbsp;</TD>
													</TR>
                                                    <TR>
														<TD HEIGHT="10" style="width: 156px">&nbsp;&nbsp;&nbsp;CANCEL DATE</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtCancelDate" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="10" style="width: 156px">&nbsp;&nbsp;&nbsp;THEME 1</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme1" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="10" style="width: 156px">&nbsp;&nbsp;&nbsp;THEME 2</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme2" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="10" style="width: 156px">&nbsp;&nbsp;&nbsp;THEME 3</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme3" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="10" style="width: 156px">&nbsp;&nbsp;&nbsp;THEME 4</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme4" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="10" style="width: 156px">&nbsp;&nbsp;&nbsp;THEME 5</TD>
													</TR>
													<TR>
												         <TD style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme5" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													<TR>
														<TD HEIGHT="10" style="width: 156px">&nbsp;&nbsp;&nbsp;PRIV. LABEL</TD>
													</TR>
													<TR>
												         <TD style="width: 156px; height: 26px;">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtPriLabel" runat="server" CssClass="tableInput" columns="12"></asp:textbox></TD>
													</TR>
													 <TR>
												         <TD HEIGHT="34" style="width: 156px">&nbsp;&nbsp;&nbsp;</TD>
													</TR>
													</table></td>
										</tr>
									   <tr><td><asp:Button id="btnUpdate" runat="server" Text="SEARCH" CssClass="buttonRed" OnClick="btnUpdate_Click"></asp:Button></td>
									                   <td><asp:Button id="btnClear" runat="server" Text="CLEAR" CssClass="buttonRed" OnClick="btnClear_Click"></asp:Button></td></tr>
									
									   </TBODY>
								</TABLE>
							</TD>
						</tr>
						
					</TBODY>
				</TABLE>
				<br>
			</div>
			</div>

		</form>
	</body>
</HTML>
