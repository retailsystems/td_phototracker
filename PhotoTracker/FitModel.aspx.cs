using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections.Generic;


namespace PhotoTracker
{
    public partial class FitModel : System.Web.UI.Page
    {
        string dbconn = ConfigurationManager.AppSettings["strPhotoTrackerSQLConn"].ToString();
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand _cmd = new SqlCommand();
        DataSet ds = new DataSet();
        string _EmpSql, _EmpAccessSql, _EmpTrackSQL, GroupId, TrackGroupId;
        string strSupported_Divisions = ConfigurationManager.AppSettings["Supported_Divisions"];

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindModelData();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Reset();
        }
        private void Reset()
        {
            hdnModelId.Value = string.Empty;
            txtName.Text = string.Empty;
            txtName.Enabled = true;
            txtDescription.Text = string.Empty;
            txtModelHeight.Text = string.Empty;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SqlConnection _Sqlconn = new SqlConnection(dbconn);
            if (hdnModelId.Value != "")
            {
                Update();
            }
            else
            {
                try
                {
                    SqlCommand check_Model_Name = new SqlCommand("SELECT COUNT(*) FROM [RMS_DW_Model_MASTER] WHERE ([Name] = @modelName)", _Sqlconn);
                    check_Model_Name.Parameters.AddWithValue("@modelName", txtName.Text.Trim());
                    _Sqlconn.Open();
                    int ModelExist = (int)check_Model_Name.ExecuteScalar();

                    if (ModelExist > 0)
                    {
                        //ValidationSummary1.HeaderText = "Oops something went wrong";
                        valCustom.IsValid = false;
                        valCustom.ErrorMessage = "Model name already exist.";
                        
                    }
                    else
                    {
                        SqlCommand cmd = new SqlCommand("sp_ModelInsert", _Sqlconn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Division", Convert.ToInt32(strSupported_Divisions));
                        cmd.Parameters.AddWithValue("@Name", txtName.Text);
                        cmd.Parameters.AddWithValue("@Description", txtDescription.Text);
                        cmd.Parameters.AddWithValue("@Hight", txtModelHeight.Text);
                        cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.Date);

                       
                        cmd.ExecuteNonQuery();
                        bindModelData();
                    }
              
                
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    string msg = "Error creating Fit Model." + ex.Message;
                    throw new Exception(msg);

                }
                finally
                {
                    _Sqlconn.Close();                  
                }             
            }
            Reset();
        }
        protected void Update()
        {
            SqlConnection _Sqlconn = new SqlConnection(dbconn);
            SqlCommand cmd = new SqlCommand("sp_ModelUpdate", _Sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ModelId", hdnModelId.Value);
            cmd.Parameters.AddWithValue("@Division", Convert.ToInt32(strSupported_Divisions));
            cmd.Parameters.AddWithValue("@Description", txtDescription.Text);
            cmd.Parameters.AddWithValue("@Hight", txtModelHeight.Text);
            cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.Date);
           
            try
            {
                _Sqlconn.Open();
                cmd.ExecuteNonQuery();                
                bindModelData();
            }             
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "Updation Error:";
                msg += ex.Message;
                throw new Exception(msg);

            }
            finally
            {
                _Sqlconn.Close();
            }
        }
        protected void delete()
        {
            SqlConnection _Sqlconn = new SqlConnection(dbconn);
            SqlCommand cmd = new SqlCommand("sp_ModelDelete", _Sqlconn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ModelId", hdnModelId.Value);
            try
            {
                _Sqlconn.Open();
                cmd.ExecuteNonQuery();
                bindModelData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "Deletion Error:";
                msg += ex.Message;
                throw new Exception(msg);

            }
            finally
            {
                _Sqlconn.Close();
            }
        }
        private void bindModelData()
        {
            SqlDataAdapter da = new SqlDataAdapter("sp_ModelSelect", dbconn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            grdModel.DataSource = ds;
            grdModel.DataBind();
        }

      
        protected void grdModel_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int rowIndex = Convert.ToInt32(e.CommandArgument);

            if (e.CommandName == "EditItem")
            {
                hdnModelId.Value = e.CommandArgument.ToString();                
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                txtName.Text = (row.FindControl("lblName") as Label).Text;
                txtName.Enabled = false;
                txtDescription.Text = (row.FindControl("lblDescription") as Label).Text;
                txtModelHeight.Text = (row.FindControl("lblHeight") as Label).Text;
            }
            else if (e.CommandName == "DeleteItem")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                hdnModelId.Value = e.CommandArgument.ToString();
                delete();

            }
        }
    }
}
