<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductCopyApproval.aspx.cs" Inherits="PhotoTracker.ProductCopyApproval"  validateRequest="false"%>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="Bytescout.BarCode" Namespace="Bytescout.BarCode" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>PhotoTrack</title> 
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet" />
         <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script language="javascript" type="text/javascript">

            function calendarPicker(strField)
            {
              window.open('Calendar.aspx?field=' + strField, 'calendarPopup', 'width=250,height=200,resizable=yes');
            }
            function DetailDesc()
            {
              var itemnum = "<%=strItemNum%>";
              window.open('DetailDesc.aspx?strItmNum=' + itemnum, 'DetailDesc', 'width=600,height=600,resizable=yes');
            }

          $(document).ready(function() 
          {
             $(document.body).delegate('.alters a', 'click', function()
	         {
                var image = $(this).attr("rel");
			    var src = $('.productimg img').attr("src") == image;
	           $('.productimg').html('<img src="' + image + '" style="height: 600px;" />').fadeOut(100).fadeIn(150);
             }
          );
          
         });
 

        function mainthumb_onclick(strName) {
         document.getElementById("imgPath").value = strName.replace("http://img.hottopic.com","");
        }

		</script>		
		
	</head>
		<body onload="javascript:document.Form1.txtItemNum.focus();"  bgcolor="#000000">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header>
			<div id="main" style="width: 988px">
<table class="innerTableDetail" id="tblOption" cellspacing="0" cellpadding="0" width="96%" align="center" border="0" runat="server">
					<tbody>
						<tr>
							<td class="innerTableDetail" colspan="2" >
								<table cellspacing="0" cellpadding="1" width="100%" align="center" border="0" style="height: 20px" id="TABLE1">
									<tbody>
										<tr>
											<td colspan="2" style="width: 621px; height: 144px;">
												<table style="width: 80%" border="0">
													<tr>
														<td class="tableTitle" colspan="2" valign="top">Items Details - Product Copy &amp; Approval</td>
													</tr>
													<tr><td colspan="4" style="width: 600px"><font color="#cc0000"><asp:Label ID ="lblNoItemMsg" runat="server"></asp:Label></font></td>
													</tr>
													<tr style="height: 30px">
														<td style="width: 507px; height: 26px">
															<table style="width: 550px" border="0">
																<tr>
                                                                    <td style="height: 103px">
                                                                    <asp:Label ID="Label1" runat="server" Text="Item:" Width="22px" Font-Bold="True"></asp:Label></td>
                                                                    <td style="width: 190px; height: 103px;"><asp:textbox id="txtItemNum" runat="server"  CssClass="tableInput" columns="10" ></asp:textbox>
                                                                        <asp:Button ID="searchbBtn" runat="server" CssClass="buttonRed" Text="SEARCH" OnClick="searchBtnClicked"/></td>
                                                                    <td style="width: 6px; height: 103px;" rowspan="">&nbsp; 
                                                                        <cc1:BarcodeWebImage ID="BarcodeWebImage2" runat="server" AddChecksum="False" AddChecksumToCaption="False"
                                                                            AdditionalCaption=" " AdditionalCaptionFont="Arial, 10pt" AdditionalCaptionPosition="Above"
                                                                            Angle="Degrees0" BackColor="White" BarHeight="20" CaptionFont="Arial, 10pt" CaptionPosition="Below"
                                                                            DrawCaption="True" EnableTheming="True" ForeColor="Black" HorizontalAlignment="Right"
                                                                            NarrowBarWidth="2" RenderingHint="SystemDefault" SmoothingMode="Default" Symbology="Code128"
                                                                            Value="DATA" VerticalAlignment="Top" WideToNarrowRatio="3" /></td>
																</tr>																
																<tr><td colspan="4" style="width: 628px; height: 17px;">
                                                                    <asp:Label ID="Label2" runat="server" Text="ID:" Width="18px" Font-Bold="True"></asp:Label>
                                                                    &nbsp; <asp:textbox ID="itemNum" ReadOnly ="True" runat="server" CssClass="tableInput" columns="10" ></asp:textbox>
                                                                    &nbsp;<asp:Label ID="Label3" runat="server" Text="Catalog: " Width="33px" Font-Bold="True"></asp:Label>&nbsp;
                                                                    &nbsp; &nbsp;<asp:textbox ID="catDesc" runat="server" ReadOnly="True"></asp:textbox></td>
																</tr>
																
															</table>
                                                            <br />
                                                            <br />
                                                            <asp:Label ID="Label11" runat="server" Height="17px" Text="RMS DESCRIPTION:" Width="133px" Font-Bold="True"></asp:Label>
                                                            <asp:textbox ReadOnly ="True" id="txtRMSDesc" runat="server" columns="48" Height="17px" Width="283px" ></asp:textbox>
                                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                            </td>
													</tr>												     
												</table>
                                               
											</td>
											<td valign ="middle" style="width:360px; height: 144px;">
                                                <asp:panel runat="server" id="pnlButton" visible="true" Width="360px">
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <asp:Button ID="save" runat="server" CssClass="buttonRed" Text="SAVE"  OnClick="save_Click" />
                                                <asp:Button ID="approve" runat="server" CssClass="buttonRed" Text="Approve" OnClick="approve_Click" />
                                                <asp:Button ID="unApprove" runat="server" CssClass="buttonRed" Text="Unapprove" OnClick="unApprove_Click" /><br />
                                                &nbsp;<br />
                                                <asp:Label ID="Label5" runat="server" Text="LAST UPDATE" Font-Bold="True"></asp:Label>
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<asp:Label ID="Label4" runat="server" Text="APPROVAL DATE" Font-Bold="True"></asp:Label><br />
                                                <asp:TextBox ID="last_upd_dt_id" runat="server" Width="200px" ReadOnly="True" BorderStyle="None" Font-Names="Arial" ForeColor="White"></asp:TextBox>
                                                &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<asp:TextBox ID="apprDate" runat="server" BorderStyle="Solid" Width="89px" ForeColor="Transparent"></asp:TextBox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.apprDate');"
								href="javascript:;"><img height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0" /></a>
									<br />
                                                <br />
                                                <asp:TextBox ID="itemTitle" runat="server"  Width="305px" BorderStyle="Outset" ></asp:TextBox>&nbsp;
                                                <asp:LinkButton ID="titleEdit" runat="server" Height="11px" Width="19px" OnClick="titleEdit_Click1">edit</asp:LinkButton>&nbsp; 
                                                    </asp:panel>
                                                    </td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class="innerTableDetail" valign="top" style="width: 439px">
                                &nbsp;&nbsp;<br />
                                <table align="center" border="0" cellpadding="1" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="innerTableDetailThinTiny" colspan="3" style="width: 351px; height: 700px">
                                            <div>
                                                <table style="width: 350px; height: 650px">
                                                    <tr>
                                                        <td style="width: 430px; height: 640px" valign="top">
                                                            <%  if (bShowImg == false)
         {
      %>    
      <div class="productimg">
          <asp:Image id="Image52" runat="server" ImageUrl="/Image/noimage.gif"></asp:Image>              
      </div>
      <%
           }
          else  
          {
           %>
            <div class="productimg">
          <img src="<%=strItmPrimaryImg%>"  style="visibility:visible; height: 600px;"  name="mainthumb"  border="1"  alt="default image" />             
       </div> 
       <%
       }
       %>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                                            <ul class="alternate_img"> 
    
       <%
       
           foreach (string strImgTp in listImages)
           {
           %>
           <li class="alters"><a rel="<%=strItmNum%><%=strImgTp%>?<%=strZoomedParams%>" > 
           <img src="<%=strItmNum%><%=strImgTp%>"  style="visibility:visible"  name="mainthumb" width="35"  id="Img1"  border="0" alt="" onclick="mainthumb_onclick(this.src)" height="40" /> </a> 
           <%
           }
           %>
</li>     
</ul>
                                        </td>
                                    </tr>
                                </table>                                
                                <asp:Label ID="Label9" runat="server" Text="IMAGE PATH" Font-Bold="True"></asp:Label><br />
                                <br />
                                <asp:Label ID="Label10" runat="server" Text="Primary" Font-Bold="True"></asp:Label><br />
                                <br />
                                <asp:TextBox ID="imgPath" runat="server"  Height="25px" Width="410px" ></asp:TextBox><br />
                                <br />
                                <br />
                                <br />
           </td>
							<td class="innerTableDetail" valign="top" style="width: 30px" align="left">
                                
                                <br />
                                <asp:TextBox ID="itemDtlDesc" runat="server"  ForeColor="Gray" Height="278px" Width="379px" BorderStyle="Solid" TextMode="MultiLine" ></asp:TextBox>                                

                                <table cellspacing="0" cellpadding="1" style="width: 40px; height: 1px" align="center" border="0" id="Table2">
                                    <tbody>
                                        <tr>
                                            <td class="tableTitle" style="height: 3px; width: 400px;" colspan="6" align="left">
                                             <FTB:FreeTextBox ID="FreeTextBox1" runat="server" Width="360px" ConvertHtmlSymbolsToHtmlCodes="True" DownLevelMode="TextArea" BreakMode="Paragraph"></FTB:FreeTextBox>
            
            </td>
            <td><asp:LinkButton ID="dtlDescEdit" runat="server" Height="11px" Width="19px" OnClick="dtlDescEdit_Click" >edit</asp:LinkButton></td>
                                        </tr>
                                    </tbody>
                                </table>                                
                                &nbsp; 
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                
								<table cellspacing="0" cellpadding="1" style="width: 352px; height: 1px" align="center" border="0" id="TABLE2"">
									<tbody>
										<tr>
											<td class="tableTitle" style="height: 3px; width: 146px;" colspan="2" align="left">Item&nbsp;Information</td>
										</tr>
									  
									</tbody>
									
								</table>
                                
                                <table cellspacing="0" cellpadding="1" style="width: 352px; height: 1px" align="center" border="0" id="Table3" ">
                                    <tbody>
                                        <tr>
                                            <td class="tableTitle" style="height: 3px; width: 450px;" colspan="6">
                                <asp:Label ID="Label6" runat="server" Text="Dept Code:" Font-Bold="True"></asp:Label><asp:TextBox ID="deptNum" runat="server" Width="20px"></asp:TextBox>&nbsp;
                                                <asp:Label ID="Label7"
                                    runat="server" Text="Class Code:" Font-Bold="True"></asp:Label><asp:TextBox ID="classNum" runat="server"
                                        Width="20px"></asp:TextBox>
                                                &nbsp;<asp:Label ID="Label8" runat="server" Text="Subclass Code:" Font-Bold="True"></asp:Label>
                                                <asp:TextBox
                                            ID="subClassNum" runat="server" Width="20px"></asp:TextBox></td>
                                        </tr>
                                    </tbody>
                                </table>
                                &nbsp;&nbsp;&nbsp;<br />
                                <asp:GridView ID="skuGrdView" runat="server" Width="403px" AutoGenerateColumns="False" BorderStyle="Solid" BorderColor="Black" BorderWidth="1px">
                                <Columns>
                                        <asp:BoundField DataField="skuNum" HeaderText="SKU" FooterText="SKU" ReadOnly="True">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sizeId" HeaderText="SIZE"  FooterText="SIZE" ReadOnly="True">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="colorId" HeaderText="COLOR" FooterText="COLOR" ReadOnly ="True">
                                        </asp:BoundField>
                                    <asp:BoundField DataField="ats" HeaderText="ATS" ReadOnly="True" SortExpression="ats" />
                                    <asp:BoundField DataField="price" HeaderText="PRICE" ReadOnly="True" SortExpression="price" />
             </Columns>   
                                </asp:GridView>
                                <br />
                                </td></tr>
  </tbody>
 </table>
 </div>
</div>
</form>
</body>
</html>
