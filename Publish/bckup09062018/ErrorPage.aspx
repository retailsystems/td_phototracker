<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="PhotoTracker.ErrorPage" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ErrorPage</title>
     <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet">
</head>
<body>
  <div id="header"><uc1:header id="Header1" runat="server"></uc1:header>
    <form id="form1" runat="server">
    <div id="main">
        <TABLE class="innerTableDetail" id="tblOption" cellSpacing="0" cellPadding="0" width="86%" height="400" align="center" border="0" runat="server">
		     <tr valign="middle" align="center"><td>Active session was expired ,&nbsp; You may need to re-submit your login information.</td></tr>
        </TABLE>
          
      </div>
     </form>
    </div>
  
</body>
</html>

