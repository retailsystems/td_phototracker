<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProdBatchUpdate.aspx.cs" Inherits="PhotoTracker.ProdBatchUpdate"  validateRequest="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>PhotoTrack</title> 
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet" />
         <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script language="javascript" type="text/javascript">

            function calendarPicker(strField)
            {
              window.open('Calendar.aspx?field=' + strField, 'calendarPopup', 'width=250,height=200,resizable=yes');
            }
            
            function validateAndConfirmApprove(apprOrUnappr) 
            {

                var j=0;
                var gdvw = document.getElementById('<%= skuGrdView.ClientID %>');
                for (var i = 1; i < gdvw.rows.length; i++)
                {
                    var value = gdvw.rows[i].getElementsByTagName('input');
                    if (value != null)
                    {
                        if (value[0].type == "checkbox")
                        {
                            if (value[0].checked)
                            {
                                //alert("Checkbox selected successfully");
                                j = j + 1;
                            }
                        }
                    }
                }
                if (j>0) 
                {
                    document.getElementById("lblNoItemMsg").Visible = false;
                    var res = confirm('Are You Sure you want to ' + apprOrUnappr +' the selected Item/Items? Click OK to confirm');
                }
                else
                {
                    document.getElementById("lblNoItemMsg").innerHTML = 'At Least One Style need to be selected to ' + apprOrUnappr;
                    document.getElementById("lblNoItemMsg").Visible = true;
                }
                
                if (res != true)
                {    
                    return false;
                }
                else
                {
                    return true;
                }    
            }
            function validateSelect(fieldName) 
            {
                var myGrid = document.getElementById("<%= skuGrdView.ClientID %>");
                var oRows = myGrid.rows;
                var cell;
                var j=0;
                for (var i = 0; i < oRows.length; i++) 
                {
                    var oRow = oRows[i];
                    if (myGrid.rows(i).cells(0).children[0] != null && myGrid.rows(i).cells(0).children[0].checked == true) 
                    {
                        j = j + 1;
                    }
                }
                if (j>0) 
                {
                    document.getElementById("lblNoItemMsg").Visible = false;
                    var res = true;
                }
                else
                {
                    document.getElementById("lblNoItemMsg").innerHTML = "At Least One Style need to be selected for " + fieldName + "apply";
                    document.getElementById("lblNoItemMsg").Visible = true;
                    var res = false;
                }
                
                return res;  
            }

		</script>		
		
	</head>
		<body onload="javascript:document.Form1.txtItemNum.focus();"  bgcolor="#000000">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header>
			<div id="main" style="width: 988px">
<table class="innerTableDetail" id="tblOption" cellspacing="0" cellpadding="0" width="96%" align="center" border="0" runat="server">
					<tbody>
						<tr>
							<td class="innerTableDetail" colspan="2" >
								<table cellspacing="0" cellpadding="1" width="100%" align="center" border="0" style="height: 20px" id="TABLE1">
									<tbody>
										<tr>
											<td valign ="left" colspan="2" style="width: 980px; height: 144px;">
												<table style="width: 80%" border="0">
													<tr>
														<td class="tableTitle" style="width: 980px;" colspan="2" valign="top">Items Details - Batch Update</td>
													</tr>
													<tr><td colspan="4" style="width: 980px; height: 17px;"><font color="#cc0000"><asp:Label ID ="lblNoItemMsg" runat="server"></asp:Label></font></td>
													</tr>
													<tr style="height: 30px; width: 975px;">
														<td style="width: 970px; height: 26px">
															<table style="width: 970px" border="0">
																<tr>
                                                                    <td style="height: 103px; Width: 25px ">
                                                                    <asp:Label ID="Label1" runat="server" Text="Item: " Width="25px" Font-Bold="True"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 280px; height: 103px;">
                                                                    <asp:textbox id="txtItemNum" style="width: 280px; " runat="server"  CssClass="tableInput" columns="10" ForeColor="Black" Height="100px" TextMode="MultiLine" Width="100px" onkeydown = "return (event.keyCode!=13)"></asp:textbox>
                                                                    </td>
                                                                    <td style="width: 60px; height: 103px;">
                                                                    <asp:Button ID="searchbBtn" style="width: 60px; " runat="server" CssClass="buttonRed" Text="SEARCH" OnClick="searchBtnClicked"/>
                                                                    <br />
                                                                    <br />
                                                                    <br />
                                                                    <asp:Button ID="clearBtn" style="width: 60px; " runat="server" CssClass="buttonRed" Text="CLEAR" OnClick="clearBtn_Click" />
                                                                    </td>
                                                                    <td style="width: 250px; height: 103px;">
                                                                    </td>
                                                                    <td style="width: 60px; height: 103px;">
                                                                    <asp:Button ID="save" runat="server" CssClass="buttonRed" Text="SAVE"  OnClick="batchSave" />                                                                    
                                                                    </td>                                                                           
																</tr>																
															</table>
                                                            </td>
													</tr>
													<tr style="height: 30px; width: 975px;">
													<td style="width: 970px; height: 26px">
															<table style="width: 970px" border="0">
													<tr style="height: 30px; width: 970px;">
													<td style="width: 20px;height: 32px;">
													</td>
													<td style="width: 70px;height: 32px;">
													</td>
													<td style="width: 250px;height: 32px;">
													</td>
													<td style="height: 32px;">
													<asp:Label ID="Label4" style="width:150px;" runat="server" Text="APPROVAL DATE" Font-Bold="True"></asp:Label>
													</td>
													<td style="height: 32px">
													<asp:Label ID="Label3" style="width:150px;" runat="server" Text="LAST RECEIPT DATE" Font-Bold="True"></asp:Label>
													</td>
													<td style="height: 32px;width:110px;">
													</td>
													<td style="height: 32px;width:130px;">
													</td>
													</tr>
													<tr style="height: 30px; width: 970px;">
													<td style="width: 20px;height: 37px;" >
													<asp:CheckBox ID="slctAll" style="width: 20px;height: 32px;" runat="server" AutoPostBack="True" OnCheckedChanged="slctAll_CheckedChanged" />													
													</td>
													<td style="width: 80px; height: 37px;">
													<asp:Label ID="Label2" runat="server" Text="SELECT ALL" Width="80px" Font-Bold="True"></asp:Label>
													</td>
													<td style="width: 200px;height: 37px;">
													</td>
													<td style="height: 37px;width:150px;">
													<asp:TextBox ID="apprDate" runat="server" BorderStyle="Solid" Width="60px" ForeColor="Black" ></asp:TextBox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.apprDate');"
								href="javascript:;"><img height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="10" vspace="5"
									border="0" /></a>
                                                        <asp:Button ID="apprDtBtn" runat="server" OnClick="apprDate_TextChanged" Text="Apply" Font-Size="XX-Small" /></td>
													<td style="height: 37px;width:150px;">
													<asp:TextBox ID="rcvdDate" runat="server" BorderStyle="Solid" Width="60px" ForeColor="Black" ></asp:TextBox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.rcvdDate');"
								href="javascript:;"><img height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="10" vspace="5"
									border="0" /></a>
                                                        <asp:Button ID="rcptDtBtn" runat="server" Text="Apply" OnClick="rcvdDate_TextChanged" Font-Size="XX-Small" /></td>
													<td style="height: 37px;width:110px;">
													<asp:Button ID="approve" runat="server" Text="Approve" OnClick="approve_Click" OnClientClick="return validateAndConfirmApprove('Approve');" />
													</td>
													<td style="height: 37px;width:130px;" >
													<asp:Button ID="unApprove" runat="server" Text="Unapprove" OnClick="unApprove_Click" OnClientClick="return validateAndConfirmApprove('UnApprove');" />
													</td>
													</tr>
													</table>
                                                            </td>
													</tr>												     
												</table>                                               
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>							
							<td class="innerTableDetail" valign="top" style="width: 30px" align="left">                              

                                <asp:GridView ID="skuGrdView" runat="server" Width="930px" AutoGenerateColumns="False" BorderStyle="Solid" BorderColor="Black" BorderWidth="1px">
                                <Columns>
                                        <asp:TemplateField HeaderText="SELECT" FooterText="SELECT">
                                        <ItemTemplate>                                       
                                           <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="false" />                                        
                                        </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:BoundField DataField="item" HeaderText="ID" FooterText="ID" ReadOnly="True">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="itemDesc" HeaderText="Name"  FooterText="Name" ReadOnly="True">
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="APPROVAL DATE" FooterText="APPROVAL DATE">
                                        <ItemTemplate>                                       
                                           <asp:TextBox ID="apprDt" runat="server" BorderStyle="Solid" Width="150px" ForeColor="Black"></asp:TextBox>                                       
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LAST RECEIPT DATE" FooterText="LAST RECEIPT DATE">
                                        <ItemTemplate>                                       
                                           <asp:TextBox ID="lastRcvdDt" runat="server" BorderStyle="Solid" Width="150px" ForeColor="Black"></asp:TextBox>                                       
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="APPROVE" FooterText="APPROVE">
                                         <ItemTemplate>
                                            <asp:CheckBox ID="approve" Width="110px" runat="server" />
                                         </ItemTemplate>                             
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UNAPPROVE" FooterText="UNAPPROVE">
                                         <ItemTemplate>
                                            <asp:CheckBox ID="unApprove" Width="130px" runat="server" />
                                         </ItemTemplate>                             
                                        </asp:TemplateField>
             </Columns>   
                                </asp:GridView>
                                <br />
                                </td></tr>
  </tbody>
 </table>
 </div>
</div>
</form>
</body>
</html>
