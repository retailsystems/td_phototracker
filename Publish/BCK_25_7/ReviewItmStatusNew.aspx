<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReviewItmStatusNew.aspx.cs" Inherits="PhotoTracker.ReviewItmStatusNew" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Register Assembly="Bytescout.BarCode" Namespace="Bytescout.BarCode" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>PhotoTrack</title> 
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet" />
         <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script language="javascript" type="text/javascript">

            function calendarPicker(strField)
            {
              window.open('Calendar.aspx?field=' + strField, 'calendarPopup', 'width=250,height=200,resizable=yes');
            }
            
            function Comments()
            {
              var itemnum = "<%=strItemNum%>";
              window.open('Comments.aspx?strItmNum=' + itemnum, 'Comments', 'width=600,height=600,resizable=yes');
            }
 
          $(document).ready(function() 
          {
             $(document.body).delegate('.alters a', 'click', function()
	         {
                var image = $(this).attr("rel");
			    var src = $('.productimg img').attr("src") == image;
	           $('.productimg').html('<img src="' + image + '"/>').fadeOut(100).fadeIn(150);
             }
          );
         });
 

        function mainthumb_onclick() {

        }
		</script>
		
	</head>
		<body onload="javascript:document.Form1.txtItemNum.focus();"  bgcolor="#000000">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header>
			<div id="main" style="width: 988px">
<table class="innerTableDetail" id="tblOption" cellspacing="0" cellpadding="0" width="96%"
					align="center" border="0" runat="server">
					<tbody>
						<tr>
							<td class="innerTableDetail" colspan="2">
								<table cellspacing="0" cellpadding="1" width="100%" align="center" border="0" style="height: 20px" id="TABLE1">
									<tbody>
										<tr>
											<td colspan="2">
												<table width="100%" border="0">
													<tr>
														<td class="tableTitle" colspan="2" valign="middle">Items Details - Review Status</td>
													</tr>
													<tr style="height: 30px">
														<td>
															<table style="width: 600px" border="0">
																<tr>
																	<td style="width: 60px">Item Input: </td>
                                                                    <td style="width: 70px"><asp:textbox id="txtItemNum" runat="server" CssClass="tableInput" columns="10" OnTextChanged="txtItemNum_TextChanged"></asp:textbox></td>
                                                                    <td style="width: 62px">&nbsp;Description:</td>
                                                                     <td><asp:textbox ReadOnly ="True" id="txtDescription" runat="server" CssClass="tableInput" columns="48"></asp:textbox></td>
																</tr>
																
																<tr><td colspan="4" style="width: 600px"><font color="#cc0000"><asp:Label ID ="lblNoItemMsg" runat="server"></asp:Label></font></td>
																</tr>
																
															</table>
                                                            </td>
													</tr>
													<tr style="height: 20px"><td><table border="0">
													            <tr style="height: 4px"><td style="width: 38px">IN BY:</td>
													            <td style="width: 160px"><asp:textbox id="txtReceivIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="33"></asp:textbox></td><td width="10"> </td>
													            <td>SAMPLE IN:</td><td><asp:textbox id="txtSampleDateIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="30"></asp:textbox></td></tr>
													</table>
                                                        </td></tr>                                                    
												</table>
                                               
											</td>
											<td valign ="bottom" style="width:300px">
												<table cellspacing="0" cellpadding="0" width="50%" border="0">
													<tr>
																	
																	
																	<TD style="HEIGHT: 21px" align="left">
                                                                        <strong>&nbsp; </strong>
																		<asp:radiobuttonlist id="rdTopSam" runat="server" AutoPostBack="True" ToolTip="Please select TOP or SAMPLE item type.">
																		<asp:listitem value="TOPS">TOPS</asp:listitem>
																	    <asp:listitem value="SAMPLE">SAMPLE</asp:listitem></asp:radiobuttonlist>
																	</TD>
																	<td></td>
																	</tr>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class="innerTableDetail" valign="top" style="width: 493px">
								<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
									<tr style="height: 23px"><td style="width: 500px"><table>
									     <tr valign="bottom"><td class="tableTitle" style="HEIGHT: 23px; width: 300px" colspan="2">Entry &nbsp;Information</td>
									     <td align="right" valign="bottom"><font class="fontSmallBold">( Format: MM/DD/YYYY )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>
                                                &nbsp; &nbsp; &nbsp;</strong></font></td>
									</tr></table>
                                    </td></tr>

						            <tr style="font-weight: bold; font-family: Arial"><td style="width: 500px" class="innerTableDetailThin">
						            <table border="0" cellspacing="0" cellpadding="0">
									<tr style="height: 50px">										
										<td class="tableCell">
										<asp:Label Font-Bold="True"  ID="Label2" runat="server" Text="Stylist Check-in:" />
										&nbsp;
										  <table border="0" cellspacing="0" cellpadding="0">
										      <tr style="height: 20px">
     						                   <td valign="bottom" style="height: 20px">
     						                         <asp:textbox id="txtStylistChkIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="33" Width="206px"></asp:textbox>
     						                  </td>
						                      <td valign="bottom" width="48%" style="height: 20px">
						                          <asp:textbox id="txtStylistChkInDt" ReadOnly ="True" runat="server" CssClass="tableInput" columns="30"></asp:textbox>
									    </td>
									         </tr>
									         <tr style="height: 2px"><td></td></tr>
									         </table>
                                           	</td>
									 </tr>
									</table></td></tr> 
						            
						            <tr style="font-weight: bold; font-family: Arial"><td style="width: 500px" class="innerTableDetailThin">
						            <table border="0" cellspacing="0" cellpadding="0">
									<tr style="height: 50px">										
										<td class="tableCell">
										  <table border="0" cellspacing="0" cellpadding="0">
										      <tr style="height: 20px">
										      <td style="height: 12px; width: 40px;"> &nbsp;<strong> Copy</strong>:
                                              </td>
     						                   <td valign="bottom" style="height: 20px">
     						                         <asp:dropdownlist id="ddlCopyIn" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" ></asp:dropdownlist>
     						                  </td>
						                      <td valign="bottom" width="48%" style="height: 20px">
						                          <asp:textbox id="txtCopyDateIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtCopyDateIn');"
								href="javascript:;"><img height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0" /></a>
									    </td>
									         </tr>
									         <tr style="height: 2px"><td></td></tr>
									         </table>
                                           	</td>
									 </tr>
									</table></td></tr>  
									
									<tr style="color: #000000"><td style="width: 600px" class="innerTableDetailThin">
									<table border="0" cellspacing="0" cellpadding="0" style="width: 495px">
									<tr style="height: 20px">										
											<td class="tableCell" style="width: 36px;" >
										  <table border="0" cellspacing="0" cellpadding="0">
										      <tr style="height: 20px">
     						                   <td style="width: 40px;"><b>
										 &nbsp; Photo&nbsp;&nbsp; Photo:</b></td>
     						                   <td valign="bottom">
     						                         <asp:dropdownlist id="ddlPhotoIn" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput"></asp:dropdownlist>
     						                  </td>
						                      <td valign="bottom" style="width: 203px">
						                          <asp:textbox id="txtPhotoDateIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhotoDateIn');"
								href="javascript:;"><img height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0" /></a>
									</td>
									         </tr>
									         <tr style="height: 2px"><td></td></tr>
									         </table>

									      </td>
									 </tr>
									</table></td></tr>  
									<tr><td style="width: 500px;" class="innerTableDetailThin"><table border="0" cellspacing="0" cellpadding="0">
									<tr style="height: 50px">										
										<td class="tableCell">
										  <table border="0" cellspacing="0" cellpadding="0">
										      <tr style="height: 20px">
     						                   <td style="width: 40px; height: 20px;"><b>
										 &nbsp; Photo&nbsp;<br /> &nbsp; Edit:</b></td>
     						                   <td valign="bottom" style="height: 20px">
     						                         <asp:dropdownlist id="ddlPhEditIn" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput" ></asp:dropdownlist>
     						                  </td>
						                      <td valign="bottom" style="height: 20px">
						                          <asp:textbox id="txtPhEditIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtPhEditIn');"
								href="javascript:;"><img height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0" /></a>
									</td>
									         </tr>
									         <tr style="height: 2px"><td></td></tr>
									         </table>
									         
									      </td>
									 </tr>
									</table></td></tr>
									<tr><td style="width: 500px" class="innerTableDetailThin"><table border="0" cellspacing="0" cellpadding="0">
									<tr style="height: 50px">										
										<td class="tableCell" >
										  <table border="0" cellspacing="0" cellpadding="0">
										      <tr style="height: 20px">
     						                   <td style="width: 40px;"><b>&nbsp; Final <br /> &nbsp;&nbsp;App: </b></td>
     						                   <td valign="bottom">
     						                         <asp:dropdownlist id="ddlFinalIn" runat="server" AutoPostBack="True" Width="206px" CssClass="tableInput"></asp:dropdownlist>
     						                  </td>
						                      <td valign="bottom" >
						                          <asp:textbox id="txtFinalDateIn" ReadOnly ="True" runat="server" CssClass="tableInput" columns="20"></asp:textbox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.txtFinalDateIn');"
								href="javascript:;"><img style="height: 12px" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0" /></a>
									         </td>
									         </tr>
									         </table>
									      </td>
									 </tr>
									
									</table></td></tr>
									  <tr valign="bottom"><td style="width: 600px">
									  <table><tr><td style="width: 200px" valign="bottom">
									         <table><tr><td valign="bottom">&nbsp<a href="javascript:Comments()"><img src="./Image/Comments.gif" border="0" alt="" /></a></td></tr>
									                <tr><td style="width: 200px" valign="bottom"><font color="#cc0000"><asp:Label ID ="lblComExist" runat="server"></asp:Label></font></td></tr>
									         </table></td>
									             <td><cc1:barcodewebimage id="BarcodeWebImage2" runat="server" RenderingHint="SystemDefault" SmoothingMode="Default" Value="DATA" ForeColor="Black" HorizontalAlignment="Right" Angle="Degrees0" CaptionPosition="Below" NarrowBarWidth="2" VerticalAlignment="Top" AddChecksum="False" AddChecksumToCaption="False" AdditionalCaptionPosition="Above" AdditionalCaptionFont="Arial, 10pt" BackColor="White" BarHeight="20" CaptionFont="Arial, 10pt" DrawCaption="True" WideToNarrowRatio="3" Symbology="Code128" AdditionalCaption=" " EnableTheming="True"></cc1:barcodewebimage></td>
									  </tr></table>
                                      </td></tr>   
                                       
								</table>
                               </td>
							<td class="innerTableDetail" valign="top" style="width: 30px" align="center">
								<table cellspacing="0" cellpadding="1" style="width: 100px; height: 20px" align="center" border="0">
									<tbody>
										<tr>
											<td class="tableTitle" style="height: 5px" colspan="2">Item&nbsp;Information</td>
										</tr>
										<tr valign="top" style="height: 5px">
											<td class="innerTableDetailThinTiny" align="center" style="width: 150px; height: 5px">
												<table border="0" align="center" style="height: 35px; width : 80px" >
													<tr style="height: 5px">
														<td style="height: 5px; width: 84px;">&nbsp;DIVISION:</td><td style="width: 40px; height: 8px;">&nbsp;<asp:textbox ReadOnly ="True" id="txtDivision" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													
													<tr>
														<td style="height: 5px; width: 84px;">&nbsp;DEPARTMENT:</td><td style="width: 40px; height: 10px;">&nbsp;<asp:textbox ReadOnly ="True" id="txtDept" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													
													<tr>
														<td style="height: 5px;width: 84px">&nbsp;CLASS:</td><td style="width: 40px">&nbsp;<asp:textbox ReadOnly ="True" id="txtClass" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													
													<tr>
														<td style="height: 5px;width: 84px">&nbsp;SUBCLASS:</td><td style="width: 40px">&nbsp;<asp:textbox ReadOnly ="True" id="txtSubclass" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													 
													<tr>
														<td style="height: 38px;width: 84px">&nbsp;PRICE:</td><td style="width: 40px; height: 38px;">&nbsp;<asp:textbox ReadOnly ="True" id="txtPrice" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
													<tr>
														<td style="width: 84px; height: 59px;">&nbsp;CANCEL DATE:</td><td style="width: 40px; height: 59px;">&nbsp;<asp:textbox ReadOnly ="True" id="txtCancelDate" runat="server" CssClass="tableInput" columns="13"></asp:textbox></td>
													</tr>
										            <tr ><td style="height: 36px; width: 84px;">
										            <table><tr><td><asp:panel runat="server" id="pnlUpdate" visible="true"><table><tr><td><asp:Button id="btnUpdate" runat="server" Text="UPDATE" CssClass="buttonRed" OnClick="btnUpdate_Click"></asp:Button></td></tr></table></asp:panel>
									                      </td>
									                    	</tr></table></td><td style="height: 36px">
															<asp:Button id="btnClear" runat="server" Text="CLEAR" CssClass="buttonRed" OnClick="btnClear_Click"></asp:Button></td>
														 </tr>
													
												</table>
                                                </td><td style="width: 130px; height: 5px"><table border="0" class="innerTableDetailThinTiny">
                                                   
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;THEME 1</td>
													</tr>
													<tr>
												         <td style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme1" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
                                                  
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;THEME 2</td>
													</tr>
													<tr>
												         <td style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme2" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;THEME 3</td>
													</tr>
													<tr>
												         <td style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme3" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;THEME 4</td>
													</tr>
													<tr>
												         <td style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme4" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;THEME 5</td>
													</tr>
													<tr>
												         <td style="width: 156px">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtTheme5" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
													<tr>
														<td style="height: 5px; width: 156px">&nbsp;&nbsp;&nbsp;PRIV. LABEL</td>
													</tr>
													<tr>
												         <td style="width: 156px; height: 26px;">&nbsp;&nbsp;&nbsp;<asp:textbox ReadOnly ="True" id="txtPriLabel" runat="server" CssClass="tableInput" columns="12"></asp:textbox></td>
													</tr>
													
													</table></td>
										</tr>
									  
									</tbody>
									
								</table>
                                 <table cellspacing="0" cellpadding="1" width="100%" align="center" border="0">
                                <tr><td colspan="3" class="innerTableDetailThinTiny">
								<div>	<table style="width: 350px; height: 190px">
									<tr><td style="height: 190px; width: 50px" valign="top">  


     <ul class="alternate_img"> 
    
       <%
       
           foreach (string strImgTp in listImages)
           {
           %>
           <li class="alters"><a rel="<%=strItmNum%><%=strImgTp%>?<%=strZoomedParams%>" > 
           <img src="<%=strItmNum%><%=strImgTp%>?wid=45&hei=55&fmt=jpeg&qlt=85,0&op_sharpen=1&resMode=bicub"  style="visibility:visible"  name="mainthumb" width="35"  id="Img1"  border="0" alt="" onclick="return mainthumb_onclick()" /> </a> 
           <%
           }
           %>
</li>     
</ul> </td> 
       <td style="height: 190px; width: 200px" valign="top">   
           <%  if (bShowImg == false)
         {
      %>     
      <div class="productimg">
          <img src="<%=strItmNum%>_hi?wid=79&hei=109&fmt=jpeg&qlt=85,0&op_sharpen=1&resMode=bicub" height="190px"   style="visibility:hidden"  name="mainthumb"   border="0"  alt="default image" /></div>              
      
      <%
           }
          else  
          {
           %>
            <div class="productimg">
          <img src="<%=strItmNum%>_hi?$215x219$"  style="visibility:visible"  name="mainthumb" height="190px"  border="1"  alt="default image" />             
       </div> 
       <%
       }
       %>
      </td>
	 </tr></table></div>
	</td></tr>
  </table></td></tr>
  </tbody>
 </table>
 </div>
</div>
</form>
</body>
</html>


