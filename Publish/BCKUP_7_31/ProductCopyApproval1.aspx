<%@ Register TagPrefix="uc1" TagName="Header" Src="Header.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductCopyApproval1.aspx.cs" Inherits="PhotoTracker.ProductCopyApproval1"  validateRequest="false"%>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<%@ Register Assembly="Bytescout.BarCode" Namespace="Bytescout.BarCode" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>PhotoTrack</title> 
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link id="lnkStyles" href="./Style/Style.css" type="text/css" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
         <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script language="javascript" type="text/javascript">

            function calendarPicker(strField)
            {
              window.open('Calendar.aspx?field=' + strField, 'calendarPopup', 'width=250,height=200,resizable=yes');
            }
            function DetailDesc()
            {
              var itemnum = "<%=strItemNum%>";
              window.open('DetailDesc.aspx?strItmNum=' + itemnum, 'DetailDesc', 'width=600,height=600,resizable=yes');
            }

          $(document).ready(function() 
          {
             $(document.body).delegate('.alters a', 'click', function()
	         {
                var image = $(this).attr("rel");
			    var src = $('.productimg img').attr("src") == image;
	           $('.productimg').html('<img src="' + image + '" style="height: 600px;" />').fadeOut(100).fadeIn(150);
             }
          );
          
         });
        
        function NextColor_onclick() {            
            var i= document.getElementById("ddlColorShade").selectedIndex;
            var j= document.getElementById("ddlColorShade").length;
            if(i<j-1){
            document.getElementById("ddlColorShade").selectedIndex=i+1;
             __doPostBack('<%= ddlColorShade %>', '');
              document.getElementById("selectPrev").disabled = false;  
             }else{
             document.getElementById("selectNext").disabled = true;}
           
         }
         
        function PrevColor_onclick() { 
            var i= document.getElementById("ddlColorShade").selectedIndex;        
            if(i>0)            
            {document.getElementById("ddlColorShade").selectedIndex=i-1;
             __doPostBack('<%= ddlColorShade %>', '');
             document.getElementById("selectNext").disabled = false;
             }else{    
              document.getElementById("selectPrev").disabled = true;}    
         }
         
        function mainthumb_onclick(strName) {
         document.getElementById("imgPath").value = strName;
         document.getElementById("lblPriImgerrormsg").innerHTML = '';
         if (document.getElementById("imgPath").value==document.getElementById("primeImgPath").value)
         {
             document.getElementById("primeImgChk").checked = true;
         }
         else
         {
             document.getElementById("primeImgChk").checked = false;
         }
        }
        
        function primeImg_onclick() {
        var Chk = document.getElementById("primeImgChk");
        if (Chk.checked) 
           {
                if (document.getElementById("imgPath").value == "")
                        {
                            document.getElementById("lblPriImgerrormsg").innerHTML = 'Select the Image before checking Primary Option';
                            document.getElementById("lblPriImgerrormsg").Visible = true;
                            document.getElementById("primeImgChk").checked = false;
                        }
                        else
                        {
                            document.getElementById("primeImgPath").value = document.getElementById("imgPath").value;
                            document.getElementById("lblPriImgerrormsg").innerHTML = '';
                            document.getElementById("lblPriImgerrormsg").Visible = false;
                        } 
            
            }
            else
            {
               document.getElementById("primeImgPath").value="";
            }            
         
        }

		</script>		
		
	</head>
		<body onload="javascript:document.Form1.txtItemNum.focus();"  bgcolor="#000000">
		<form id="Form1" method="post" runat="server">
			<div id="header"><uc1:header id="Header1" runat="server"></uc1:header>
			<div id="main" style="width: 988px">
<table class="innerTableDetail" id="tblOption" cellspacing="0" cellpadding="0" width="96%" align="center" border="0" runat="server">
					<tbody>
						<tr>
							<td class="innerTableDetail" colspan="2" >
								<table cellspacing="0" cellpadding="1" width="100%" align="center" border="0" style="height: 20px" id="TABLE1">
									<tbody>
										<tr>
											<td valign ="left" colspan="2" style="width: 487px; height: 144px;">
												<table style="width: 80%" border="0">
													<tr>
														<td class="tableTitle" style="width: 487px;" colspan="2" valign="top">Items Details - Product Copy &amp; Approval</td>
													</tr>
													<tr><td colspan="4" style="width: 487px; height: 17px;"><font color="#cc0000"><asp:Label ID ="lblNoItemMsg" runat="server"></asp:Label></font></td>
													</tr>
													<tr style="height: 30px; width: 487px;">
														<td style="width: 487px; height: 26px">
															<table style="width: 487px" border="0">
																<tr>
                                                                    <td style="height: 103px; Width: 25px ">
                                                                    <asp:Label ID="Label1" runat="server" Text="Item: " Width="25px" Font-Bold="True"></asp:Label></td>
                                                                    <td style="width: 160px; height: 103px;"><asp:textbox id="txtItemNum" style="width: 80px; " runat="server"  CssClass="tableInput" columns="10" ForeColor="Black" ></asp:textbox>
                                                                        <asp:Button ID="searchbBtn" style="width: 60px; " runat="server" CssClass="buttonRed" Text="SEARCH" OnClick="searchBtnClicked"/></td>
                                                                    <td style="width: 6px; height: 103px;" rowspan="">&nbsp; 
                                                                        <cc1:BarcodeWebImage ID="BarcodeWebImage2" runat="server" AddChecksum="False" AddChecksumToCaption="False"
                                                                            AdditionalCaption=" " AdditionalCaptionFont="Arial, 10pt" AdditionalCaptionPosition="Above"
                                                                            Angle="Degrees0" BackColor="White" BarHeight="20" CaptionFont="Arial, 10pt" CaptionPosition="Below"
                                                                            DrawCaption="True" EnableTheming="True" ForeColor="Black" HorizontalAlignment="Right"
                                                                            NarrowBarWidth="2" RenderingHint="SystemDefault" SmoothingMode="Default" Symbology="Code128"
                                                                            Value="DATA" VerticalAlignment="Top" WideToNarrowRatio="3" /></td>       
																</tr>																
																<tr><td colspan="4" style="width: 487px; height: 17px;">
                                                                    <asp:Label ID="Label2" runat="server" Text="ID:" Width="25px" Font-Bold="True"></asp:Label>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:textbox ID="itemNum" Width="80px" ReadOnly ="True" runat="server" CssClass="tableInput" columns="10" ForeColor="Black" Font-Bold="True" ></asp:textbox>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label15" runat="server" Text="Program ID:" Width="70px" Font-Bold="True"></asp:Label>
                                                                    &nbsp; <asp:textbox ID="programNum" Width="110px" ReadOnly ="True" runat="server" CssClass="tableInput" columns="10" ForeColor="Black" Font-Bold="True" ></asp:textbox>                                                          
                                                                    </td>
																</tr>	
																
																<tr><td colspan="4" style="width: 487px; height: 17px;">
																<br/>
																<asp:Label ID="Label3" runat="server" Text="Catalog: " Width="33px" Font-Bold="True"></asp:Label>&nbsp;
                                                                    &nbsp; &nbsp;<asp:textbox ID="catDesc" runat="server" ReadOnly="True" ForeColor="Black" Font-Bold="True"></asp:textbox>                                                                    
                                                                    &nbsp;<asp:Label ID="Label16" runat="server" Text="Color:" Width="25px" Font-Bold="True"></asp:Label>&nbsp;
                                                                    &nbsp; &nbsp; <asp:textbox ID="selColor" Width="110px" ReadOnly ="True" runat="server" CssClass="tableInput" columns="10" ForeColor="Black" Font-Bold="True" ></asp:textbox>
                                                                     </td>
																</tr>	
																														
															</table>
                                                            </td>
													</tr>												     
												</table>                                               
											</td>
											<td style="width:450px; height: 144px;">
											<table>
                                                <br />
                                                <tr>
                                                <asp:Label ID="Label5" style="width:200px;" runat="server" Text="LAST UPDATE" Font-Bold="True"></asp:Label>&nbsp;&nbsp;
                                                <asp:TextBox ID="last_upd_dt_id" runat="server" Width="220px" ReadOnly="True" BorderStyle="None" Font-Names="Arial" ForeColor="Black" Font-Bold="True"></asp:TextBox>
                                                </tr>                                                
                                                <tr >                                                                                               
                                                <br />
                                                <asp:Label ID="Label4" style="width:100px;" runat="server" Text="APPROVAL DATE" Font-Bold="True"></asp:Label>                                                
                                                &nbsp; <asp:TextBox ID="apprDate" runat="server" BorderStyle="Solid" Width="70px" ForeColor="Black"></asp:TextBox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.apprDate');"
								href="javascript:;"><img height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0" /></a>
									            </tr>
									            <tr>
									            <br />
									            <asp:Label ID="Label14" style="width:100px;" runat="server" Text="LAST RECEIPT DATE" Font-Bold="True"></asp:Label>                                                
                                                &nbsp; <asp:TextBox ID="lastRcptDate" runat="server" BorderStyle="Solid" Width="70px" ForeColor="Black"></asp:TextBox><a title="Pick Date from Calendar" onclick="calendarPicker('Form1.lastRcptDate');"
								href="javascript:;"><img height="12" alt="cal" hspace="3" src="Image/calendar.jpg" width="20" vspace="5"
									border="0" /></a>
                                                <br />
                                                </tr> 
                                                <tr>
                                                <td>                                                 
                                                <asp:Button ID="approve" runat="server" CssClass="buttonRed" Text="Approve" OnClick="approve_Click" />
                                                </td>
                                                <td style="width:20px;"><asp:TextBox ID="currStatus" runat="server" style="display:none;"></asp:TextBox></td>
                                                <td>
                                                <asp:Button ID="unApprove" runat="server" CssClass="buttonRed" Text="Unapprove" OnClick="unApprove_Click" />
                                                </td>
                                                </tr>                                                  
                                                    </table>
                                                    </td>                                                    
                                                    
										</tr>
										<tr style="height: 30px; width: 900px;">										
										<td style="height: 30px; width: 100px;">
										<asp:Label ID="Label11" runat="server" Height="17px" Text="RMS DESCRIPTION:" Width="133px" Font-Bold="True"></asp:Label>
										</td>
										<td style="height: 30px; width: 200px;">
										<asp:textbox id="txtRMSDesc" runat="server" columns="48" Height="17px" Width="283px" ForeColor="Black" Font-Bold="True" ></asp:textbox>
										</td>
										<td style="width:400px;">
										</td>
										<td>
										<table>
										<tr>
										<td style="width:100px;">
                                        <asp:LinkButton ID="titleEdit" runat="server" Height="30px" Width="50px" OnClick="titleEdit_Click1" Font-Bold="True" Font-Size="X-Large">edit</asp:LinkButton>
                                        </td>
                                        <td style="width:100px;">
                                        &nbsp;&nbsp;
                                        </td>
										<td style="width:100px;">
                                        <asp:Button ID="save" runat="server" CssClass="buttonRed" Text="SAVE"  OnClick="save_Click" />
                                        </td>                                        
                                        </tr>
                                        </table>
                                        </td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class="innerTableDetail" valign="top" style="width: 439px">
                                &nbsp;&nbsp;<br />
                                <table align="center" border="0" cellpadding="1" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="innerTableDetailThinTiny" colspan="3" style="width: 351px; height: 700px">
                                            <div>
                                                <table style="width: 350px; height: 650px">
                                                    <tr>
                                                        <td style="width: 430px; height: 640px" valign="top">
                                                            <%  if (bShowImg == false)
         {
      %>    
      <div class="productimg">
          <asp:Image id="Image52" runat="server" ImageUrl="/PhotoTracker/Image/noimage.gif"></asp:Image>              
      </div>
      <%
           }
          else  
          {
           %>
            <div class="productimg">
          <img src="<%=strItmPrimaryImg%>"  style="visibility:visible; height: 600px;"  name="mainthumb"  border="1"  alt="default image" />             
       </div> 
       <%
       }
       %>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                                            <ul class="alternate_img"> 
    
       <%
       
           foreach (string strImgTp in listImages)
           {
           %>
           <li class="alters"><a rel="<%=strItmNum%><%=strImgTp%><%=strZoomedParams%>" > 
           <img src="<%=strItmNum%><%=strImgTp%>"  style="visibility:visible"  name="mainthumb" width="35"  id="Img1"  border="0" alt="" onclick="mainthumb_onclick(this.src)" height="40" /> </a> 
           <%
           }
           %>
</li>     
</ul>
                                        </td>
                                    </tr>
                                </table>
                                                                
                                <table>
                                <tr>
                                <td>
                                <asp:Label ID="Label9" runat="server" Text="IMAGE PATH" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                <asp:Label ID="Label10" runat="server" Text="PRIMARY" Font-Bold="True"></asp:Label>
                                </td>
                                </tr>
                                <br />
                                <tr>
                                <td>
                                <asp:TextBox ID="imgPath" runat="server"  Height="20px" Width="410px" Enabled="False" ReadOnly="True" ></asp:TextBox>
                                </td>
                                <td>
                                <asp:CheckBox ID="primeImgChk" runat="server" onclick="primeImg_onclick();"/>
                                </td>
                                </tr>
                                </table>
                                <asp:Label ID="lblPriImgerrormsg"  runat="server" Text="" ForeColor="Red"></asp:Label>
                                <asp:TextBox ID="primeImgPath" runat="server" style="display:none;"></asp:TextBox>                                
                                <br />
                                <br />
                                <br />
                                <br />
           </td>
							<td class="innerTableDetail" valign="top" style="width: 30px" align="left">
                                
                                <br />
                                <asp:TextBox ID="itemDtlDesc" runat="server"  ForeColor="Gray" Height="278px" Width="379px" BorderStyle="Solid" TextMode="MultiLine" ></asp:TextBox>                                

                                <table cellspacing="0" cellpadding="1" style="width: 40px; height: 1px" align="center" border="0" id="Table2">
                                    <tbody>
                                        <tr>
                                            <td class="tableTitle" style="height: 3px; width: 400px;" colspan="6" align="left">
                                            <asp:Label ID="Label12"  runat="server" Text="Web Title" ForeColor="Black"></asp:Label>
                                            <asp:TextBox ID="itemTitle" runat="server"  Width="100%" BorderStyle="Solid" ForeColor="Gray" CssClass="tableInput" Height="80px" TextMode="MultiLine" onkeydown = "return (event.keyCode!=13)"></asp:TextBox>                                             
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                         
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                             
                                             <br />
                                             <br />
                                             <asp:Label ID="Label13"  runat="server" Text="Web Description" ForeColor="Black"></asp:Label>
                                             <br />
                                             <FTB:FreeTextBox ID="FreeTextBox1" runat="server" Width="360px" ConvertHtmlSymbolsToHtmlCodes="True" DownLevelMode="TextArea" BreakMode="Paragraph"></FTB:FreeTextBox>                                            
            
            </td>
                                        </tr>
                                    </tbody>
                                </table>                                
                                &nbsp; 
                                <br />
                                 <table cellspacing="0" cellpadding="1" style="width: 352px; height: 1px" align="center" border="0" id="TableColorSel">
									<tbody>
										<tr>
											<td class="tableTitle" style="height: 3px; width: 146px;" colspan="2" align="left">Color&nbsp;Shade</td>
										</tr>
									    <tr>
											<td class="tableTitle" style="height: 3px; width: 146px;" colspan="2" align="left">
											<a href="javascript:void(0);" id='selectPrev' style="margin-top: 3px; float: left;" onclick="PrevColor_onclick();"><i class="fa fa-arrow-circle-left" style="font-size :30px"></i></a>
											
											<asp:DropDownList
                                                ID="ddlColorShade" runat="server" Width="296px" Height="30px" CssClass="pd4" ToolTip="please select Color Shade"  OnSelectedIndexChanged="ddlColorShade_SelectedIndexChanged" AutoPostBack="True">
                                            </asp:DropDownList>
                                            <a href="javascript:void(0);" id='selectNext'style="margin-top:3px"  onclick="NextColor_onclick();"><i class="fa fa-arrow-circle-right" style="font-size :30px"></i></a>
                                          
											</td>
										</tr>
									</tbody>									
								</table>
                                <br />
                                
								<table cellspacing="0" cellpadding="1" style="width: 352px; height: 1px" align="center" border="0" id="TABLE2"">
									<tbody>
										<tr>
											<td class="tableTitle" style="height: 3px; width: 146px;" colspan="2" align="left">Item&nbsp;Information</td>
										</tr>
									  
									</tbody>
									
								</table>
                                
                                <table cellspacing="0" cellpadding="1" style="width: 352px; height: 1px" align="center" border="0" id="Table3" ">
                                    <tbody>
                                        <tr>
                                            <td class="tableTitle" style="height: 3px; width: 110px;" colspan="6">
                                <asp:Label ID="Label6" runat="server" Text="DEP" Font-Bold="True"></asp:Label>&nbsp;&nbsp;<asp:TextBox ID="deptNum" runat="server" Width="45px"></asp:TextBox>&nbsp;
                                                <asp:Label ID="Label7"
                                    runat="server" Text="CLS" Font-Bold="True"></asp:Label>&nbsp;&nbsp;<asp:TextBox ID="classNum" runat="server"
                                        Width="55px"></asp:TextBox>
                                                &nbsp;<asp:Label ID="Label8" runat="server" Text="SCLS" Font-Bold="True"></asp:Label>&nbsp;&nbsp;
                                                <asp:TextBox ID="subClassNum" runat="server" Width="55px"></asp:TextBox></td>
                                        </tr>
                                    </tbody>
                                </table>
                                &nbsp;&nbsp;&nbsp;<br />
                                <asp:GridView ID="skuGrdView" runat="server" Width="352px" CssClass="marginauto" AutoGenerateColumns="False" BorderStyle="Solid" Height="250px" BorderColor="Black" BorderWidth="1px">
                                <Columns>
                                        <asp:BoundField DataField="skuNum" HeaderText="SKU" FooterText="SKU" ReadOnly="True">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="sizeId" HeaderText="SIZE"  FooterText="SIZE" ReadOnly="True">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="colorId" HeaderText="COLOR" FooterText="COLOR" ReadOnly ="True">
                                        </asp:BoundField>
                                    <asp:BoundField DataField="ats" HeaderText="ATS" ReadOnly="True" SortExpression="ats" />
                                    <asp:BoundField DataField="price" HeaderText="PRICE" ReadOnly="True" SortExpression="price" />
                                     <asp:BoundField DataField="PhotoCompleted" HeaderText="Image" ReadOnly="True" SortExpression="Photo Complete" />
                                    <asp:BoundField DataField="online" HeaderText="Online" ReadOnly="True" SortExpression="online" />
                                   
             </Columns>   
                                </asp:GridView>
                                <br />
                                </td></tr>
  </tbody>
 </table>
 </div>
</div>
</form>
</body>
</html>
